# nsjail-test-python

This is a test container for testing that nsjail can be deployed to kubernetes.
It provides a simple example flag and healthcheck container.

This nsjail template uses a config file instead of the command line options, and also includes a more complex config file example (unused).

## Notes nsjail
* After testing ttyd, it became too complicated in our infrastructure to have priveged containers running docker in docker.
* We looked into nsjail, an unofficial google product used in googlectf.
* It can work similar to socat, but with greater fidelity on networking and file access
* Build: `docker build -t nsjail_template -f Dockerfile.nsjail .`
* Run: `docker run -it --rm -p 9000:9000 --privileged nsjail_template`
* Client connect: `nc localhost 9000`
* The Dockerfile.nsjail has a breakdown of the nsjail commandline

