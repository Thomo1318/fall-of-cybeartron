#!/usr/bin/env python3
import binascii
import sys
import traceback

if sys.version_info < (3, ):
    raise Exception("Run this in Python 3")

import consolemenu
import Cryptodome.Cipher.AES
import Cryptodome.Random

###############################################################################
# The actual crypto functionality
###############################################################################
KEY = Cryptodome.Random.get_random_bytes(32)
CIPHER = Cryptodome.Cipher.AES.new(
    key=Cryptodome.Random.get_random_bytes(32),
    mode=Cryptodome.Cipher.AES.MODE_CBC,
)

def encrypt_aes_cbc(ptext):
    # Pad the plaintext per PKCS7
    pad_len = (
        Cryptodome.Cipher.AES.block_size
        - (len(ptext) % Cryptodome.Cipher.AES.block_size)
    )
    ptext += bytearray(pad_len for _ in range(pad_len))
    # Make a new cipher object with a fresh IV
    iv = Cryptodome.Random.get_random_bytes(Cryptodome.Cipher.AES.block_size)
    ctx = Cryptodome.Cipher.AES.new(
        key=KEY, mode=Cryptodome.Cipher.AES.MODE_CBC, iv=iv
    )
    return iv + ctx.encrypt(ptext)

def decrypt_aes_cbc(ztext):
    # Split the first block (IV) and remainder
    split_idx = Cryptodome.Cipher.AES.block_size
    iv, ztext = ztext[:split_idx], ztext[split_idx:]
    # Make a new cipher object with the IV
    ctx = Cryptodome.Cipher.AES.new(
        key=KEY, mode=Cryptodome.Cipher.AES.MODE_CBC, iv=iv
    )
    # Decrypt and strip the PKCS7 padding
    ptext = ctx.decrypt(ztext)
    pad_len = ptext[-1]
    if (
        pad_len > Cryptodome.Cipher.AES.block_size or
        not all(b == pad_len for b in ptext[-pad_len:])
    ):
        # This is the oracle
        raise ValueError("Invalid padding")

###############################################################################
# CLI menu
###############################################################################
class NonClearingScreen(consolemenu.screen.Screen):
    def clear(self):
        pass

class EncryptFlagMenuItem(consolemenu.items.MenuItem):
    def action(self):
        with open("./flag.txt", "rb") as f:
            ptext = f.read().strip()
            self.menu.screen.println(
                binascii.hexlify(encrypt_aes_cbc(ptext)).decode()
            )

class DecryptDataMenuItem(consolemenu.items.MenuItem):
    def action(self):
        ztext = binascii.unhexlify(input())
        print("Read %i bytes" % len(ztext))
        try:
            ptext = decrypt_aes_cbc(ztext)
        except ValueError:
            traceback.print_exc()
        else:
            print("Processed decrypted data")

class DoAttackMenuItem(consolemenu.items.MenuItem):
    def action(self):
        with open("./flag.txt", "rb") as f:
            ptext = f.read().strip()
        ztext = encrypt_aes_cbc(ptext)
        # Walk backward block-wise through the ciphertext
        aes_bs = Cryptodome.Cipher.AES.block_size
        ptext = bytearray()
        for tail_offset in range(len(ztext), aes_bs, -aes_bs):
            # z1 is the original preceding ciphertext chunk
            z1 = ztext[tail_offset - aes_bs * 2 : tail_offset - aes_bs]
            z2 = ztext[tail_offset - aes_bs : tail_offset]
            # Walk backward through z2 brute forcing each byte with the oracle
            z1prime = bytearray(aes_bs) # Make a block size bytearray
            for block_offset in range(aes_bs - 1, -1, -1):
                desired_padval = aes_bs - block_offset
                for candidate in range(256):
                    z1prime[block_offset] = candidate
                    # Attempt a decrypt, if it doesn't explode then we have the
                    # right z1' value to have met the PKCS padding requirement
                    try:
                        decrypt_aes_cbc(z1prime + z2)
                    except ValueError:
                        pass
                    else:
                        # Found it!
                        break
                else:
                    raise Exception(
                        "Failed to find z1prime byte at %i"
                        % (tail_offset - aes_bs * 2 + block_offset)
                    )
                # So now we know that:
                #   `p2[off] = z1prime[off] ^ z1[off] ^ desired_padval`
                ptext.insert(0, candidate ^ z1[block_offset] ^ desired_padval)
                # And we want to fix `z1prime` for the next padding width
                for i in range(block_offset, aes_bs):
                    z1prime[i] ^= desired_padval ^ (desired_padval + 1)
        print("Extracted the plaintext! %r" % ptext)

def run():
    menu = consolemenu.ConsoleMenu(
        "Oracle example",
        "This interface accepts and returns hex encoded binary",
        screen=NonClearingScreen(),
    )
    menu.append_item(EncryptFlagMenuItem("Get the encrypted flag"))
    menu.append_item(DecryptDataMenuItem("Process encrypted data"))
    menu.append_item(DoAttackMenuItem("Extract the flag using the oracle"))
    menu.start()
    menu.join()

if __name__ == "__main__":
    run()
