#include <stdio.h>

int main() {
    printf("Welcome to the test challenge!\n");
    printf("Your flag is:\n");
    FILE* f = fopen("flag.txt", "r");
    char* flag[255] = {'\0'};
    fscanf(f, "%s", flag);
    printf("%s\n", flag);
    printf("Thanks for playing!\n");
    return 0;
}
