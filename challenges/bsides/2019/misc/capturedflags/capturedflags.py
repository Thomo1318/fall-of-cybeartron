from flask import Flask, render_template, Response
import time
import os.path
import base64
app = Flask(__name__)

TCP_PORT=9000
FLAG = "THE FLAG IS CYBEARS BRACE ILOVEAGOODVISUALPUN BRACE"

def get_flag(char):

    flag_file_path = os.path.join("flags", "%s.png" % char)
    if not os.path.exists(flag_file_path):
        return get_flag("READY")

    with open(flag_file_path, "rb") as flag_file:
        return flag_file.read()

def build_multipart_frame(this_frame):

    boundary = b'--frame\r\n'
    headers = b'Content-Type: image/png\r\nContent-Length: %d\r\n\r\n' % len(this_frame)

    return (boundary + headers + this_frame)

def next_frame():

    ready_frame = get_flag("READY")
    error_frame = get_flag("ERROR")
    print("ready frames")
    for x in range(0, 5):
        time.sleep(1)
        yield build_multipart_frame(ready_frame)

    print("signal frames")
    for pos in range(0, len(FLAG)):
        time.sleep(1)
        print(FLAG[pos])
        this_frame = get_flag(FLAG[pos])
        yield build_multipart_frame(get_flag(FLAG[pos]))

    print("error frames")
    for x in range(0, 5):
        time.sleep(1)
        yield build_multipart_frame(error_frame)


@app.route('/stream')
def stream():
    return Response(next_frame(), mimetype="multipart/x-mixed-replace; boundary=frame")

@app.route('/')
def index():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=TCP_PORT)
