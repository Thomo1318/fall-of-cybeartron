# Captured Flags

## Build

You can build the PCAP handout for this challenge if you're on Linux by running
the `run_capture.sh` script. The `Makefile` does this as part of its default
target. If you need to replicate it on another platform the old process is:

- Run the server (capturedflags.py)
- Start a packet capture
- Browse to the server from another machine
- Wait for the flags to stop moving
- Stop the packet capture
- Filter out just the TCP connections to the server

## Solution

The packet capture contains two TCP connections to a web server. The first stream contains a HTTP request that gets a HTML page that embeds an image from the same web server. The second stream contains a multipart HTTP stream containing multiple PNG images.

The PNG images can be extracted by first extracting the HTTP session from the pcap, then splitting it by the multipart boundary.

The images contain [flag semaphore](https://en.wikipedia.org/wiki/Flag_semaphore) that spells out a phrase containing the flag.

## Flag

```
cybears{iloveagoodvisualpun}
```
