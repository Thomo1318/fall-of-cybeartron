#!/usr/bin/python3

# level1a-flag = b'\x91\xc7&\xac\x89\xfa\xfb\r\xfdYp\xdd%\x05\x8a\xe8\xcd\xd3\xcdj\xe0\x83\xc1\x9fU\xe1\xbc\xc1>\x9b\x85\x85'
with open('level1a-flag', 'rb') as f:
    level1a = f.read()
# level1b-flag = b'\xf2\xbeD\xc9\xe8\x88\x88v\x89+\x05\xf9QZ\xe3\x86\x92\xa7\xa5Y\xbf\xe0\xf1\xf1!\x80\x9d\xaf\r\xe9\xff\xf8'
with open('level1b-flag', 'rb') as f:
    level1b = f.read()

# We xor them together (hopefully the small hints in the dockerfile lead to this)
flag = bytes(a ^ b for a, b in zip(level1a, level1b))

print("\n===\nFlag is: {}\n===\n".format(flag.decode()))
