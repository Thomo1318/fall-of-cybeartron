Level 2
---

Friend,

This will be our last communication - the security agents at the border are 
turning up the heat. I need to you to discover the final flag, and I've had
to obfuscate it in a new way.

Like before, the container will ship to you as a verified build. I've used 
a multi staging process to destroy the secret generator, so unfortunately
layer spelunking won't help you.

I'm sure if anyone can access the flag, then you can.

- Bumblebear
