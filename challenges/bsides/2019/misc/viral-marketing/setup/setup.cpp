// setup.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <windows.h>
#include "fileapi.h"
#include <iostream>
#include "bcrypt.h"
#include "pathcch.h"


#define NT_SUCCESS(Status)          (((NTSTATUS)(Status)) >= 0)

#define STATUS_UNSUCCESSFUL         ((NTSTATUS)0xC0000001L)

int wmain(int argc, wchar_t *argv[], wchar_t *envp[])
{
	DWORD serial;
	DWORD maximumComponentLength;
	DWORD fileSystemFlags;
	HANDLE f = NULL;
	DWORD bytesWritten = 0;
	std::string decryptedtext;

	if (argc != 2) {
		std::cout << "setup.exe [directory]" << std::endl;
		return -1;
	}


	
	BCRYPT_ALG_HANDLE       hAesAlg = NULL;
	BCRYPT_KEY_HANDLE       hKey = NULL;
	NTSTATUS                status = STATUS_UNSUCCESSFUL;   
	DWORD                   cbCipherText = 0,
		cbData = 0,
		cbKeyObject = 0,
		cbBlockLen = 0,
		cbBlob = 0;
	PBYTE                   pbCipherText = NULL,
		pbKeyObject = NULL,
		pbBlob = NULL;

	byte key[16], iv[16];
	std::wstring dir(argv[1]);	

	std::string flag = "cybears{TurBu13nt_ju1c3}";
	std::string encrypted_hint = "Really really close but this is not the flag you are looking for";
	std::string unencrypted_hint = "It was never going to be that easy!";

	std::wstring file_flag = dir + L"ADS.txt. :flag";
	std::wstring file_unencrypted_hint = dir + L"flag.txt ::$DATA";
	std::wstring file_encrypted_hint = dir + L"flag.txt :flag";
	wchar_t root[MAX_PATH];
	wcsncpy_s(root, dir.c_str(), (ULONG)dir.length());

	PathCchStripToRoot(root, wcsnlen(root, MAX_PATH));

	GetVolumeInformation(root, nullptr, 0, &serial, &maximumComponentLength, &fileSystemFlags, nullptr, 0);
	for (int i = 0; i < (sizeof(key) / sizeof(DWORD)); i++) {
		memcpy(key + (sizeof(DWORD) * i), (byte *)&serial, sizeof(DWORD));
		memcpy(iv + (sizeof(DWORD) * i), (byte *)&fileSystemFlags, sizeof(DWORD));
	}

	// Open an algorithm handle.
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(
		&hAesAlg,
		BCRYPT_AES_ALGORITHM,
		NULL,
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptOpenAlgorithmProvider\n", status);
		goto Cleanup;
	}

	// Calculate the size of the buffer to hold the KeyObject.
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAesAlg,
		BCRYPT_OBJECT_LENGTH,
		(PBYTE)&cbKeyObject,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptGetProperty\n", status);
		goto Cleanup;
	}

	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject)
	{
		wprintf(L"**** memory allocation failed\n");
		goto Cleanup;
	}

	// Calculate the block length for the IV.
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAesAlg,
		BCRYPT_BLOCK_LENGTH,
		(PBYTE)&cbBlockLen,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptGetProperty\n", status);
		goto Cleanup;
	}
	
	if (!NT_SUCCESS(status = BCryptSetProperty(
		hAesAlg,
		BCRYPT_CHAINING_MODE,
		(PBYTE)BCRYPT_CHAIN_MODE_CBC,
		sizeof(BCRYPT_CHAIN_MODE_CBC),
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptSetProperty\n", status);
		goto Cleanup;
	}


	// Generate the key from supplied input key bytes.
	if (!NT_SUCCESS(status = BCryptGenerateSymmetricKey(
		hAesAlg,
		&hKey,
		pbKeyObject,
		cbKeyObject,
		(PBYTE)key,
		sizeof(key),
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptGenerateSymmetricKey\n", status);
		goto Cleanup;
	}

    //
    // Get the output buffer size.
    //
    if(!NT_SUCCESS(status = BCryptEncrypt(
                                        hKey, 
                                        (PUCHAR)flag.c_str(), 
                                        (ULONG)flag.length() + 1,
                                        NULL,
										iv,
                                        cbBlockLen,
                                        NULL, 
                                        0, 
                                        &cbCipherText, 
                                        BCRYPT_BLOCK_PADDING)))
    {
        wprintf(L"**** Error 0x%x returned by BCryptEncrypt\n", status);
        goto Cleanup;
    }

    pbCipherText = (PBYTE)HeapAlloc (GetProcessHeap (), 0, cbCipherText);
    if(NULL == pbCipherText)
    {
        wprintf(L"**** memory allocation failed\n");
        goto Cleanup;
    }

    // Use the key to encrypt the plaintext buffer.
    // For block sized messages, block padding will add an extra block.
    if(!NT_SUCCESS(status = BCryptEncrypt(
                                        hKey, 
                                        (PUCHAR)flag.c_str(), 
										(ULONG)flag.length() + 1,
                                        NULL,
                                        iv,
                                        cbBlockLen, 
                                        pbCipherText, 
                                        cbCipherText, 
                                        &cbData, 
                                        BCRYPT_BLOCK_PADDING)))
    {
        wprintf(L"**** Error 0x%x returned by BCryptEncrypt\n", status);
        goto Cleanup;
    }

	f = CreateFile(file_flag.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL);
	if (f != INVALID_HANDLE_VALUE) {
		if (WriteFile(f, pbCipherText, cbCipherText, &bytesWritten, NULL)) {
        	wprintf(L"Written flag ciphertext to %s\n", file_flag.c_str());
		}
		else {
			std::cout << "Error writting flag ciphertext. GetLastError: " << std::hex << GetLastError();
		}
		CloseHandle(f);
	}
	else {
		std::cout << "GetLastError: " << std::hex << GetLastError();
	}

    //
    // Get the output buffer size.
    //
    if(!NT_SUCCESS(status = BCryptEncrypt(
                                        hKey, 
                                        (PUCHAR)encrypted_hint.c_str(), 
                                        (ULONG)encrypted_hint.length() + 1,
                                        NULL,
                                        iv,
                                        cbBlockLen,
                                        NULL, 
                                        0, 
                                        &cbCipherText, 
                                        BCRYPT_BLOCK_PADDING)))
    {
        wprintf(L"**** Error 0x%x returned by BCryptEncrypt\n", status);
        goto Cleanup;
    }

    pbCipherText = (PBYTE)HeapAlloc (GetProcessHeap (), 0, cbCipherText);
    if(NULL == pbCipherText)
    {
        wprintf(L"**** memory allocation failed\n");
        goto Cleanup;
    }

    // Use the key to encrypt the plaintext buffer.
    // For block sized messages, block padding will add an extra block.
    if(!NT_SUCCESS(status = BCryptEncrypt(
                                        hKey, 
                                        (PUCHAR)encrypted_hint.c_str(), 
                                        (ULONG)encrypted_hint.length() + 1,
                                        NULL,
                                        iv,
                                        cbBlockLen, 
                                        pbCipherText, 
                                        cbCipherText, 
                                        &cbData, 
                                        BCRYPT_BLOCK_PADDING)))
    {
        wprintf(L"**** Error 0x%x returned by BCryptEncrypt\n", status);
        goto Cleanup;
    }

	f = CreateFile(file_unencrypted_hint.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL);
	if (f != INVALID_HANDLE_VALUE) {
		if (WriteFile(f, unencrypted_hint.c_str(), (ULONG)unencrypted_hint.size(), &bytesWritten, NULL)) {
        	wprintf(L"Written hint to %s\n", file_unencrypted_hint.c_str());
		}
		else {
			std::cout << "Error writting hint. GetLastError: " << std::hex << GetLastError();
		}
		CloseHandle(f);
	}
	else {
		std::cout << "GetLastError: " << std::hex << GetLastError();
	}

	f = CreateFile(file_encrypted_hint.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL);
	if (f != INVALID_HANDLE_VALUE) {
		if (WriteFile(f, pbCipherText, cbCipherText, &bytesWritten, NULL)) {
        	wprintf(L"Written hint to %s\n", file_encrypted_hint.c_str());
		}
		else {
			std::cout << "Error writting hint ciphertext. GetLastError: " << std::hex << GetLastError();
		}
		CloseHandle(f);
	}
	else {
		std::cout << "GetLastError: " << std::hex << GetLastError();
	}

Cleanup:

	if (hAesAlg)
	{
		BCryptCloseAlgorithmProvider(hAesAlg, 0);
	}

	if (hKey)
	{
		BCryptDestroyKey(hKey);
	}

	if (pbCipherText)
	{
		HeapFree(GetProcessHeap(), 0, pbCipherText);
	}

	if (pbKeyObject)
	{
		HeapFree(GetProcessHeap(), 0, pbKeyObject);
	}
	return 0;
}