cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_FLAGS_RELEASE "/MT")
set(CMAKE_CXX_FLAGS_DEBUG "/MTd")

project(setup)
add_executable(setup setup.cpp)
add_definitions(-DUNICODE -D_UNICODE)
target_link_libraries(setup bcrypt pathcch)