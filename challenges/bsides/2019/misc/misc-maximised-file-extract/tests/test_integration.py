import time
import logging
import pytest
import math
import socket
import errno
from multiprocessing import Process
from multiprocessing import Queue
from solution.solution import solve_problem
from server import main as run_server
from server import CHALLENGE_FLAG
from server_settings import HOST_SOCKET
from maximum_extract_challenge.challenge import MaximumExtractProblem, \
    INVALID_INPUT_ERROR, GREEDY_SOLUTION_DETECTED_ERROR, \
    FILE_VALUE_NOT_MAXIMISED_ERROR, DUPLICATE_SELECTION_ERROR


logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(f'TESTER')


def solution_worker(result_queue, worker_number=None,
                    alternate_message=None, delay=None):
    """
    Wrap the solution function as a worker so we can turn
    it into a separate process that returns a result via
    a multiprocessing Queue.
    :param result_queue:
    :param worker_number:
    :param alternate_message:
    :param delay:
    :return:
    """
    result = solve_problem(alternate_message, delay)
    result_queue.put((worker_number, result))


class TestBasicFunction(object):
    """
    Test that a single user can solve the problem and that some garbage values
    sent in are rejected (no weird surprise flag returns).

    Also make sure the breadcrumbs exist in the return messages as well.
    """
    def setup_class(self):
        MaximumExtractProblem.DIFFICULTY_SCALE = 1.2

        # Test if the socket is available, and retry if it isn't
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(("127.0.0.1", HOST_SOCKET))
            sock.close()
            self.server_process = Process(target=run_server)
            self.server_process.start()
            time.sleep(1)  # Give the server a sec to start before continuing
        except socket.error as ex:
            if ex.errno == errno.EADDRINUSE:
                LOGGER.error("Port is already in use... waiting 5 seconds and"
                             " retrying...")
                time.sleep(5)
                self.setup_class(self)
            else:
                # something else raised the socket.error exception
                LOGGER.error(f'{ex}')
                raise

    def teardown_class(self):
        self.server_process.terminate()
        self.server_process.join()

    def test_invalid_answer(self, caplog):
        """
        Test we don't randomly give out the flag with any answer
        :return:
        """
        caplog.set_level(logging.DEBUG)
        message = 'THIS_IS_A_WRONG_ANSWER, SO_IS_THIS'
        server_feedback = solve_problem(alternate_message=message)
        assert (INVALID_INPUT_ERROR == server_feedback)

    def test_duplicate_answer(self, caplog):
        """
        Test we don't randomly give out the flag with any answer
        :return:
        """
        caplog.set_level(logging.DEBUG)
        message = '/donald/covfefe.msg, /donald/covfefe.msg'
        server_feedback = solve_problem(alternate_message=message)
        assert (DUPLICATE_SELECTION_ERROR == server_feedback)

    def test_unmaximised_answer(self, caplog):
        """
        Test we don't randomly give out the flag with any answer
        :return:
        """
        caplog.set_level(logging.DEBUG)
        message = '/donald/covfefe.msg'
        server_feedback = solve_problem(alternate_message=message)
        assert (FILE_VALUE_NOT_MAXIMISED_ERROR == server_feedback)

    def test_greedy_answer(self, caplog):
        """
        Check to see if the server gives a hint when a greedy solution is
        used.
        :return:
        """
        caplog.set_level(logging.DEBUG)
        server_feedback = solve_problem(greedy=True)
        assert (GREEDY_SOLUTION_DETECTED_ERROR == server_feedback)

    def test_dynamic_answer(self, caplog):
        """
        Test that the problem can be solved normally
        :return:
        """
        caplog.set_level(logging.DEBUG)
        server_feedback = solve_problem()
        assert (CHALLENGE_FLAG == server_feedback)

    def test_big_string(self, caplog):
        """
        Test that a massive string doesn't end up returning the flag?
        :return:
        """
        caplog.set_level(logging.INFO)
        message = 'a' * 10000000
        server_feedback = solve_problem(alternate_message=message)
        assert ('BROKEN_PIPE' == server_feedback)

    def test_too_slow(self, caplog):
        """
        Test you don't get the flag for being slow.
        :return:
        """
        caplog.set_level(logging.INFO)
        server_feedback = solve_problem(delay=1.0)
        assert (CHALLENGE_FLAG != server_feedback)

    def test_exec(self, caplog):
        """
        Test we don't accidentally run exec code
        :return:
        """
        caplog.set_level(logging.INFO)
        server_feedback = solve_problem(
            alternate_message='exec("exit()")')
        assert (CHALLENGE_FLAG != server_feedback)

        # Test that it still works
        server_feedback = solve_problem()
        assert (CHALLENGE_FLAG == server_feedback)

    def test_eval(self, caplog):
        """
        Test we don't accidentally run eval code
        :return:
        """
        caplog.set_level(logging.INFO)
        server_feedback = solve_problem(
            alternate_message='eval("exec("exit()")")')
        assert (CHALLENGE_FLAG != server_feedback)

        # Test that it still works
        server_feedback = solve_problem()
        assert (CHALLENGE_FLAG == server_feedback)


class TestServerConcurrency(object):
    """
    Test the server can have multiple people connected at the same time
    and most of those people can solve the problem (we'll be generous here and
    make it a little easier but adjusting the DIFFICULTY_SCALE to cope with
    server load)
    """

    def setup_class(self):
        # Be a little lenient as the load of multiple processes might not go
        # so well...
        MaximumExtractProblem.DIFFICULTY_SCALE = 100

        # Test if the socket is available, and retry if it isn't
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(("127.0.0.1", HOST_SOCKET))
            sock.close()
            self.server_process = Process(target=run_server)
            self.server_process.start()
            time.sleep(1)  # Give the server a sec to start before continuing
        except socket.error as ex:
            if ex.errno == errno.EADDRINUSE:
                LOGGER.error("Port is already in use... waiting 5 seconds and"
                             " retrying...")
                time.sleep(5)
                self.setup_class(self)
            else:
                # something else raised the socket.error exception
                LOGGER.error(f'{ex}')
                raise

    def teardown_class(self):
        self.server_process.terminate()
        self.server_process.join()

    def test_many_solves(self, caplog):
        """
        Test that the problem can be solved correctly concurrently by a
        bunch of connections
        :return:
        """
        caplog.set_level(logging.INFO)
        client_count = 25
        result_queue = Queue()
        clients = []

        # Create a bunch of connections and solve the problem
        for client_no in range(client_count):
            client = Process(target=solution_worker, args=(result_queue,
                                                           client_no,
                                                           None,
                                                           None,))

            LOGGER.info(f'Starting client {client_no}')
            clients.append(client)
            client.start()

        for client in clients:
            client.join()

        # Count up how many connections got the flag
        successful_count = 0
        while not result_queue.empty():
            worker_number, result = result_queue.get_nowait()
            LOGGER.info((worker_number, result))

            if result == CHALLENGE_FLAG:
                successful_count += 1

        # Assert that >= 80% make it through with the load (given the client
        # and server are all running on the same machine this is OK.
        assert (successful_count >= client_count * 0.8)
