# MAXMISED FILE EXTRACT

## Handout

None

## Running

Build the container and it'll be listening for socket 
TCP connections on a port.

<details>
<summary>SPOILERS</summary>
<p>

## Premise

There are a bunch of files that have different sizes and are of differing 
"intelligence" value to us. 

We want to maximise the total amount of value we get by choosing a set of files
that both fit inside the download limit and maximises the total value score 
for all the files we've selected. 

The challenge is a classic NP-hard algorithm problem. When the user connects, 
an "agent" reports back a list of files with sizes and values that we can 
choose from.  

In the background, the server solves the created problem algorithm and times 
how long it took to solve it. The time taken by the server is then used as a
benchmark that the connected client needs to solve the problem within otherwise
they get disconnected.

Each connection has randomised file_size and file_value values.

## Difficulty

This puzzle is a medium/hard algorithms challenge for the following reasons:

- When the server creates the problem, it checks that it cannot be solved by
a greedy algorithm implementation (if it can, it retries creating the puzzle).
- Because it's an NP-Hard complexity problem, brute forcing won't work within
the time limit.
- The puzzle is randomised each time (you can't take the puzzle away and 
come back with the answer).

In order to guide the person playing a couple of clues are embedded in the
artwork and also in the interaction with the player. They are:

- Subtle clue on the ASCII disk "0-1 SAC" symbolic for 0-1 knapsack; which is
the best way to solve the problem.
- When a user submits garbage, it indicates that they should submit a comma
separated list of the file names.
- When a user submits duplicate files in a list it tells them they're not
allowed duplicates ("No duplicates allowed").
- When a user solves the problem sub-optimally the server prints back 
"csc282//C16" in the return message; which when googled takes the searcher 
directly to [this pdf](https://www.cs.rochester.edu/~gildea/csc282/slides/C16-greedy.pdf)
which is a set of lecture notes on greedy algorithms (a possible solution that
they may want to try).
- When a user solves the problem in a greedy way, the server prints out 
"RAND Corporation P550" in it's return message. When you look this up on 
google it takes you to [the original paper](https://www.rand.org/pubs/papers/P550.html)
discussing the theory of Dynamic Programming mathematics (a little treat for 
the history buffs).

If the user finds all of the clues they know that it is (assuming they've not
worked out what the problem is yet):
- A Dynamic Programming problem.
- Involves maximising item value in a limited space.
- Does not allow duplicates.

Searching for combinations of "Dynamic Programming " and "No duplicates" leads 
to a whole raft of results for the "0-1 Knapsack" algorithm which is the ideal
way to solve the problem.

The rest is just an engineering (copy paste from the web) problem.

</p>
</details>