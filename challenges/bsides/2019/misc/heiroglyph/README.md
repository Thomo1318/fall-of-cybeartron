Heiroglyphics
=============

* _title_: Heiroglyphics
* _points_: 100
* _tags_: story, misc, crypto

## Flags
* `cybears{CONTRACTUALLY OBLIGED}`
* `cybears{CONTRACTUALLYOBLIGED}`
* `CONTRACTUALLY OBLIGED`
* `CONTRACTUALLYOBLIGED`
* (and lower case versions of the above)

## Challenge Text
```markdown
We have discovered what look like ancient Cybeartronian heiroglyphs while excavating our defensive bunker. All records of this script have been lost to the ages, and there is no-one alive that remembers how to read it.

Can you help?
```

## Attachments
* `AncientRunes.jpg` : Image of ancient cybertronian heiroglyphics

## Hints
*

## References
*

## Notes - Solve
* The "Heiroglyphs" are the "code" font used in ASD's new branding
* Cre8tive is the canberra design company that put together the imagery and branding for the campaign. They used to have ASD in their online portfolio, but it looks like this was requested to be removed. Searching for "site:www.cre8ive.com.au asd" allows you to view a cached web version of the code (i've saved a copy of the image here locally as cre8tive_code_font.jpeg)
* Another was to solve is to purely look at it as a two-symbol monoalphabetic substitution cipher and solve from there. Realising it is the ASD font should also help as the ASD annual review has it on the front cover, presumably spelling out "Australian Signals Directorate" which should give code breakers a good start.
* There should be enough text to be able to do freq counts/solve a simple sub, but not enough to put people off
* I've confirmed that if they write out an arbitrary symbol-to-character mapping and use an online simple substitution solver, it will come out correctly.

## Notes - Creating the image
* Using ImageMagick's "covert" tool, you can provide a font file and create an image
* `convert -pointsize 72 -font ./Signal-Expanded.otf label:@./AncientRunes.txt  AncientRunes.jpg`
