# In a bit of a pickle

# Handout

The handout is a sample yaml file.

# Running

Just build and run the docker container, it will be listening on a port (default is 2323).

<details><summary>SPOILERS</summary>
<p>

# Premise

Not many people know this, but you should always use `load_safe` when loading untrusted yaml with Python.
There's a yaml directive that allows you to get code exec. This challenge is a server that takes a yaml
file, decodes it, pickles it and returns the result.

Players don't have access to the code. They'll need to determine that it is a pickle that is returned.
Using pickle is a red herring, they can't control the serialize function on the yaml object, but it
should prompt them that this is a serialization issue.

# Difficulty

This is an easy challenge is you know the trick, but a bit of research into pickle should prompt
them that this is a serialization bug. Reading the python docs for yaml should reveal the issue.

If you google `python yaml safe` you should get a page on this bug.

</p>
</details>
