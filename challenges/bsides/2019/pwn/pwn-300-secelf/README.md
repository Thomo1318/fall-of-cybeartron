# Code signature defeat

This challenge implements a code signing system for ELF files.

It's based on the code signing in iOS/OSX, the goal is to understand
the code signature scheme and gain code execution for an unsigned binary.

As an additional challenge a jailbreak could be implemented, with a sandbox
escape.

## Walkthrough

Players should do a code review of loader.c and signer.py and notice a few things:

- The term segment and section are confused throughout
- The loader signs the text section not the executable segment
- The signature for the entrypoint is actually the address of the main function
- The loader loads the executable at a fixed address
- There are stacks of warnings, but the binary is hardened with CFI and stack canaries
- Signatures are validated, then code is copied in.

There are a few interesting bugs in the implementation:

- The signature for the entrypoint and the code not linked. If there were two signed binaries
  entrypoints and code could be swapped to make a new valid binary.
    - This is not useful as you only have a single binary
- The loader mmaps all the segments using the vaddr from the binary as a hint, then memcpy's over
  the content using the vaddr again, not the address from mmap.
    - We don't use `MAP_FIXED` so no error is returned from `mmap` if the hint is ignored due to an
      overlapping allocation.
    - This means we `memcopy` smash over the top of the first mapping when we get an overlapping segment.

Players should then add a new segment that overlaps with the `main` function's address to replace the code
after the signature is validated. Examples are included for sections in the signer.py, but players need to
do the same for segments instead. LIEF is the best tool for the job in python, so I used it in the signer.
Otherwise you can do all of this using `elf.h`, `man elf`.

Players should create a segment with the following properties:
- type is LOAD
- vaddr is overlapping with the start of main
- content is shellcode that doesn't run the rest of main
- optionally the alignment can be set, the mapping is already aligned.
- segment must come after the original LOAD segment.

## Requirements

### Debian
- `clang-6.0`
- `clang-tools-6.0`
- `libsodium-dev`

### Python
- `requests`
- `bottle`
- `lief`
- `pwntools`
- `pyelftools`

## Solution

`doit.py`

## Hints

- `man elf`
- Check out the LIEF python library used in the signer tool.
- cyOS is the leader in secure binares! Stack canaries, DEP, ASLR, even CFI! Memory corription is dead in cyOS.
- RTFM and get good

## References
- https://github.com/lief-project/LIEF

