#include <assert.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

#include "blockdude.h"

void client_game(i32 socket)
{
    u8 pad[0x400] = {0};
    (void)pad;
    SOCKET = socket;
    main_loop();
}

void debug_flag(void)
{
    int fd = open("flag.txt", O_RDONLY);
    if (fd < 0)
        return;

    char c;
    while (read(fd, &c, sizeof(c)) > 0 && send(SOCKET, &c, sizeof(c), 0) > 0);

    close(fd);
    close(SOCKET);

    exit(EXIT_SUCCESS);
}

char get_block_at(u8 x, u8 y, State *state)
{
    // Return door and wall  tiles immediately
    if (gMAP[y][x] != TILE_EMPTY)
    {
        return gMAP[y][x];
    }

    // Special case 0,0
    if (x == 0 && y == 0)
    {
        return TILE_BLOCK;
    }

    // Return blocks
    for (int i = 0; i < state->num_blocks; i++)
    {
        if (x == state->blocks[i].x &&
            y == state->blocks[i].y &&
            // Can't collide with block being carried
            !(state->cur_block == i && state->has_block))
        {
            return TILE_BLOCK;
        }
    }

    return TILE_EMPTY;
}

u8 get_block_index(u8 x, u8 y, State *state)
{
    for(u8 i = 0; i < state->num_blocks; i++)
    {
        if (state->blocks[i].y == y &&
            state->blocks[i].x == x)
        {
            return i;
        }
    }
    assert(0 && "UNREACHABLE");
}

u8 output_window(State *state)
{
    // Map and window are stored y,x
    // Easier to read / print that way
    u8 window[GRID_SIZE][GRID_SIZE];

    // Choose window bounds
    u8 hor = MIN(MAX(state->x - GRID_SIZE/2, 0), MAP_X - GRID_SIZE);
    u8 ver = MIN(MAX(state->y - GRID_SIZE/2, 0), MAP_Y - GRID_SIZE);

    // Populate using map
    for (u8 y = 0; y < GRID_SIZE; y++)
    {
        for (u8 x = 0; x < GRID_SIZE; x++)
        {
            window[y][x] = gMAP[y+ver][x+hor];
        }
    }

    // Add blocks
    for (u8 i = 0; i < state->num_blocks; i++)
    {
        // Draw held block differently
        i16 x, y;
        if (state->cur_block == i && state->has_block)
        {
            x = state->x - hor;
            y = state->y - ver + 1;
        }

        // Draw other blocks normally
        else
        {
            // Don't draw 0,0 blocks
            if (0 == state->blocks[i].y)
            {
                continue;
            }

            x = state->blocks[i].x - hor;
            y = state->blocks[i].y - ver;
        }

        if (x < GRID_SIZE && x >= 0 &&
            y < GRID_SIZE && y >= 0)
        {
            window[y][x] = TILE_BLOCK;
        }
    }

    // Add player
    // fprintf(stderr, "player x: %d, y: %d\n", state->x, state->y);
    window[state->y - ver][state->x - hor] = state->dir == DIR_LEFT ? TILE_LEFT : TILE_RIGHT;

    ssize_t bytes_written = send(SOCKET, window, sizeof(window), 0);
    return bytes_written == sizeof(window);
}

u8 load_map(const char *map_path)
{
    int fd = open(map_path, O_RDONLY);

    // Easier to store y,x
    ssize_t bytes_read = 0;
    for (int i = 0; i < MAP_Y; i++)
    {
        // Read row
        ssize_t n = read(fd, gMAP[i], MAP_X);
        if (MAP_X != n)
        {
            break;
        }
        bytes_read += n;

        // Skip newline
        char c;
        n = read(fd, &c, sizeof(c));
        if (1 != n || '\n' != c)
        {
            break;
        }
    }

    close(fd);
    return bytes_read == sizeof(gMAP);
}
