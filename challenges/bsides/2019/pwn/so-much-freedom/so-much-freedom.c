
#include <stdio.h>
#include <string.h>

#define NAME_LEN 0x200
void main(void)
{
    char input[NAME_LEN+1];
    char *readString;

    setvbuf(stdin, 0LL, 2LL, 0LL);
    setvbuf(stdout, 0LL, 2LL, 0LL);

    puts("Would you like to  experience true freedom?\nIt's simple just give me your name.\nFirst Name: "); 
    readString = fgets(input, NAME_LEN, stdin);

    if(readString != NULL) {
        size_t firstNameLen = strlen(readString);
        printf("Thanks "); 
        printf(input); 
        printf("\nLast Name: "); 
        readString = fgets(input + firstNameLen, NAME_LEN, stdin);
        if(readString != NULL) {
            printf("Congratulations %s %s, you have now experienced true freedom.\nSurely nothing will go wrong from here\n", input, readString); 
        }
    }
}
