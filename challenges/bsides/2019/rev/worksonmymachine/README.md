# Works on MY machine

## Build

Use the included Makefile to build Works On My Machine. The build uses mingw-gcc to compile a Windows binary on Linux, and Python to do the post-build obfuscation step.

## Solution

### Obfuscation

The ".ctf" section in the PE is obfuscated by XORing with the CPU vendor string. The CPUID instruction is used to get the vendor string.

If the CPUID returns "GenuineTMx86" (for a Transmeta x86 CPU), the code will be deobfuscated correctly. Otherwise the program will crash.

The intent of this is to get the player to go to the Wikipedia page for "CPUID" and look at the possible values for the CPU vendor. "GenuineTMx86" was chosen because:

 - Pretty good odds that nobody has a Transmeta CPU
 - The first half of the CPUID string is the same on Intel processors (GenuineIntel), so it will partially deobfuscate on some CPUs and give a bit of a hint as to the correct key.

### Virtual Machine

The binary implements a custom virtual machine with one of the opcodes not properly implemented. Possible solutions:

 - Patch the binary to implement the missing instruction
 - Reimplement the VM in Python/etc.
 - Write a DLL that implements the missing instruction

The VM itself is pretty simple, it has a set of registers, a stack, and reads the program from a constant buffer.

Instructions:

 - Exit
 - Push DWORD
 - Push register value
 - Pop to register
 - XOR register 1 with register 2, store in register 1
 - Move DWORD from register to output buffer + offset
 - Move DWORD from output buffer + offset to register
   - This instruction is broken, it always writes "LOL " instead.

If an instruction is unhandled, the binary will load VMExtensions.dll and call the exported function CanDo to check if the DLL has an implementation of the missing opcode. If the function returns true it will call the Do function, passing it the current instruction, a pointer to the program, a pointer to the VM state structure (containing all of the registers), and a pointer to the output string.
   
#### Solution: using patching

Patch the instruction code to replace the NOP sled with code that will:

    1. Fetch the first operand: the register number (eg. operand1 = g_Program[IP + 1]
    2. Fetch the value of the register (eg. value = registers[operand1 * 4])
    3. Write the value to the output (eg. output[operand2] = value)

#### Solution: writing a DLL

Write a DLL that exports two functions:

    1. a function called "CanDo" that returns True if the DLL 
    2. a function called "Do" that will do the same as above
        1. The function is passed the current opcode, a pointer to the VM state structure (containing all of the registers and the stack), and a pointer to the output string. You will need to reverse engineer the layout of the state structure to find the registers etc.

## Flag

```
cybears{its_@11_ab0ut_t3h_P3NT1UMS}
```