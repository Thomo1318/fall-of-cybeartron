VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTrackingProgress
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "TRACKING IP ADDRESS IN PROGRESS"
   ClientHeight    =   780
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6045
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   780
   ScaleWidth      =   6045
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1
      Interval        =   100
      Left            =   5640
      Top             =   120
   End
   Begin MSComctlLib.ProgressBar ProgressBar1
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   873
      _Version        =   393216
      Appearance      =   1
      Max             =   10
   End
End
Attribute VB_Name = "frmTrackingProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public IPAddress As String

Private Sub Timer1_Timer()
ProgressBar1.Value = ProgressBar1.Value + 1
If ProgressBar1.Value = 10 Then

    ' Determine which screen to display
    If Len(IPAddress) = 7 And IPAddress Like "[1I]* [i1]n*" And IPAddress Like "I'm*" And Right(IPAddress, 1) = "!" Then
        frmAccessGranted.IPAddress = IPAddress
        frmAccessGranted.Show vbModal, Me
    Else
        frmAccessDenied.Show vbModal, Me
    End If
    Unload Me
End If

End Sub
