package io.cybears.rev.passwordchecker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        //TextView tv = (TextView) findViewById(R.id.sample_text);
        //tv.setText(stringFromJNI());
    }


    public void checkPassword(View view){

        TextView textView = findViewById(R.id.editText);
        String password = textView.getText().toString();

        Toast toast = Toast.makeText(getApplicationContext(),
                "RESULT:" + checkPassword3(password),
                Toast.LENGTH_SHORT);
        toast.show();

    }
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native String checkPassword(String s);
    public native String checkPassword2(String s);
    public native String checkPassword3(String s);


}
