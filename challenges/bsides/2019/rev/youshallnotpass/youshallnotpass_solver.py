import angr
import claripy
import sys
import tempfile
import zipfile

flag = b'cybears{RU_SAT-15f13d?}'

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: youshallnotpass_solver.py PasswordChecker.apk")
        exit(-1)

    infile = sys.argv[1]
    with tempfile.NamedTemporaryFile() as native_so:
        with zipfile.ZipFile(infile, "r") as z:
            native_so_data = z.read("lib/arm64-v8a/libnative-lib.so")
        native_so.write(native_so_data)
        native_so.flush()
        proj = angr.Project(native_so.name)

    initial_state = proj.factory.blank_state(addr=0x40c600)
    concrete_addr = 0xffe00000
    password = claripy.BVS("password",0x17*8)

    initial_state.add_constraints(password.get_byte(0) == b'c')
    initial_state.add_constraints(password.get_byte(1) == b'y')
    initial_state.add_constraints(password.get_byte(2) == b'b')
    initial_state.add_constraints(password.get_byte(3) == b'e')
    initial_state.add_constraints(password.get_byte(4) == b'a')
    initial_state.add_constraints(password.get_byte(5) == b'r')
    initial_state.add_constraints(password.get_byte(6) == b's')

    initial_state.memory.store(concrete_addr, password, endness='Iend_BE')
    initial_state.regs.x20 = concrete_addr
    initial_state.regs.x19 = concrete_addr
    #create a path group using the created initial state
    sm = proj.factory.simulation_manager(initial_state)
    sm.explore(find=0x40c7f8, avoid=[0x40c874])
    found = sm.found[0]

    solution = found.solver.eval(password, cast_to=bytes)
    assert solution == flag, solution
    print("Found solution %r" % solution)
