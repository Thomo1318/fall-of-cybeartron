; Bootloader
; This will be loaded at 0x7E00

BITS 16
ORG 0x7E00

KEY_LENGTH                      equ     8
ONE_SECOND                      equ     18      ; ticks
A_FEW_SECONDS                   equ     5       ; seconds

; Location of flag file on disk
; Disk geometry:
; Tracks: 80
; Sectors/Track: 18
; Heads: 2
; Total sectors: 2880
; 0x4200 -> 33 -> sector 34
; 

FLAG_FILE_LOCATION_SECTOR       equ     16
FLAG_FILE_LOCATION_HEAD         equ     1
FLAG_FILE_LOCATION_TRACK        equ     0
FLAG_FILE_SIZE_SECTORS          equ     1       ; sector(s)

; Load the flag file into this address
FLAG_BUFFER                     equ     0xa000

setup_environment:
    sti                     ; interrupts on (required for time-related stuff)
    cld                     ; clear direction flag
    xor ax, ax              ; Set all segments to 0
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov bx, 0x7D00          ; Set stack pointer
    mov sp, bx
    
main_read_key:
    ; Read the key file
    call read_flag_file
    test al, al
    jnz main_done
    
    ; Check if the key file is already encrypted
    mov si, FLAG_BUFFER
    mov al, [si]
    cmp al, 'T'
    jnz already_decrypted

main_show_message:

    ; Display the "enter key" message
    push encrypted_disk_message
    call print_message
    
    ; Prompt the user for the key
main_enter_key_start:
    xor bx, bx
    mov cx, KEY_LENGTH
    mov si, key_buffer
main_enter_key_loop:
    ; Read a key from the keyboard
    xor ah, ah
    int 16h
    mov [bx+si], al
    
    ; Print the letter
    mov ah, 0x0e
    ; mov al, '*'
    int 10h
    
    inc bx
    loop main_enter_key_loop
    
main_enter_key_done:

    ; write a new line
    mov ax, 0x0e0d      ; ah = 0xe, al = '\r'
    int 10h
    mov ax, 0x0e0a      ; ah = 0xe, al = '\n'
    int 10h

    ; Check the key is valid
    push key_buffer
    call validate_key
    test al, al
    jz main_bad_key
    
main_good_key:
    ; print "valid key"
    push valid_key_message
    call print_message
    
    ; Decrypt the key file
    xor bx, bx
    
    
decrypt_loop_test:
    cmp bx, FLAG_FILE_SIZE_SECTORS * 512
    jz decrypt_done
    
    ; Get the current position in key
    mov dx, bx
    ; dx = pos % key length (8)
    and dx, 7
    
    ; Get the current byte of the key
    mov si, key_buffer
    add si, dx
    mov al, [si]
    
    ; Get the current byte of the flag file
    mov di, FLAG_BUFFER
    mov ah, [di+bx]

    ; skip if source byte is null or equals the key byte
    test ah, ah
    jz decrypt_loop_inc
    cmp ah, al
    jz decrypt_loop_inc

    xor ah, al

    ; Write back to the flag buffer
    mov [di+bx], ah
decrypt_loop_inc:
    inc bx
    jmp decrypt_loop_test
    
decrypt_done:

    ; Write the key to disk
    mov ah, 3   ; INT 13h, 3: write sectors to drive
    mov al, FLAG_FILE_SIZE_SECTORS
    mov ch, FLAG_FILE_LOCATION_TRACK
    mov cl, FLAG_FILE_LOCATION_SECTOR
    mov dh, FLAG_FILE_LOCATION_HEAD
    xor dl, dl
    mov bx, FLAG_BUFFER
    int 13h
    
    ; check if something bad happened
    jc show_reboot_message
    
    ; Print success message
    push unlocked_message
    call print_message
        
    jmp main_done

main_bad_key:
    ; Print "invalid key"
    push invalid_key_message
    call print_message
    
main_done:

    ; Wait for a few seconds
    push ONE_SECOND * A_FEW_SECONDS
    call wait_ticks

    ; Try to power off the machine
    ;Connect to APM API
    MOV     AX,0x5301
    XOR     BX,BX
    INT     0x15

    ;Try to set APM version (to 1.2)
    MOV     AX, 0x530E
    XOR     BX, BX
    MOV     CX, 0x0102
    INT     0x15

    ;Turn off the system
    MOV     AX,0x5307
    MOV     BX, 1
    MOV     CX, 3
    INT     0x15

    ; Failed to shut down, so prompt the user to reboot
    call show_reboot_message

already_decrypted:
    ; print already decrypted message
    push already_unlocked_message
    call print_message
    jmp main_done
    
; ====================== FUNCTIONS ======================

; bool __stdcall validate_key(char * key)
; AL = non-zero if key is valid
validate_key:
    push bp
    mov bp, sp
    xor al, al
    mov si, [bp+4]
    
    ; Include the generated key validation code
%include "validate_key.asm"

validate_key_success:
    inc al
validate_key_fail:
    pop bp
    retn 2

look_busy:
    pusha
    mov ax, 0x0e2e      ; ah = 0xe, al = '.'
    int 10h
    push 3
    call wait_ticks
    popa
    ret
    

; void __stdcall print_message(char * message)
; XORs message and prints it
print_message:
    push bp
    mov bp, sp
    mov si, [bp+4]          ; si = ptr to string to print
    mov ah, 0x0e            ; int 10h + ah = 0xe : teletype output
print_message_loop:
    lodsb                   ; get next char
    xor al, 0x42
    or al, al               ; check if it's a null
    jz print_message_done
    int 10h                 ; write char
    jmp print_message_loop
print_message_done:
    pop bp
    retn 2


; void __stdcall wait_ticks(int ticks)
; ticks = 1/18th of a second
wait_ticks:
    push bp
    mov bp, sp
    mov cx, [bp+4]  ; parameter
    xor ax, ax
    mov ds, ax
wait_ticks_loop_enter:
    mov ax, [0x46C]     ; this BIOS timer is incremented every 1/18th of a second
    
wait_ticks_loop_check:    
    mov bx, [0x46C]     ; repeatedly fetch the timer value until it changes
    cmp ax, bx
    je wait_ticks_loop_check
    
    loop wait_ticks_loop_enter
    pop bp
    retn 2


; void show_reboot_message()
show_reboot_message:
    push reboot_message
    call print_message
    
    ; Read a key
    xor ah, ah
    int 16h
    ; Reboot
    int 19h
    ; how did you get here?
spin:
    jmp spin
    
; void __stdcall read_flag_file()
read_flag_file:
    pusha
    
    mov si, 10      ; max retries
    
read_flag_file_reset_disk:
    
    ; Reset the disk
    xor ah, ah  ; INT 13h, 0: Reset disk system
    xor dl, dl  ; Disk number: 0 = A:
    int 13h
    jc read_flag_file_reset_disk
    
    ; Now read the file
    xor ax, ax
    mov ah, 2   ; INT 13h, 2: Read disk sectors
    mov al, FLAG_FILE_SIZE_SECTORS
    mov ch, FLAG_FILE_LOCATION_TRACK
    mov cl, FLAG_FILE_LOCATION_SECTOR
    mov dh, FLAG_FILE_LOCATION_HEAD
    xor dl, dl      ; Disk number: 0 = A:
    mov bx, FLAG_BUFFER
    int 13h
    jnc read_flag_file_succeeded
    
    ; Try again?
    
    
    dec si
    jnz read_flag_file_reset_disk
    
    ; No more retries
    ; return 0
    xor ax, ax
    
    jmp read_flag_file_epilog
    
    
read_flag_file_succeeded:
    ; return 1
    xor ax, ax
    inc ax

read_flag_file_epilog:
    popa
    ret
    
    
; ====================== STRINGS ======================

invalid_key_message:
; \r\nINVALID KEY\r\n
db 0x4f, 0x48, 0x0b, 0x0c, 0x14, 0x03, 0x0e, 0x0b, 0x06, 0x62, 0x09, 0x07, 0x1b, 0x4f, 0x48, 0x42
valid_key_message:
; \r\nVALID KEY\r\n
db 0x4f, 0x48, 0x14, 0x03, 0x0e, 0x0b, 0x06, 0x62, 0x09, 0x07, 0x1b, 0x4f, 0x48, 0x42
reboot_message:
; Press any key to reboot
db 0x12, 0x30, 0x27, 0x31, 0x31, 0x62, 0x23, 0x2c, 0x3b, 0x62, 0x29, 0x27, 0x3b, 0x62, 0x36, 0x2d, 0x62, 0x30, 0x27, 0x20, 0x2d, 0x2d, 0x36, 0x6c, 0x42
unlocked_message:
;\r\nDisk unlocked. 
db 0x4f, 0x48, 0x06, 0x2b, 0x31, 0x29, 0x62, 0x37, 0x2c, 0x2e, 0x2d, 0x21, 0x29, 0x27, 0x26, 0x6c, 0x62, 0x42

encrypted_disk_message:
; "\r\nCYBEARS ENCRYPTED DISK\r\nENTER KEY: "
db 0x4f, 0x48, 0x01, 0x1b, 0x00, 0x07, 0x03, 0x10, 0x11, 0x62, 0x07, 0x0c, 0x01, 0x10, 0x1b, 0x12, 0x16, 0x07, 0x06, 0x62, 0x06, 0x0b, 0x11, 0x09, 0x4f, 0x48, 0x07, 0x0c, 0x16, 0x07, 0x10, 0x62, 0x09, 0x07, 0x1b, 0x78, 0x62, 0x42

already_unlocked_message:
; "\r\nDISK ALREADY UNLOCKED!\r\n
db 0x4f, 0x48, 0x06, 0x0b, 0x11, 0x09, 0x62, 0x03, 0x0e, 0x10, 0x07, 0x03, 0x06, 0x1b, 0x62, 0x17, 0x0c, 0x0e, 0x0d, 0x01, 0x09, 0x07, 0x06, 0x63, 0x4f, 0x48, 0x42

key_buffer:
    times (KEY_LENGTH + 1) db 0
