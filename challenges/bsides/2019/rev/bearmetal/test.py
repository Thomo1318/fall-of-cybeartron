#!/usr/bin/env python
import re
import shutil
import subprocess
import sys
import time
import zipfile

import io
import pexpect

DEFAULT_PASSWORD = b"ZvbXrp1-"

BOOT_TIMEOUT = 60
UNLOCK_TIMEOUT = 30
SHUTDOWN_TIMEOUT = 30


def find_flag(message):
    m = re.search(b"cybears{(.+)}", message)
    return m.group(0)


def extract_file(image, filename="GARBAGE.TXT"):
    """
    Use Sleuthkit to extract a file from a disk image
    :param image: disk image
    :param filename: file to search for
    :return: raw file data
    """

    # Use fls to list inodes
    fls_output = subprocess.check_output(["fls", image]).splitlines()
    print(fls_output)
    inode = 0

    for line in fls_output:
        pattern = r"^.+ (\d+):\t{}$".format(filename).encode("iso-8859-1")
        m = re.search(pattern, line)
        if m:
            inode = int(m.group(1))

    if inode == 0:
        raise Exception("not found")

    # Use icat to extract the file
    file_contents = subprocess.check_output(["icat", image, "%s" % inode])
    return file_contents


def try_extract_flag_file(image_path):
    """
    Extract the garbage file and carve out the flag (if it is there)
    """
    print("Extracting garbage file")
    garbage_contents = extract_file(image_path)

    # Check if the garbage file is encrypted
    if b"The flag is cybears{--FLAG ENCRYPTED--}" in garbage_contents:
        return None
    else:
        # Garbage file is decrypted

        print("Unzipping...")
        garbage_file = io.BytesIO(garbage_contents)
        garbage_zip = zipfile.ZipFile(garbage_file)

        flag_file = garbage_zip.read("f")

        return flag_file


def test_image(image_path, password=DEFAULT_PASSWORD):
    """
    Test Bear Metal image
    :param image_path: path to Bear Metal image
    """
    print("Testing image: %s" % image_path)

    wait_time = 60

    # Create a copy of the image
    image_copy_path = "test.img"
    shutil.copy(image_path, image_copy_path)

    # Verify the flag file is present (but encrypted)
    encrypted_flag = try_extract_flag_file(image_copy_path)
    if encrypted_flag is not None:
        print(encrypted_flag)
        raise Exception("flag is not encrypted properly!")

    # Start the image
    qemu_process = start_qemu(image_copy_path)

    print("Waiting for prompt")
    qemu_process.expect(b"ENTER KEY:", timeout=BOOT_TIMEOUT)

    time.sleep(1)

    print("Entering key")
    qemu_process.send(password)

    qemu_process.expect(b"VALID", timeout=UNLOCK_TIMEOUT)

    stop_qemu(qemu_process)

    # Verify the flag file is present
    encrypted_flag = find_flag(try_extract_flag_file(image_copy_path))
    expected_flag = find_flag(open("flag.txt", "rb").read())
    if encrypted_flag != expected_flag:
        print(encrypted_flag)
        raise Exception("flag is not encrypted properly!")
    else:
        print("SUCCESS")

    # Verify that booting the image a second time results in an "already unlocked" message
    print("Checking if booting a second time results in 'DISK ALREADY UNLOCKED'")
    qemu_process = start_qemu(image_copy_path)
    qemu_process.expect(b"DISK ALREADY UNLOCKED", timeout=BOOT_TIMEOUT)
    stop_qemu(qemu_process)

    # Check that booting the disk a second time didn't alter the flag
    print("Checking if booting a second time altered the flag")
    encrypted_flag = find_flag(try_extract_flag_file(image_copy_path))
    expected_flag = find_flag(open("flag.txt", "rb").read())
    if encrypted_flag != expected_flag:
        print(encrypted_flag)
        raise Exception("flag has been altered!")
    else:
        print("SUCCESS")

    print("All tests passed!")


def stop_qemu(qemu_process):
    print("Waiting for qemu to shut down")
    timeout = SHUTDOWN_TIMEOUT
    while timeout > 0 and qemu_process.isalive():
        time.sleep(1)
        timeout -= 1
    if qemu_process.isalive():
        qemu_process.kill()
        raise Exception("qemu still running after timeout")
    print("qemu exited successfully")


def start_qemu(image_path):
    # Start qemu
    qemu_args = [
        "qemu-system-i386",
        "-drive", "format=raw,if=floppy,file=%s" % image_path,
        "-nographic",
    ]
    qemu_command = " ".join(qemu_args)
    print("Running: %s" % qemu_command)
    qemu_process = pexpect.spawn(qemu_command, )
    try:
        qemu_process.logfile = sys.stderr.buffer
    except AttributeError:
        qemu_process.logfile = sys.stderr
    return qemu_process


def main():
    if len(sys.argv) < 2:
        print("usage: %s <image>" % sys.argv[0])
    else:
        test_image(sys.argv[1])


if __name__ == "__main__":
    main()
