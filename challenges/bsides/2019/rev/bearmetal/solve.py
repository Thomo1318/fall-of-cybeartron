from z3 import *

len_flag = 8

s = Solver()

#initiate instances
x=[]
for i in range(0,len_flag):
    x.append(Int('x'+str(i)))
    
#add printable ASCII contraints to solver
for i in range(0,len_flag):
    s.add( x[i] >= 32 )
    s.add( x[i] < 127 )

s.add(x[6]+x[4]+x[3]==251)
s.add(x[1]+x[4]+x[1]==350)
s.add(x[7]+x[2]+x[1]==261)
s.add(x[6]+x[6]+x[1]+x[4]==330)
s.add(x[4]+x[2]+x[6]+x[5]==373)
s.add(x[1]+x[2]+x[0]==306)
s.add(x[1]+x[3]+x[2]+x[7]==349)
s.add(x[3]+x[6]+x[4]+x[4]==365)
s.add(x[7]+x[6]+x[3]+x[7]==227)
s.add(x[4]+x[3]+x[0]+x[2]==390)
s.add(x[1]+x[2]+x[4]==330)
s.add(x[7]+x[2]+x[6]+x[7]==237)
s.add(x[1]+x[2]+x[1]+x[0]==424)
s.add(x[2]+x[7]+x[1]==261)
s.add(x[0]+x[4]+x[0]+x[6]==343)
s.add(x[1]+x[3]+x[4]+x[4]==434)

print(s.check())
mod = s.model()

output = ""

for i in range(0,len_flag):
    output += (chr(int(str(mod[x[i]]))))

print output

e=''
for i in range(0, len(output)):
        e += "x["+str(i) + "]==" + str(ord(output[i])) + ","

s.add(Not(And(eval(e[:-1]))))

if str(s.check()) == 'unsat':
    print("Unique Solution!")
else:
    print("Non-unique solutions exist... more work needed")


