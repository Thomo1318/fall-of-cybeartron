#!/bin/sh

HANDOUT=export/handout/bearmetl.img
TESTED_BUILDS=manual-tested-builds.txt

BUILD_HASH=`md5sum $HANDOUT | cut -d ' ' -f 1`

if grep -q $BUILD_HASH $TESTED_BUILDS
then
    echo Build $BUILD_HASH has already been manually tested. Skipping tests.
else
    python3 test.py export/handout/bearmetl.img
fi
