from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)

@bp.route('/')
def index():
    """Show all the posts, most recent first."""
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('blog/index.html', posts=posts)


def get_post(id, check_author=True):
    """Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    """Create a new post for the current user."""
    flash("ERROR: Creating blogs disabled")
    return redirect(url_for('blog.index'))
    
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    """Update a post if the current user is the author."""
    flash("ERROR: Updating blogs disabled")
    return redirect(url_for('blog.index'))

    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ? WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    flash("ERROR: Deleting blogs disabled")
    return redirect(url_for('blog.index'))

    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))

@bp.route('/show_users', methods=('GET',))
@login_required
def show_users():
    """List users

Must be logged in user
    """
    db = get_db()
    u = db.execute('SELECT id, username, energon, alliance FROM user').fetchall()

    #print("Debug: in show user")
    #for user in u: 
    #    print("id: {}, username: {}, energon: {}, alliance: {}".format(user['id'], user['username'], user['energon'], user['alliance']))

    if u is None:
        abort(404, "Couldn't find any users")

    return render_template('blog/show_users.html', users=u)


@bp.route('/give_energon', methods=('GET',))
@login_required
def give_energon():
    """Give Energon

    User must have at least 10 energon to give energon
    User must have at least 10 energon to access flag

    Must be logged in user
    """
    
    recv_id = None
    recv_id = request.args.get('recv_id')

    # check that recv_id is an integer
    try: 
        recv_id = int(recv_id)
    except: 
        flash("ERROR: Error in recv_id, must be an integer")
        return redirect(url_for('blog.show_users'))

    # check recv_id is positive (>0)
    if (recv_id < 1):
        flash("ERROR: error in recv_id")
        return redirect(url_for('blog.show_users'))

    # check recv_id is not own ID
    if (recv_id == int(g.user['id'])):
        flash("ERROR: Can't give energon to self")
        return redirect(url_for('blog.show_users'))

    # check recv_id user exists within SQL DB
    u = get_db().execute('SELECT id, username, energon FROM user WHERE id = ?',(recv_id,)).fetchone()
    if u is None:
        flash("ERROR: User ID does not exist")
        return redirect(url_for('blog.show_users'))

    #check whether recipient has over MAX energon
    if u['energon'] >= 20 : 
        flash("ERROR: User has MAX energon... probably doesn't need any more")
        return redirect(url_for('blog.show_users'))

    # check whether giver has >10 energon
    if (g.user['energon'] < 10):
        flash("ERROR: Users must have at least 10 energon to gift energon to others")
        return redirect(url_for('blog.show_users'))

    # update energon for receiver
    #'update user SET energon = energon +1 where ID = ?'
    db = get_db()
    db.execute('UPDATE user SET energon = energon + 1 where id = ?', (recv_id,))
    db.commit()
    flash("SUCCESS: Energon delivered")

    #print("DEBUG: energon giver ID: {}, has energon: {}".format(g.user['id'], g.user['energon']))
    #print("DEBUG: energon receiver ID: {} ".format(recv_id))
    return redirect(url_for('blog.show_users'))

@bp.route('/flag', methods=('GET',))
@login_required
def flag():
    """Show Flag

Must be logged in user
    """
    if (g.user['energon']< 10):
        flash("ERROR: Users need at least 10 energon to see flag!")
        return redirect(url_for('index'))

    return render_template('blog/flag.html')
