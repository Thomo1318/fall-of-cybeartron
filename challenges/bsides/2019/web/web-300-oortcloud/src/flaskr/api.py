from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

import json

bp = Blueprint('api', __name__)

def error_json(error):
    error_json = {"result":"ERROR", "error":error}
    return json.dumps(error_json)

@bp.route('/show_users')
@login_required
def show_users():
    """Show all the users, as json."""
 
    db = get_db()
    users = db.execute(
        'SELECT id, username, energon, alliance'
        ' FROM user'
    ).fetchall()
    
    if users is None: 
        return error_json("No users found")

    #jsonify results
    users_json = {"result": "SUCCESS", "users":[]}
    for u in users: 
        users_json['users'].append({"id": u['id'], "username": u['username'], "energon":u['energon'], "alliance":u['alliance']})

    return json.dumps(users_json)