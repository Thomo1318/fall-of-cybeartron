# vim: ft=yaml
#
# This is a template which is suitable for deploying a HTTP challenge with
# sticky client session balancing across pods.
#
###############################################################################
# External facing Service
###############################################################################
# This is the service frontend for the challenge which we expose to the
# Internet.
---
apiVersion: v1
kind: Service
metadata:
    name: $name
    labels:
        challenge: $name
    annotations:
        cybears_dns: $target_dns
spec:
    type: LoadBalancer
    ports:
        $service_ports
    selector:
        challenge: $name
###############################################################################
# Challenge Deployment
###############################################################################
---
apiVersion: apps/v1
kind: Deployment
metadata:
    name: $category-$name
    labels:
        challenge: $name
        category: $category
spec:
  # We can't do sticky session on AWS without it being a huge pain in the butt
  # so we limit ourselves to a single replica here
    replicas: 1
    selector:
        matchLabels:
            challenge: $name
    template:
        metadata:
            labels:
                challenge: $name
        spec:
            automountServiceAccountToken: false
            enableServiceLinks: false
            # We define this host alias in the container so the solve script as
            # defined in the MANIFEST can use it like it might expect
            hostAliases:
                - ip: "127.0.0.1"
                  hostnames:
                  - "$target_dns"
                  - "redis"
            containers:
                # The actual challenge containers
                - name: $name
                  image: $server_container
                  env:
                      - name: BACKEND_HOSTNAME
                        value: "127.0.0.1"  # Just use localhost addressing
                  ports:
                    $container_ports
                - name: session-store
                  image: redis
                # Healthcheck container
                - name: $name-test
                  image: $healthcheck_container
                  # Make the healthcheck container sleep forever and probe the actual
                  # challenge container for liveness. If the challenge container
                  # becomes unhealthy then this one will die and take the pod with it.
                  args:
                  - /bin/sh
                  - -c
                  - while true; do sleep 5; done
                  livenessProbe:
                      exec:
                        command:
                        - /bin/sh
                        - -c
                        - "$solve_script"
                      initialDelaySeconds: 5
                      periodSeconds: $healthcheck_interval
                      timeoutSeconds: 5
                  # Allow viewing errors in describe pods
                  terminationMessagePolicy: "FallbackToLogsOnError"
            imagePullSecrets:
              - name: regcred
