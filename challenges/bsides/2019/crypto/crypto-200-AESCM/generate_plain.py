# Let's make a plaintext with a flag
# Make the length so that the last session gets mostly padded to help out a bit
# Possibly give the length of the data, would be a pretty big hint
# You don't actually need the IV to exploit this, but maybe give it as a false flag

prefix = "The flag is out there. In this plain text."

# Need some story related text to bang into the plain text; the flag will then just be hidden inside that.

#Make sure if you change the plain the length stays the same (457)

someplain = "found some old recording device in a bunker on the Plateau. Seems to be talking about some older civilisation, older than anything we know about for sure. Maybe the people that left all these artifacts around? Anyway, sennding a rough transcript: <Noir-style narration> Cybeartron. It had stood the test of time. A dynasty founded in the distant past. But that night, overlooking the candy-striped neon playground of the city, I could sense a change coming."

flag = "cybears{|3 /-\ |) ( 0 |_| |\| + 3 R}"

#Do not change the positioning of the flag here

theplain = prefix + someplain[:300] + flag + someplain[300:]

print(theplain)