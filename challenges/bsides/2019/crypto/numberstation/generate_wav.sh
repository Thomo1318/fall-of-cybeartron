#!/usr/bin/env bash
./generate_text.py | espeak -g 50 -p 10 -s 120 -w out.wav

ffmpeg -y -i syphony_5_beethoven.mp3 -i out.wav -filter_complex amerge -ac 1 -acodec pcm_u8 -ar 48000 final.wav
#ffmpeg -i syphony_5_beethoven.mp3 -i out.wav -ac 1 -acodec pcm_u8 -ar 48000 final.wav
