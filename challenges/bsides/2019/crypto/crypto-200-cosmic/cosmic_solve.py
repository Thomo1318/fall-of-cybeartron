
import Crypto.Util.number as num
import Crypto.Signature.PKCS1_v1_5 as pkcs
from Crypto.Hash import SHA
import random
from binascii import *
import Crypto.PublicKey.RSA as rsa
import json
import os
import cosmic_generate
import sys


if __name__ == '__main__':

    if(len(sys.argv) != 3): 
        print("usage: cosmic_solve messages.json flag.txt.enc")
        exit(-1)

    filename1 = sys.argv[1] 
    filename2 = sys.argv[2]

    with open(filename1, "r") as f: 
        d = f.read()

    j = json.loads(d)

    n = j['modulus']
    e = j['exponent']

    r = rsa.construct((n,e))
    signer = pkcs.new(r)


    for jj in j['content']:
        m = jj['message'].encode("utf-8")
        ss = unhexlify(jj['signature'])
        if (signer.verify(SHA.new(m), ss) == False):
            break

    p = cosmic_generate.find_factor(m, r.e, r.n, int.from_bytes(ss, byteorder='big'))

    if p == 1 or p == n: 
        print("ERROR occured, failed to factor")
        exit(-1)

    print("Successfully factored RSA modulus")

    q = n//p
    d = num.inverse(r.e, (p-1)*(q-1))

    new_r = rsa.construct((n,e,d))

    with open(filename2, "rb") as g: 
        dd = g.read()

    assert(new_r.decrypt(dd) == b'cybears{D@mn_th0s3_c0sm1c_r@yz}')

    print("Successfully decrypted flag!")
    print("Passed all tests")






