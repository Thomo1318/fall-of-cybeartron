#!/bin/sh
if [ $# -ne 3 ]; then
    echo "ping: invalid argument $@" >&2
    exit 1
fi
echo "PING ${3} 56(84) bytes of data."
sleep 1     # This should really be 10 but no reason to slow players down more
echo "\n--- ${3} ping statistics ---"
echo "1 packets transmitted, 0 received, 100% packet loss, time 0ms"
