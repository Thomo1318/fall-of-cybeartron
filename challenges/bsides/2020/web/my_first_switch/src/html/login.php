<?php
$username = null;
$password = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

//Straight up SQLi.

    if(!empty($_POST["username"]) && !empty($_POST["password"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
	//echo $username . ":" . $password;
        
	//Create in memory database for ease of use in case someone hacks it...
	$db = new PDO("sqlite::memory:");
	try {
		$db->exec(
			"CREATE TABLE IF NOT EXISTS users (
			username TEXT UNIQUE NOT NULL,
			password TEXT NOT NULL)"
		);
		$db->exec("INSERT OR IGNORE INTO users VALUES ('user', 'klasd8#SNa#&S.')");
			
	} catch (PDOException $e) {
		echo $e->getMessage();
	}

	try {
	$query = "SELECT * FROM users WHERE username = '" . $username . "' AND password = '" . $password . "'";
	$result = $db->query($query);
	} catch (Exception $e) {
		echo $e->getMessage();
	}	

	if ($result->fetchColumn()) {
		//echo "authed";
		session_start();
		$_SESSION["authenticated"] = 'true';
		header('Location: index.php?loggedin');
        }
        else {
		echo "NoAuth";
		header('Location: login.php?loginfail');
        }
        
    } else {
	    header('Location: login.php?loginempty');
    }
} else {
?>

<html>
<head><title>Login</title>
</head>
<body>

<h1>Cybear Networking&copy;</h1>

<?php
	if (isset($_GET['loginfail'])) {
		echo "<h2>Login Failed</h2>";
	}
?>

<form action="login.php" method="post">
<table>
	<tr>
		<td>Username:</td>
		<td><input type="text" name="username"></td>
	</tr>
	<tr>
		<td>Password:</td>
		<td><input type="password" name="password"></td>
	</tr>
	<tr>
		<td></td><td><input type="submit" value="Login"></td>
	</td>
</table>

</form>

</body>
</html>
<?php
}
?>
