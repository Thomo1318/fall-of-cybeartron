# cybearchan walkthrough

This challenge requires identifying a couple of different vulnerabilities and using them together to bypass protections and achieve RCE.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>
How can I get some code on to the server?
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
There's a local file inclusion that's been introduced which isn't in the default tinyib codebase.
</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>
Grandfather do sheep have sheep? No, sheep don't have sheep, sheep have lambs.
</details>

## Steps

<details>
<summary>Spoiler warning</summary>

Notice that there is a link to share.php on the index. This page gives us a basic file inclusion primitive via load.php?img=<file>.

Use this vulnerability to dump the source code for send.php (which is the calling script to load.php). In this file you will note that it relies on an external class, inc/mailer.php. Dump this class and you will notice that it is susceptible to a phar deserialisation attack due to handling of files and implementation of \_\_destruct(). (See https://github.com/s-n-t/presentations/blob/master/us-18-Thomas-It's-A-PHP-Unserialization-Vulnerability-Jim-But-Not-As-We-Know-It.pdf)

As other php wrappers have been filtered, we have to upload our payload as an image attachment. Forge a phar payload that is a jpg/phar polyglot in order to pass the image upload checks done by the image board. Note that the jpg header will need to be valid for an image under 250x250px otherwise the imageboard thumbnailer kicks and will exit with an error.

The imageboard will tell you the filename once successfully uploaded an image.

Jump back to share.php and trigger your payload with http://cybearchan.ctf.cybears.io/share.php?img=phar:///var/www/html/src/-filename-.jpg/test.txt

Note that PHP8 no longer automatically deserialises phar metadata, as such closes off a lot of these vulnerabilities.
</details>

