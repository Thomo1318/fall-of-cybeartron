<?php
$username = null;
$password = null;

if (isset($_GET['debug'])) { highlight_file('login.php'); die;}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//This time I won't use SQL so I don't have any vulnerabilities!

	if(!empty($_POST["username"]) && !empty($_POST["password"])) {
		$username = $_POST["username"];
		$password = $_POST["password"];
		$config = json_decode(file_get_contents("/master_password.json"));

		if (($username == "admin") && (sha1($password) == $config->{"password_hash"})) {
			session_start();
			$_SESSION["authenticated"] = 'true';
			header('Location: index.php?loggedin');
		} else {
			header('Location: login.php?loginfail');
		}

	} else {
		header('Location: login.php?loginempty');
	}

	die();	//Location header already sent.

} else {	//No POST data; Show login page.

?>

	<html>
	<head><title>Login</title></head>
	<body>
		<h1>Cybear Networking&copy;</h1>

		<?php
			if (isset($_GET['loginfail'])) {
				echo "<h2>Login Failed</h2>";
			}
		?>

		<form action="login.php" method="post">
		<table>
			<tr>
				<td>Username:</td>
				<td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td></td><td><input type="submit" value="Login"></td>
			</tr>
		</table>
		<!-- Debug: login.php?debug=1 -->
		</form>

	</body>
	</html>

<?php
}	//End no POST data
?>
