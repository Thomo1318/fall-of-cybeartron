from pwn import *
import sys
import argparse

def probe(addr, port):
    conn = connect(addr, port)

    probe = b"=== Main Menu ==="
    data = conn.readuntil(probe, timeout=5)

    assert(probe in data)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote')
    parser.add_argument('-p', '--port')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')
    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'

    if args.remote and args.port:
        probe(args.remote, args.port)
        sys.exit(0)
    else:
        log.error('Invalid arguments')
        sys.exit(1)

if __name__ == "__main__":
    main()
