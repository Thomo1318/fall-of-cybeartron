#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <linux/seccomp.h>
#include <sys/prctl.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <sys/random.h>

#define FLAG_PATH "flag.txt"

#define MAX_PROGRAM_ANALYSIS_SIZE 100

/*
 * Get an unsigned long from stdin and store it in *number.
 *
 * Returns NULL if an error occurred (EOF encountered or invalid input)
 *
 * Returns number on success
 */
unsigned long *get_number(unsigned long *number)
{
#define INPUT_SIZE 255
    char line[INPUT_SIZE] = {0};
    char *endptr = NULL;
    unsigned long ret = 0;

    if (fgets(line, INPUT_SIZE, stdin) == NULL) {
        return NULL;
    }

    /* Remove the \n from the input so that strtoul error
     * checking is accurate
     */

    line[strcspn(line, "\n")] = '\0';

    errno = 0;

    ret = strtoul(line, &endptr, 10);

    if (errno == 0 && (*endptr == '\0' && line[0] != '\0')) {
        *number = ret;
        return number;
    }

    return NULL;
}

void *map_mem_for_program(void)
{
    void *buf = NULL;

    buf = mmap(NULL, MAX_PROGRAM_ANALYSIS_SIZE, PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    if (buf == MAP_FAILED) {
        printf("Failed to map space for the analysis program\n");
        return NULL;
    }

    return buf;
}

int random_in_range(size_t low, size_t high)
{
    ssize_t random_bytes = -1;
    size_t seed = 0;

    do {
        random_bytes = getrandom(&seed, sizeof(seed), 0);
    } while(random_bytes != sizeof(seed));

    srand(seed);

    return low + rand() / (RAND_MAX / (high - low + 1) + 1);
}

int get_random_fd_val(void)
{
    struct rlimit rl = {0};

    if (getrlimit(RLIMIT_NOFILE, &rl) == -1) {
        return -1;
    }

    return random_in_range(5, rl.rlim_cur);
}
 
int main(int argc, char *argv[])
{
    void *buf = NULL;
    /* This is volatile to _attempt_ to prevent it being available in a register when the shellcode is called */
    volatile int flag_fd = -1;
    size_t analysis_size = 0;
    pid_t child = -1;

    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    if ((flag_fd = open(FLAG_PATH, O_RDONLY)) == -1) {
        printf("Error: Unable to open flag. It should be placed at %s\n", FLAG_PATH);
        goto leave_program;
    }
   
    /* At this stage the flag_fd is almost always going to be 3, making the corresponding shellcode trivial.
     * We'll crank up the difficulty a little by duping flag_fd onto a random fd to actually force some investigation
     * in the analysis program.
     */
    volatile int temp_flag_fd = -1;
    volatile int rand_fd_val = get_random_fd_val();

    if (rand_fd_val == -1)
    {
        printf("Error: Unable to get random fd value\n");
        goto close_flag_fd;
    }
        
    if ((temp_flag_fd = dup2(flag_fd, rand_fd_val)) == -1)
    {
        printf("Error: Unable to remap flag_fd to a random fd: %s\n", strerror(errno));
        goto close_flag_fd;
    }

    close(flag_fd);
    flag_fd = temp_flag_fd;
    temp_flag_fd = 0;
    rand_fd_val = 0;

    if ((buf = map_mem_for_program()) == NULL) {
        printf("Error: Unable to map memory for analysis program\n");
        goto close_flag_fd;
    }

    printf("Cybears Cloud Analysis Service\n");

    printf("\nEnter size in bytes of the analysis program: ");

    if (get_number(&analysis_size) == NULL) {
        printf("Invalid input\n");
        goto unmap_memory;
    }

    if (analysis_size > MAX_PROGRAM_ANALYSIS_SIZE - 1) {
        printf("The Cloud Analysis Service is unable to accomodate an analysis program of that size\n");
        goto unmap_memory;
    }

    printf("\nEnter program as raw X86_64 bytes: ");

    if (fgets(buf, analysis_size, stdin) == NULL) {
        printf("Invalid Input\n");
        goto unmap_memory;
    }

    ((char *)buf)[strcspn(buf, "\n")] = '\xc3'; /* Force a ret */
    ((char *)buf)[MAX_PROGRAM_ANALYSIS_SIZE - 1] = '\xc3'; /* fgets will NULL terminate. Change to a ret */

    if (mprotect(buf, MAX_PROGRAM_ANALYSIS_SIZE, PROT_EXEC) == -1) {
        goto unmap_memory;
    }

    child = fork();

    if (child == -1) {
        printf("Error: Failed to fork()\n");
        goto unmap_memory;
    }

    else if (child == 0) {
        /* Child Process */

        /* Close stdin and stderr */
        close(0);
        close(2);

        timer_t timer = {0};

        struct sigevent evp = {
            .sigev_notify = SIGEV_SIGNAL,
            .sigev_signo = SIGKILL
        };

        struct itimerspec expiry = {
            .it_value = {
                .tv_sec = 5
            }
        };

        /* Set a timer to limit code execution time to 5s */
        if (timer_create(CLOCK_MONOTONIC, &evp, &timer) != 0) {
            printf("Error: Unable to set execution limits\n");
            exit(1);
        }

        if (timer_settime(timer, 0, &expiry, NULL) != 0) {
            printf("Error: Unable to set execution limits\n");
            exit(1);
        }

        prctl(PR_SET_SECCOMP, SECCOMP_MODE_STRICT);

        /* Call the code. */
        ((void (*)(void))buf)();

        exit(1);
    }

    else {
        /* parent process */

        waitpid(child, NULL, 0);
   }
    

unmap_memory:
    munmap(buf, MAX_PROGRAM_ANALYSIS_SIZE);
close_flag_fd:
    close(flag_fd);
leave_program:
    return 0;


}

