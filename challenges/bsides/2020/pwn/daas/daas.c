#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "./libdaas.h"

/*
   gcc -I/anaconda/pkgs/openssl-1.0.1h-1/include -L/usr/local/Cellar/openssl/1.0.2q/lib ./daas.c -lssl -lcrypto -DDEBUG -o daas && ./daas
   gcc -I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib ./daas.c -lssl -lcrypto -DDEBUG -o daas && ./daas

   encrypt and decrypt from https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption

*/


/***********************************************************
 *
 *  Start of main function
 *
 * *********************************************************/

int main(int argc, char *argv[])
{

    unsigned char * input, *output;
    int ret, i, input_len;
    unsigned char decrypt_buffer[16*10];
    unsigned char key256[32] = {0};
    unsigned char iv[16]={0};

    /*
     * Buffer for ciphertext. Ensure the buffer is long enough for the
     * ciphertext which may be longer than the plaintext, depending on the
     * algorithm and mode.
     */
    unsigned char ciphertext[128];

    /* Buffer for the decrypted text */
    unsigned char decryptedtext[128];
    int decryptedtext_len, ciphertext_len;

    //Perform self-test
    ret = self_test();
    if (0!=ret)
    {
        printf("ERROR: Self-test failed... exiting\n");
        return -1;
    }

    //Generate random key and iv for session
    ret = RAND_bytes(key256,32);
    if (1!=ret)
    {
        printf("Error generating random bytes (key)\n");
        return -1;
    }

    ret = RAND_bytes(iv,16);
    if (1!=ret)
    {
        printf("Error generating random bytes (iv)\n");
        return -1;
    }


    while(1)
    {
        printf("Enter a string\n");
        scanf("%ms", &input);

        //printf("DEBUG: You entered [%s]\n", input);

        input_len = strlen((const char *)input);

        output = malloc(input_len+1);
        if(NULL == output)
        {
            printf("ERROR allocating memory\n");
            return -1;
        }

        //Validate input string
        //Should be only hex chars, odd length and... multiple of block size
        if(	!is_hex_string(input) ||
            !is_odd_length(input) ||
            !is_multiple_of_blocklen(input) )
        {
            free(input);
            free(output);
            printf("ERROR invalid input - requires hex string that will be a multiple of an AES block length\n");
            return -1;

        }

        //Convert hex string to raw bytes
        //printf("DEBUG: This was a valid hex string!\n");
        convert_hex_to_bytes(input, output);
        //printf("DEBUG: output is [%s]\n", output);

        //Decrypt raw buffer
        //printf("DEBUG: output len [%d]\n", input_len/2);
        ret = decrypt_and_print_hex(output, input_len/2, key256, iv);
        if (0!=ret)
        {
            printf("ERROR in decryption...\n");
            return -1;
        }
        //decryptedtext_len = decrypt(output, input_len/2, key256, iv,
        //                 decrypt_buffer);
        //printf("DEBUG: decrypted_len = %d\n", decryptedtext_len);
        //decrypt_buffer[decryptedtext_len] = 0;
        //printf("DEBUG - decrypted input: %s\n", decrypt_buffer);
    } //END main loop

    free(input);
    free(output);
    return 0;
}
