# vi: ft=dockerfile
FROM gcc

RUN apt update && apt install -y avr-libc gcc-avr libsimavr-dev srecord \
 && rm -rf /var/lib/apt/*
