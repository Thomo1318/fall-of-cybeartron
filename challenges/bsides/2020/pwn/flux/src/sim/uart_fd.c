#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#include <simavr/avr_uart.h>
#include <simavr/fifo_declare.h>

#include "uart_fd.h"

#define fatal_error(msg) { fprintf(stderr, msg); exit(EXIT_FAILURE); }

// Technically UART has a 9 bit maximum size. Currently only 8 bit is supported.
DEFINE_FIFO(uint8_t, fd_fifo);

enum {
    IRQ_UART_IN = 0,
    IRQ_UART_OUT,
    IRQ_UART_COUNT
};

// AVR -> Output
// Called when data gets sent by the AVR
static void uart_avr_to_fd_hook(struct avr_irq_t *irq, uint32_t value, void *arg)
{
    uart_fd_t *info = (uart_fd_t *)arg;

    // Put the data in the FIFO
    // We will lose excess data if the buffer is full.
    if(!fd_fifo_isfull(&info->avr_out_fifo))
    {
        fd_fifo_write(&info->avr_out_fifo, value);
    }
}

// Sends data to the AVR UART.
// Triggered by a poll of the UDRE.
// Needs to be called manually in the main loop to correctly handle interrupt driven IO.
// TODO - Find a callback or something to hook in the init.
void send_data_to_avr(uart_fd_t *info)
{
    // Write each byte until we run out or get told to stop
    while (info->xon && !fd_fifo_isempty(&info->avr_in_fifo))
    {
        uint8_t uart_data = fd_fifo_read(&info->avr_in_fifo);
        avr_raise_irq(info->irq + IRQ_UART_OUT, (uint8_t)uart_data);
    }
}

// Called when the AVR is ready to receive
static void uart_avr_xon_hook(struct avr_irq_t *irq, uint32_t value, void *arg)
{
    uart_fd_t *info = (uart_fd_t *)arg;
    info->xon = 1;

    send_data_to_avr(info);
}

// Called when the AVR is no longer raedy to receive
static void uart_avr_xoff_hook(struct avr_irq_t *irq, uint32_t value, void *arg)
{
    uart_fd_t *info = (uart_fd_t *)arg;
    info->xon = 0;
}

// This thread handles TCP comms
static void *uart_fd_thread(void *arg)
{
    uart_fd_t *info = (uart_fd_t *)arg;
    int max_fd = (info->in_fd > info->out_fd) ? info->in_fd : info->out_fd;

    while (1) {
        fd_set read_set, write_set;
        FD_ZERO(&read_set);
        FD_ZERO(&write_set);

        // Set when there is data to transfer
        unsigned actions = 0;

        // If we can read data
        uint16_t fifo_avail = fd_fifo_get_write_size(&info->avr_in_fifo);
        if (fifo_avail)
        {
            FD_SET(info->in_fd, &read_set);
            actions = 1;
        }
        
        // If there is data to send
        if (!fd_fifo_isempty(&info->avr_out_fifo))
        {
            FD_SET(info->out_fd, &write_set);
            actions = 1;
        }

        if (!actions)
        {
            // Nothing to do.
            usleep(1);
            continue;
        }

        //printf("s\n");
        struct timeval timeout = { 0, 100 };
        int ret = select(max_fd + 1, &read_set, &write_set, NULL, &timeout);

        if (ret == 0) continue;

        if (ret < 0)
        {
            perror("select error");
            exit(EXIT_FAILURE);
        }

        // If there is data to read
        if (FD_ISSET(info->in_fd, &read_set))
        {
            uint8_t buffer[fd_fifo_fifo_size];

            size_t count = read(info->in_fd, buffer, fifo_avail);
            if (count < 0)
            {
                perror("select error");
                exit(EXIT_FAILURE);
            }

            // Copy the received data into the fifo
            int index = 0;
            while((index < count) && !fd_fifo_isfull(&info->avr_in_fifo))
            {
                fd_fifo_write(&info->avr_in_fifo, buffer[index]);
                index++;
            }
        }
        
        // If we can send data
        if (FD_ISSET(info->out_fd, &write_set))
        {
            uint8_t buffer[fd_fifo_fifo_size];
            
            // Copy any data from the fifo into the buffer
            uint16_t count = fd_fifo_get_read_size(&info->avr_out_fifo);
            for (uint16_t index = 0; index < count; index ++)
            {
                buffer[index] = fd_fifo_read_at(&info->avr_out_fifo, index);
            }

            // Write the buffer out to the fd
            ssize_t wr_count = write(info->out_fd, buffer, count);
            if (wr_count < 0)
            {
                perror("write error");
                exit(EXIT_FAILURE);
            }

            // Remove the number of bytes that actually got written from the buffer.
            for (ssize_t index = 0; index < wr_count; index ++)
            {
                fd_fifo_read(&info->avr_out_fifo);
            }
        }
    }
    return NULL;
}

uart_fd_t * uart_fd_init(struct avr_t * avr, int in_fd, int out_fd, uint8_t uart_index)
{
    uart_fd_t * info = calloc(1, sizeof(uart_fd_t));

    info->avr = avr;
    info->in_fd = in_fd;
    info->out_fd = out_fd;

    // Disable stdio output
    char uart_num = '0' + uart_index;
    uint32_t flags = 0;
    avr_ioctl(info->avr, AVR_IOCTL_UART_GET_FLAGS(uart_num), &flags);
    flags &= ~AVR_UART_FLAG_STDIO;
    avr_ioctl(info->avr, AVR_IOCTL_UART_SET_FLAGS(uart_num), &flags);


    const char * names[2] = {
        "UART_FD_IN",
        "UART_FD_OUT"
    };

    // Create interrupts.
    info->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_UART_COUNT, names);

    avr_irq_t *src = avr_io_getirq(avr, AVR_IOCTL_UART_GETIRQ(uart_num), UART_IRQ_OUTPUT);
    avr_irq_t *dst = avr_io_getirq(avr, AVR_IOCTL_UART_GETIRQ(uart_num), UART_IRQ_INPUT);
    avr_irq_t *xon = avr_io_getirq(avr, AVR_IOCTL_UART_GETIRQ(uart_num), UART_IRQ_OUT_XON);
    avr_irq_t *xoff = avr_io_getirq(avr, AVR_IOCTL_UART_GETIRQ(uart_num), UART_IRQ_OUT_XOFF);

    if (!src) fatal_error("IRQ Error - src");
    if (!dst) fatal_error("IRQ Error - dst");
    if (!xon) fatal_error("IRQ Error - xon");
    if (!xoff) fatal_error("IRQ Error - xoff");

    avr_connect_irq(src, info->irq + IRQ_UART_IN);
    avr_connect_irq(info->irq + IRQ_UART_OUT, dst);

    avr_irq_register_notify(xon, uart_avr_xon_hook, info);
    avr_irq_register_notify(xoff, uart_avr_xoff_hook, info);
    avr_irq_register_notify(info->irq + IRQ_UART_IN, uart_avr_to_fd_hook, info);

    // Start the IO thread
    pthread_create(&info->thread, NULL, uart_fd_thread, info);
    
    return info;
}
