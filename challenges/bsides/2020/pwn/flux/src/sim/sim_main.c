#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <simavr/sim_avr.h>
#include <simavr/sim_hex.h>
#include <simavr/sim_gdb.h>
#include <simavr/avr_flash.h>
#include <simavr/avr_eeprom.h>
#include <simavr/sim_regbit.h>

#include "uart_fd.h"

// Options
const char * FLASH_HEX = "avr_fw.hex";
const char * EEPROM_HEX = "eeprom.hex";

const char * MMCU = "attiny2313";
uint32_t M_FREQ = 8000000;

// ATtiny2313 Constants
// See "iotn2313.h"
#define SPM_PAGESIZE 32
#define SPMCSR  0x37 + 32
#define CTPB    4
#define RFLB    3
#define PGWRT   2
#define PGERS   1
#define SELFPRGEN 0

#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)


int main(int argc, char *argv[])
{
    int err_val;
    int clientfd;
    avr_t * avr_core;
    uart_fd_t * uart_conn;

    // Create the MCU Core
    avr_core = avr_make_mcu_by_name(MMCU);
    avr_init(avr_core);

#ifdef VERBOSE
    // Increase logging
    avr_core->log = LOG_TRACE;
    avr_core->trace = 1;
#endif // VERBOSE

    avr_core->frequency = M_FREQ;

    // Fix for ATTiny2313
    avr_flash_t flash_info = {
        .flags = 0,
        .r_spm = SPMCSR,
        .spm_pagesize = SPM_PAGESIZE,
        .selfprgen = AVR_IO_REGBIT(SPMCSR, SELFPRGEN),
        .pgers = AVR_IO_REGBIT(SPMCSR, PGERS),
        .pgwrt = AVR_IO_REGBIT(SPMCSR, PGWRT),
        .blbset = AVR_IO_REGBIT(SPMCSR, RFLB),
    };

    avr_flash_init(avr_core, &flash_info);

   // Load in Flash data.
   uint32_t flash_size = 0;
   uint32_t flash_base = 0;
   uint8_t *flash_data = read_ihex_file(FLASH_HEX, &flash_size, &flash_base);

   if (flash_data == NULL)
   {
        fprintf(stderr, "Error loading firmware.");
        exit(-1);
   }

    avr_loadcode(avr_core, flash_data, flash_size, flash_base);

   // Load in EEPROM data.
    avr_eeprom_desc_t eeprom_desc = {0};
    uint32_t eeprom_start = 0;
    eeprom_desc.ee = read_ihex_file(EEPROM_HEX, &eeprom_desc.size, &eeprom_start);

    if (eeprom_desc.ee != NULL && eeprom_desc.size)
    {
        eeprom_desc.offset = eeprom_start;
        avr_ioctl(avr_core, AVR_IOCTL_EEPROM_SET, &eeprom_desc);
    }

    // Connect STDIO to UART
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    uart_conn = uart_fd_init(avr_core, fileno(stdin), fileno(stdout), 0);

    avr_reset(avr_core);

#ifdef DEBUG
    // For debugging with gdb-avr (apt install gdb-avr)
    avr_core->gdb_port = 1234;
    avr_core->state = cpu_Stopped;
    avr_gdb_init(avr_core);
#endif // DEBUG

    int state;

    // Run for a limited time.
    while(avr_core->cycle < (300 * M_FREQ))
    {
        state = avr_run(avr_core);
        if (state == cpu_Done || state == cpu_Crashed)
            break;

        // We need to manually check if there is data from the UART.
        // This is because the XON IRQ does not get triggered for interrupt driven UART RX.
        send_data_to_avr(uart_conn);
    }

#ifdef VERBOSE
    if (state == cpu_Crashed) printf("\nCRASHED!\n");    
#endif

    exit(EXIT_SUCCESS);
}
