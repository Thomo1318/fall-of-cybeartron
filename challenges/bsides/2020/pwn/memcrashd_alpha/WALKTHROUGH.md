# memcrashd_alpha walkthrough

The challenge is designed to demonstrate an interesting property of computer architecture.


## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

two's compliment

</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

Negating a number then using it is weird. But should be safe shouldn't it?

</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>

There is one case where the negating of a negative number actually results in the same negative number.

</details>

## Steps

Follow the hints above.

<details>
<summary>Spoiler warning</summary>

So if you now realise that INT_MIN when negated will still equal INT_MIN. Therefor if we set our buffer size to INT_MIN it wont be change during this check. Then the check that this length is not larger than our buffer size will pass as our size is negative and this is a signed comparison.

Unfortunately our read function (fread) uses size_t so this negative length will be treated as a very large positive number. The end result is we can overflow the stack buffer.

No stack canaries, and no ASLR means we can easily ROP into shellcode.

</details>
