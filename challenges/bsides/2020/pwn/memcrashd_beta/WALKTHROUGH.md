# memcrashd_beta walkthrough

The is a reincarnation of the old heap exploitation technique of a write-what-where.

It is a progression of memcrashd_alpha and the changelog printed hints at where the flaw might be, and also what the exploitation primitive should be.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

From the changelog `Added support for unicode names.`

How many bytes are required for unicode names in Linux?

</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

Look at the structure of the Items. Double-linked list and Items are allocated in blocks - i.e. no heap-metadata between items.

</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>

Look at the initial Item hash table. This will be at a known location and has initial (blank) Items. Perhaps a good place for abuse.

</details>

## Steps

Follow the hints above.

<details>
<summary>Spoiler warning</summary>

You should now have a write-what-where. Note that Item_hash starts with an initial item for each key index. 
Normally you couldn't use these items as they have no key set, perhaps you can use the first one will null?.
Then you can set the data pointer and size with your write-what-where.

Alternatively you could set the key name of another one of these Items with the write-what-where.

A good place to point the data pointer is another blank Item. You could use the write-what-where, or overflow again to set a key on the next Item and control its data pointer.

From there you can use get/set on the latest Item and you have an arbitrary read/write.

</details>
