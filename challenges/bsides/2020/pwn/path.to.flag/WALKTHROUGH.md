# path.to.flag walkthrough

This challenge is about exploiting a simple race condition. The `strtok(3)`
function is not reentrant and there aren't many good examples of how to
exploit it online, people just say "don't use it". This challenge provides
a somewhat realistic case of bad `strtok` use for path parsing.

## Hints

A list of hints might be nice. This helps game masters on the day decide what
might be an approriate hint if many people are having issues with a challenge.

1. First hint
<details>
<summary>Spoiler warning</summary>
Check out the following hint!

Have you read the man page?
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

What's with that weird `sleep` call?
</details>

## Steps

The key to this challenge is understanding the way `strtok(3)` behaves
when `NULL` is passed as the first argument.

Have a read of the man page for `strtok`.

The actual exploitation is fairly simple once you wrap your head around
how the static variable works in `strtok`. Each time we call `strtok(path, delim);`
we reset the static variable that is used in `strtok(NULL, delim);`.

There's a `sleep` call just before the call to the `NULL` `strtok` to make it easier
to exploit.

We need to send strings such that the first `strtok` will strip the first thing,
and the call to the `NULL` `strtok` will get the second thing, but time it so the
previous thread retrieves our next token.

The correct sequence is as follows:
- `the.flag`
- `flag.is`
- `is.right`
- `here.buddy`

And each of these must be sent during the `sleep` call for the _previous_ thread.

To time this correctly, we wait until we see `Navigating to '{PATH}'` to send the
next path in the list.

This gives us a decent chance to trigger the race, but with all races it's
not guaranteed, so we need to try our solution a few times. Seems like 1/10
are pretty decent odds.

By watching the output of `Final path` you can see if you're winning the race or not.
