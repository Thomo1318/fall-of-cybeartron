.DEFAULT_GOAL := handout/memcrashd

CC := g++

memcrashd: memcrashd.cpp Item.h
	$(CC) -o $@ -Wall -D_FORTIFY_SOURCE=2 -O1 -pie -fPIE -fstack-protector  -Wl,-z,relro -Wl,-z,now -fno-rtti -std=c++2a $<

handout/memcrashd: memcrashd
	mkdir -p handout
	cp $< $@
	strip -s $@

# --- Docker releated items ---
CHALLENGE_NAME = memcrashd
CHALLENGE_CATEGORY = pwn
CI_REGISTRY_IMAGE = registry.gitlab.com/cybears-private/fall-of-cybeartron
CHALLENGE_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}
BUILD_IMAGE_TAG = gcc
HEALTHCHECK_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-healthcheck
REPO_DIR := $(shell git rev-parse --show-toplevel)
CHALLENGE_HOST = ${CHALLENGE_NAME}.ctf.cybears.io
CHALLENGE_PORT = 2323
CHALLENGE_NETWORK = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}

docker-build:
	# run the builder container and build the challenge
	docker run -v $(REPO_DIR):$(REPO_DIR) --rm ${BUILD_IMAGE_TAG} make -C $(shell pwd)
	# build the container that hosts the challenge
	docker build --build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} -t $(CI_REGISTRY_IMAGE)/$(CHALLENGE_IMAGE_TAG) -f Dockerfile .
	# build the container that solves the challenge
	docker build --build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} -t $(CI_REGISTRY_IMAGE)/$(HEALTHCHECK_IMAGE_TAG) -f Dockerfile.healthcheck .

docker-test: docker-clean docker-build
	# run the challenge in the background
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} -d --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	# run the healthcheck to test against the challenge
	docker run --rm --link ${CHALLENGE_HOST} -it --name ${HEALTHCHECK_IMAGE_TAG} ${CI_REGISTRY_IMAGE}/$(HEALTHCHECK_IMAGE_TAG) ./solve.py --host ${CHALLENGE_HOST} --port ${CHALLENGE_PORT}
	# Now we're done delete the bits we created
	docker rm -f ${CHALLENGE_HOST} || true

docker-run: docker-clean docker-build
	# Run the challenge in the foreground
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	docker rm -f ${CHALLENGE_HOST} || true

docker-clean: clean
	docker rm -f ${CHALLENGE_HOST} || true
	docker rm -f ${HEALTHCHECK_IMAGE_TAG} || true
	docker network rm ${CHALLENGE_NETWORK} || true


clean:
	rm -f memcrashd
	rm -rf handout/
