#include "Item.h"
#include <map>

using std::cout;
using std::cin;
using std::string;

static std::map<string, std::unique_ptr<DataItem>> global_map;

static void clear_line() {
  string temp;
  std::getline(cin, temp);
  cin.clear();
}

char get_char() {
  char input;
  while (!(cin.get(input))) {
    clear_line();
  }
  clear_line();
  return input;
}

static string get_key() {
  string key;
  cout << "key name: ";
  cin >> key;
  clear_line();
  return key;
}

static void get_item(void) {
  auto key = get_key();
  auto search = global_map.find(key);
  if (search != global_map.end()) {
    cout << search->first << " : ";
    search->second->print();
    cout << endl;
  }
}

static void set_item(void) {
  auto key = get_key();
  auto search = global_map.find(key);
  if (search != global_map.end()) {
    search->second->set();
  } else {
    global_map[key] = std::make_unique<DataItem>();
    global_map[key]->set();
    if (!global_map[key]->valid()) {
      global_map.erase(key);
    }
  }
}

static void delete_item(void) {
  auto key = get_key();
  if (global_map.erase(key)) {
    cout << "deleted" << endl;
  } else {
    cout << "key not found" << endl;
  }
}

static void clone_item(void) {
  cout << "clone ";
  auto cloneKey = get_key();
  cout << "new ";
  auto newKey = get_key();
  auto search = global_map.find(cloneKey);
  if (search != global_map.end()) {
    global_map[newKey] = std::make_unique<DataItem>(*search->second);
  } else {
    cout << "key not found" << endl;
  }
}

void help() {
  cout << "g\tGet a Value" << endl;
  cout << "s\tSet a Value" << endl;
  cout << "d\tDelete a Value" << endl;
  cout << "c\tClone a Value" << endl;
  cout << "q\tQuit" << endl;
}

static void menu() {
  char ch = '\0';
  do {
    cout << "Menu > ";
    ch = get_char();
    switch (ch) {
    case 'g':
      get_item();
      break;
    case 's':
      set_item();
      break;
    case 'd':
      delete_item();
      break;
    case 'c':
      clone_item();
      break;
    case 'h':
      help();
      break;
    default:
      cout << ch;
      break;
    }
  } while (ch != 'q');
  cout << endl;
}

int main() {
  std::ios::sync_with_stdio(true);
  setvbuf(stdin, 0LL, 2LL, 0LL);
  setvbuf(stdout, 0LL, 2LL, 0LL);

  cout << "Welcome to Memcrashd, a production ready remote key value store."
       << endl;
  help();
  menu();

  return 0;
}
