# Certs

* _author_: Cybears:cipher
* _title_: Certs
* _points_: 50
* _tags_:  crypto, beginner

The description/flavour text that will be shown to players is in the
[MANIFEST.yml](MANIFEST.yml) file. This README is a developer/post-event
focused description of the challenge.

## Quick start

To build and test locally (will need openssl and make):
```bash
make
```

##  Attachments
* `handout/CybearsTest.crt`: Public certificate

