"""
Demo the concept of CFB ciphertext watermarking.
"""
import binascii
from Cryptodome import Random
from Cryptodome.Cipher import AES

KEY = Random.get_random_bytes(AES.block_size)

def demo():
    # First ptext
    ptext = b"A" * (AES.block_size - 1) + b"A" + b"A" * AES.block_size * 2
    print(binascii.b2a_hex(ptext), len(ptext))
    # First IV
    iv = Random.get_random_bytes(AES.block_size)
    # Make two ciphertexts to confirm IV reuse
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print(binascii.b2a_hex(iv + ztext), len(ztext))
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print(binascii.b2a_hex(iv + ztext), len(ztext))
    # Second IV
    iv = Random.get_random_bytes(AES.block_size)
    # Make another ciphertext to see that the IV changes periodically
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print(binascii.b2a_hex(iv + ztext), len(iv + ztext))
    # Second ptext
    ptext = b"A" * (AES.block_size - 1) + b"@" + b"A" * AES.block_size * 2
    print("PTEXT", binascii.b2a_hex(ptext), len(ptext))
    # One more ciphertext to confirm bit control
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    # First block shows that ztext = something XOR ptext
    # This implies that we're using CFB, OFB or something exotic
    # Second block shows that ztext is scrmabled by first block ptext change
    # implying that the cipher input is related to the preceding block's ztext
    # This eliminates OFB as a potential block mode
    print("ZTEXT", binascii.b2a_hex(ztext), len(ztext))
    # Again for sanity
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print("ZTEXT", binascii.b2a_hex(ztext), len(ztext))
    # Now we can make the first block of controlled ztext
    desired_ztext = b"CYBEARS" * 3
    print("DESIR", binascii.b2a_hex(desired_ztext), len(desired_ztext))
    blocks = (len(desired_ztext) - 1) // AES.block_size + 1
    for _ in range(blocks):
        ptext = bytearray(
            p ^ z ^ d for p, z, d in zip(ptext, ztext, desired_ztext)
        )
        print("PTEXT", binascii.b2a_hex(ptext), len(ptext))
        # Make the controlled ztext
        cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
        ztext = cipher_obj.encrypt(ptext)
        print(ztext, len(ztext))

if __name__ == "__main__":
    demo()
