import Crypto.Cipher.AES as aes
import os
from binascii import *

#First time this was random, now we need to fix the key
#AES_KEY = os.urandom(16)
AES_KEY = unhexlify(b'1af7ecfac3596c6425ce4f403c44e526')

a = aes.new(AES_KEY, aes.MODE_ECB)
flag = b'cybears{y0u_sh0uld_r34lly_u53_4_l0ng_p455phr453}'

cipher = a.encrypt(flag)

PARTIAL_KEY = hexlify(AES_KEY[:-2])

print("Partial AES key: {}".format(PARTIAL_KEY.decode('utf-8')))
print("Cipher: {}".format(hexlify(cipher).decode('utf-8')))



