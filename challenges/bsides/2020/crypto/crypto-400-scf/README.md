# Super Cool Facts!

* _author_: Cybears:cipher
* _title_: Super Cool Facts!
* _points_: 400
* _tags_:  crypto

## Flags
* `cybears{TheVirginiaStateFlagIsTheOnlyUSStateFlagToFeatureNudity}`

## Challenge Text
```markdown
These facts are so super cool, we need to give them to you securely.

        `nc $target_dns $target_port`
```

## Attachments
* `handout/SuperCoolFactsServer.py`: Server code
* `handout/EC.py`: Elliptic Curve code

## Hints
* How is elliptic curve addition happening? Is there anything missing?

## Notes - Build
* run and solve locally:
* `socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"python3 ./SuperCoolFactsServer.py"`
* `sage solve.sage -r localhost:3141`

* run and test with docker
* `docker network create --driver bridge cybearsnet`
* `docker build -t scf -f Dockerfile . `
* `docker build -t scf_test -f Dockerfile.test .`
* `docker run -it -p 3141:3141 --network=cybearsnet --name scf_server scf `
* `docker run -it --network=cybearsnet --name scf_tester scf_test `
