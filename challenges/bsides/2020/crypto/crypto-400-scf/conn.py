import socket

# host = "10.211.55.23"
# port = 8888

def make_conn(host,port):
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.connect((host,port))
    return sock

def recvuntil(sock,delim = b'\n') :
    data = b""
    while not data.endswith(delim):
        data += sock.recv(1)
    return data

def sendline(sock,data):
    sock.send(data + b'\n')
    return 1

# from https://github.com/scwuaptx/CTF/blob/master/2015-writeup/32c3/tree.py
# sock = make_conn(host,port)
# recvuntil(sock,"action:")
# sendline(sock,"3")
