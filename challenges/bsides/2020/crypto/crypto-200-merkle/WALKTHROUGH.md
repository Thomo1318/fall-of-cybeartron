## Empty Vault walkthrough

# Merkle Trees
* This challenge looks at merkle trees, a common cryptographic construct utilising hashes https://en.wikipedia.org/wiki/Merkle_tree. 
* Merkle trees are used in some PQ-signatures, as well as the new Messaging Layer Security (MLS) RFC
* In this challenge, the merkle tree is used to construct a password hash, where each node is a character of the password. 
* In theory, this should mix all of the content of a password, while also ensuring ordering is important. 

# The challenge
* The issue in this challenge is that there is no input validation on the nodes and there is a form of type confusion in the nodes - at the bottom layer, nodes are characters (bytes), whereas in each upper layer, each node is a hash. 
* This confusion is used to create a valid password
* There is a test password (SuperSecretPassword), but this is specifically disallowed in its raw form as an input
* The other passwords are hex-random and high entropy and unlikely to be attackable
* The aim is to create an input that creates the same hash as SuperSecretPassword, but passes the input validation

# Constructing the hash
* Consider the hash tree of "abc" 
```
         H2 = H(H1_L + H1_R)
                  /\
                 /  \
                /    \
	     H1_L     H1_R
            / \        \
           /   \        \
          /     \        \
         'a'    'b'      'c' 
```
* Then `H1_L = H(H('a') + H('b'))` and `H1_R = H('c')`
* If we can create a different input that makes the H1_L node the same, we can bypass the original check. 
* One possiblity is to create a single node are H1_L that is actually `H('a')+H('b')`
* This is one possible solution to bypass the validation, and is the one used in the solve script, but any node could be created and replayed. You could just create a single character password that was `H1_L+H1_R`!
