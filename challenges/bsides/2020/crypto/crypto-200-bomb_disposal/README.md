# Bomb Disposal

* _author_: Cybears:cipher
* _title_: Bomb Disposal
* _points_: 200
* _tags_:  crypto

The description/flavour text that will be shown to players is in the
[MANIFEST.yml](MANIFEST.yml) file. This README is a developer/post-event
focused description of the challenge.

## Quick start

To build and test locally:
```bash
make 
```

##  Attachments
* `handout/bomb.py`: Generate output
* `handout/countdown.py`: encrypted output

