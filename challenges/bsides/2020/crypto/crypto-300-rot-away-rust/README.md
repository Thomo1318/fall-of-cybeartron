# Rot Away Rust

* _author_: Cybears:cipher
* _title_: Rot Away Rust
* _points_: 300
* _tags_:  crypto, protocol

## Challenge Text
* See MANIFEST.yml

## Notes - Build
* This challenge has three roles, a server, and clients in either an initiator role, or a responder role.
* The challenge sets up the server as well as six of client_responders. Some of these responders have cybears affilitation, others decepticom, and some have none. They will only respond to client initiators of their own allegiance.
* One of these client responders holds the flag.
* The player takes the role of a client_initiator and must interact with the clients and servers to receive an encrypted message.

* Set up servers locally:
 * `./run_servers_local.sh` or
 * `docker build -t rar -f Dockerfile.server . &&  docker run -it --rm -p 9000-9007:9000-9007 rar`

* Solve locally
 * `python3 solve.py` #solve script
 * `python3 client_initiator.py` #try to connect with echo client_responder and get session key from server
