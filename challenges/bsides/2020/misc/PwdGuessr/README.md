# PwdGuessr

"To guess at things randomly is not to guess at all. Only through being methodical can enlightenment be achieved"

We have learned that the Demon-tron server has very particular requirements for
their users' passwords.
We have also managed to learn how those passwords are checked:

```python
def check_pwd(sample, pwd):
    for s, p in zip_longest(sample, pwd):
        if s is None or p is None:
            return False
        if p != s:
            time.sleep(0.5)  # Add a delay to stop password-guessing attacks
            return False
    return True
```
## Quick start

To build and test locally:
```bash
make test
```

To replicate the CI locally with docker:

```bash
# For this to work, you’ll need to login with docker to pull the images
docker login registry.gitlab.com
# Once you’re logged in you can run the docker containers
make docker-test
```

This will:
- Build the containers
    - Dockerfile.builder - This knows how to compile the challenge
    - Dockerfile.challenge - This knows how to run the challenge
    - Dockerfile.healthcheck - This knows how to solve the challenge
- Build the challenge inside the builder container, mounting the repo into the container
  the same way the GitLab builder does.
- Create a network
- Spin up the challenge in a container
- Run the solver in a container against the challenge container, exporting
  environment to tell the solver where the challenge is hosted.

## Layout

There are a bunch of files here, it can be a bit scary at first!
So here is a walkthrough of the layout:

| Path | Description |
| ---- | ----------- |
| [Dockerfile.challenge](Dockerfile.challenge)     | This knows how to run the challenge     |
| [Dockerfile.healthcheck](Dockerfile.healthcheck)   | This knows how to solve the challenge   |
| [Makefile](Makefile)                 | The standard Makefile for challenges    |
| handout                  | This is where things you want to give to players go |
| dist                     | This is where server side things go     |
| build                    | This is where intermediate build bits go |
| README.md                | An internal README, not given to players until after the competition |
| [MANIFEST.yml](MANIFEST.yml)             | This is where all the info about the challenge goes, stuff like flags and the description given to players |
| [gitlab-ci.yml](gitlab-ci.yml)            | This is the CI configuration for this challenge, you shouldn't need to touch this. |
| [solve.py](solve.py) | An example challenge solver. You can rewrite this to solve your challenge |

## Default names

The CI system will try a few things to build and test your challenge:

To build a challenge it will try the following commands from within
the docker container it builds from [Dockerfile.builder](Dockerfile.builder).

- `./bin/chal.py --local build`
- `make build`
- `./build.py`
- `./build.sh`

The builder container is built in [gitlab-ci.yml](gitlab-ci.yml) in the
`builder:misc/PwdGuessr` job, and the compilation/building of PwdGuessr
happens in the `compile:misc/PwdGuessr` job. You may change this job
if you wish, but you should probably try to stick to changing these scripts
if you can. It'll just make it easier for you and others.

To test your challenge the CI will try to run the following in the container
it builds from [Dockerfile.healthcheck](Dockerfile.healthcheck):

- `./bin/chal.py --local test`
- `make test`
- `./solve.py`
- `./solve.sh`
- `./doit.py`
- `./doit.sh`

It will expose two environment variables, `${CHALLENGE_HOST}` and
`${CHALLENGE_PORT}` which will be filled in with the hostname and port
of an instance of [Dockerfile.challenge](Dockerfile.challenge) where your
challenge will be running. These varables should be used by your solver
to test the remote version of your challenge. See the default
[solve.py](solve.py) for an example of how to do this.
