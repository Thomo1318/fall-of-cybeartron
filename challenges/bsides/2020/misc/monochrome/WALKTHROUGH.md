# Monochrome Wakthrough

* This puzzle is a `nonogram`, each row and column shows how many runs or blocks of black pixels are in a row/column.
* This can be solved by hand, but can also be solved with SAT/SMT solvers
* See references for more details

## References
* `https://en.wikipedia.org/wiki/Nonogram`
* `https://yurichev.com/writings/SAT_SMT_by_example.pdf`
* `https://github.com/DennisYurichev/SAT_SMT_by_example/tree/master/puzzles/nonogram`
