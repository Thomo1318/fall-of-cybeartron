
import sys

with open(sys.argv[1], 'r') as f:
    d = f.readlines()

maxlen = max([len(d[i]) for i in range(0,len(d))])

dd=[]
for i in range(0,len(d)):
    dd.append((d[i] + "\n"*(maxlen-len(d[i]))).replace("#", "*"))

#extract clue info from row
def extractClue(x):
    A = []
    count = 0
    for i in range(0,len(x)):
        if x[i]=="*":
            count+=1
        else:
            if(count!=0):
                A.append(count)
            count = 0
    if A==[]:
        A = [0]
    return A


#recover column 0 as array
def getColumn(d,col):
    c = [d[i][col] for i in range(0,len(d))]
    return "".join(c)+"\n"


R = []
for i in range(0,len(dd)):
    R.append(extractClue(dd[i]))

C = []
for i in range(0,len(dd[0])-1):
    C.append((extractClue(getColumn(dd,i))))

print("rows = " + str(R))
print("cols = " + str(C))
