# zipper

* _author_: Bear Arms
* _title_: zipper
* _points_: 200
* _tags_:  misc

## Flags
* `cybears{L3t5_m0j1b@k3_50m3_c00k135}`

## Challenge Text
```markdown
"The traveller who drags his feet only raises dust". After a hard day's walking, you stop at a likely-looking grotto to rest for the night. There you find the remains of a camp - you are not the first to have stopped here. Half-hidden in the bushes, you find a magic container, and what looks like the diary of someone trying to pry it open...
```

## Attachments
* `level0.zip`

## Hints
* 

## Notes - Build
* `make` : to build file output
* `make test` : to test generated zip file
* `make tidy` : to remove artifacts (except handout)
