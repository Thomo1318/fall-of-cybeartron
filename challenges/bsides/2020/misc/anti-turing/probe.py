#!/usr/bin/python3

from pwn import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=3141, type=int, help='Remote port')
args = parser.parse_args()

hostname = args.hostname
port = args.port

if __name__ == "__main__":
    print("Probing...")

    p = remote(hostname, port)


    probe = b"Puzzle:"
    prelim = p.readuntil(probe)
    assert probe in prelim

    print("Success")

