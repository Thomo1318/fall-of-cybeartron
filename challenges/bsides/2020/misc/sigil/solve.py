"""
Sigil solver

There are some opportunities for improvements to this script:

I assume a symbol greater than ^ isn't necessary (in reality, ^ should count as
itself or greater). If the real grid contains a ^, you'll have to re-code it
slightly.

Most of the time is taken up by the is_reachable function, but I'm pretty sure
it's necessary. Work on improving that (probably through better data/class
structures).

The data/class structures used were the first ones I thought of that would
work, you'd almost certainly get an improvement by tuning them.

If the S and E are adjacent, there's the possibility of an infinite loop in the
search. Same if the S or E are in a corner. I haven't bothered to code around
these situations, but it wouldn't be hard.
"""
import argparse
import base64
import functools
import signal
import string
import sys
import time

import pwn

# Pretty much all attempts that we solve are slightly trivial walks into a
# corner of the frame which we can solve very quickly. Since we need many
# attempts to get one of these, we'll retry a bunch and limit ourselves to a
# very short time before escaping the solve logic
MAX_ATTEMPTS = 100
MAX_ATTEMPT_TIME = 0.05 # seconds


xdim = 17
ydim = 9

init_pos = (8, 4)

end = -1
start = -2

# Setting up the valid movements based on the allowed letters
valid_chars = string.printable  # Modify the allowed characters here
alphabet = " .o+=*BOX@%&#/^SE"  # The symbols that count in the picture
# If we ever use the last symbols, this won't work!
v0char = {}
v1char = {}
v2char = {}

for c in valid_chars:
    o = ord(c)
    o0 = (o >> 0) % 4
    o1 = (o >> 2) % 4
    o2 = (o >> 4) % 4
    o3 = (o >> 6) % 4
    if not o0 in v0char:
        v0char[o0] = set([])
    v0char[o0].add(o1)
    if not (o0, o1) in v1char:
        v1char[o0, o1] = set([])
    v1char[o0, o1].add(o2)
    if not (o0, o1, o2) in v2char:
        v2char[o0, o1, o2] = set([])
    v2char[o0, o1, o2].add(o3)


def input_grid(remote = None):

    if remote == None:
        print("Enter the grid to solve:")
        print("+---[CYBEARS]----+")
        p = [input()[1:] for _ in range(ydim)]
        print("+----[SIGIL]-----+")
    else:
        p = remote

    grid = {}
    for y in range(ydim):
        for x in range(xdim):
            c = p[y][x]
            if c == 'S':
                grid[x, y] = start
            elif c == 'E':
                grid[x, y] = end
            elif c == ' ':
                pass
            else:
                grid[x, y] = alphabet.index(c)
    return grid


class state:
    def __init__(self, grid, pos, end_pos, instr, step):
        self.grid = grid
        self.pos = pos
        self.end_pos = end_pos
        self.instr = instr
        self.step = step

    def adj_cells(self, c):
        x, y = c
        yi = min(y + 1, ydim - 1)
        yd = max(y - 1, 0)
        xi = min(x + 1, xdim - 1)
        xd = max(x - 1, 0)
        return {(xi, yi), (xi, yd), (xd, yi), (xd, yd)}

    def is_reachable(self):
        all_grid = self.grid.keys()
        seen = {self.end_pos}
        search_stack = [self.end_pos]

        while len(search_stack) > 0:
            search_pos = search_stack.pop()
            for a in self.adj_cells(search_pos):
                if a in self.grid:
                    if not a in seen:
                        seen.add(a)
                        search_stack.append(a)

        seen.add(init_pos)
        return len(all_grid) == len(seen)

    def successors(self):
        x, y = self.pos
        r = []
        yi = min(y + 1, ydim - 1)
        yd = max(y - 1, 0)
        xi = min(x + 1, xdim - 1)
        xd = max(x - 1, 0)
        for (xn, yn, xys) in [(xd, yd, 0), (xi, yd, 1), (xd, yi, 2), (xi, yi, 3)]:
            if self.step % 4 == 0:
                if xys not in v0char:
                    continue
            elif self.step % 4 == 1:
                if xys not in v0char[self.instr[-1]]:
                    continue
            elif self.step % 4 == 2:
                if xys not in v1char[self.instr[-2], self.instr[-1]]:
                    continue
            else:
                if xys not in v2char[self.instr[-3], self.instr[-2], self.instr[-1]]:
                    continue

            if (xn, yn) in self.grid:
                cg = self.grid.copy()
                if cg[xn, yn] == 1:
                    del cg[xn, yn]
                    instr = xys
                    ns = state(cg, (xn, yn), self.end_pos, self.instr + [instr], self.step + 1)
                    if ns.is_reachable():
                        r.append(ns)
                elif cg[xn, yn] > 1:
                    cg[xn, yn] -= 1
                    instr = xys
                    ns = state(cg, (xn, yn), self.end_pos, self.instr + [instr], self.step + 1)
                    r.append(ns)
                else:
                    instr = xys
                    ns = state(cg, (xn, yn), self.end_pos, self.instr + [instr], self.step + 1)
                    r.append(ns)

        end_dist = lambda s: abs(s.pos[0] - self.end_pos[0]) + abs(s.pos[1] - self.end_pos[1])
        r.sort(key=end_dist)
        return r

    def success(self):
        if self.pos != self.end_pos:
            return False
        for p in self.grid:
            if not (self.grid[p] == end or self.grid[p] == start):
                return False
        return True


def instr_to_string(i):
    o = ''
    if len(i) % 4 != 0:
        return False, "Gonna fail, path length not a multiple of 4, look for possible infinite paths between 'S' and 'E'"
    while len(i) > 0:
        h = i[:4]
        o += chr((h[0] << 0) + (h[1] << 2) + (h[2] << 4) + (h[3] << 6))
        i = i[4:]
    return True, o


def solve_grid(remote = None):
    init_grid = input_grid(remote)
    for cell in init_grid:
        if init_grid[cell] == end:
            end_pos = cell

    init_instr = []
    init_state = state(init_grid, init_pos, end_pos, init_instr, 0)

    s = [init_state]

    seen_grid = []

    while len(s) > 0:
        h = s.pop()
        if len(h.instr) % 4 == 0:
            if h.grid in seen_grid:
                continue
            else:
                seen_grid.append(h.grid)
        if h.success():
            print("Output:", instr_to_string(h.instr))
            return instr_to_string(h.instr)
        else:
            for su in h.successors():
                s.append(su)
    return False, "How did I get here?"


def handle_alrm(_, __):
    raise TimeoutError("Solver timed out")

def try_solve(p):
    p.recvline_containsb(b"+---[CYBEARS]----+")
    x = [l[1:] for l in p.recvlinesS(9)]
    p.recvuntilb(b">>")

    print("Attempting to solve")
    # Set up an alarm to break us out of potentially unsolvable attempts
    signal.signal(signal.SIGALRM, handle_alrm)
    signal.setitimer(signal.ITIMER_REAL, MAX_ATTEMPT_TIME)
    start = time.time()
    try:
        valid, answer = solve_grid(x)
    finally:
        # Cancel the alarm if we got out of the solve logic
        signal.alarm(0)
    print("Took {:0.3f} seconds".format(time.time() - start))

    if not valid:
        raise ValueError(answer)
    send_answer = base64.b64encode(answer.encode())
    p.sendline(send_answer)
    return p.readallS()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action="store_true")
    parser.add_argument('-p', '--probe', action="store_true")
    parser.add_argument(
        '-r', '--remote', required=False,
        help="The address:port of the remote server hosting the challenge",
    )
    args = parser.parse_args()

    # The slow prints need a decent timeout to avoid killing us
    pwn.context.timeout = 10
    if args.debug:
        pwn.context.log_level = "debug"

    # Make a factory for connecting to or spinning up the target
    if args.remote != None:
        host = args.remote.split(":")[0]
        port = int(args.remote.split(":")[1])
        factory = functools.partial(pwn.remote, host, port)
    else:
        # We can make it go fast if we're running it locally
        factory = functools.partial(
            pwn.process,
            ["python3", "./src/server.py"],
            env={"SIGIL_SLOW": "0"},
        )

    if args.probe:
        print("Probing the target for liveness")
        p = factory()
        header = p.recvline_containsb(b"+---[CYBEARS]----+")
        if header:
            print("Received sigil header: {!r}".format(header))
        else:
            raise TimeoutError("Failed to get sigil")
    else:
        for i in range(MAX_ATTEMPTS):
            p = factory()
            try:
                flag = try_solve(p)
            except TimeoutError:
                print("Timed out ({}/{})".format(i+1, MAX_ATTEMPTS))
            except ValueError:
                print("Solve logic barfed ({}/{})".format(i+1, MAX_ATTEMPTS))
            else:
                print("Answered and got response!\n{}".format(flag))
                assert "cybears{0ne_of_m4ny_pr0b4ble_4nsw3rz}" in flag
                break
            finally:
                p.close()
        else:
            print("Failed to get a flag in {} attempts".format(MAX_ATTEMPTS))
            sys.exit(1)
