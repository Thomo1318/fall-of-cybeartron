# Sigil Walkthrough

The sigil challenge shows users some random ascii art and gets them to input a
value that creates an identical picture. These images may look familiar, as
they are the same images produced as a "visual hash" of SSH keys. The algorithm
for generating these is well known and relates to the "Drunken Bishop" problem,
simulating a drunken person randomly wandering around a garden.

## Solution
If players had access to the original input, or could generate a collision,
this is relatively straight forward based on the paper in the references.

As only the output is given, one possible way is to perform a "backtracking"
algorithm. Start at the end-point and figure out the possible characters and
directions that could have led there. Store all of these possibilities in a
table and step outwards from there. This can lead to many many branches, and
these need to be intelligently "pruned" or removed based on the impossibility
(or otherwise) of reaching these branches. It is unlikely you will get back to
the exact original, but we just need one to pass the check.

The attached solve script performs this attack, although:
 * Sometimes it never finishes
 * Sometimes it knows it is impossible and fails early
 * Sometimes it works instantly

Just rerun on failure - the server generates a random picture each run.

## References
 * `http://dirk-loss.de/sshvis/drunken_bishop.pdf`
