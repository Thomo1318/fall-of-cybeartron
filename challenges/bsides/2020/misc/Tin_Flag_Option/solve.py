#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import sys
from math import ceil, log2
from numpy import float32

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
parser.add_argument('--verbose', '-v', action='store_true', default=default_verbose, help='Enable more verbose logging')
args = parser.parse_args()

if args.verbose:
    context.log_level = 'debug'

# Change this to point to your binary if you have one
target = './src/chal.py'

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!
if args.remote:
    log.info("Solving against server")
    p = remote(*args.remote.split(':'))
else:
    log.info("Solving against local binary")
    if not os.path.isfile(target):
        log.error("Binary %s does not exist! Have you built the challenge?", target)
        sys.exit(1)
    p = process(target)


def get_answer(a, y):
    # Given two floats a and y, what's the smallest integer x such that x*a and x*a + y both round to the same float.
    bucket = ceil(log2(y))  # This value is what we need to be the distance on the number line between adjacent floats
    precision_limit = (2 << (23 + bucket)) - 2 ** (bucket - 1)
    return ceil(precision_limit / a)


def parse_values(line):
    words = line.split()
    retval = []
    for word in words:
        try:
            f_word = float32(word)
            retval.append(f_word)
        except ValueError:
            pass
    assert len(retval) == 2
    return retval


flag_chars = []
this_flag_char = ''
while this_flag_char != '}':
    line_with_values = p.recvline()
    targ_dist, stride_len = parse_values(line_with_values)
    log.debug('stride length {}, targ_dist {}'.format(stride_len, targ_dist))
    ans = get_answer(stride_len, targ_dist)
    p.recvuntil('take? ')  # Get prompt for answer
    p.sendline(str(ans))
    flag_char_line = p.recvline().decode()  # Assume we got it right
    this_flag_char = flag_char_line[flag_char_line.index("'") + 1]
    log.info("Flag character '{}'".format(this_flag_char))
    flag_chars.append(this_flag_char)

# Now we assert our flag was found.
# The script should exit with 0 if it solved the challenge
# and got the flag and it should return an error code
# if it could not. This is an easy way to fail.
flag = ''.join(flag_chars)
assert 'cybears{b3waRe_pReci$ion_1055!}' in flag

# If we're all good we print it and exit cleanly with a 0
log.success("The flag is: %s", flag)
