# Flags hidden in the banners at the venue

## Notes

* A flag is hidden in each of the BSides CTF 2021 banners
* The mahjong tiles encode a secret message
* For online players, we'll have to tweet a message containing a photo (with good enough resolution) or include the png files in this directory as part of the challenge data.


## Main banner (outside CTF hall)

The full set of mahjong tiles contains: dragons (3), flowers (4), winds (4), seasons (4), 1-9 bamboo (4 each), 1-9 dots (4 each), 1-9 number chars (4 each), and a joker.

* On the main banner, all these tiles are present
    * There are also 8 extra (unused) tiles representing (non-chinese) zodiac signs.
* The intersection created by joining lines between opposing matching tiles falls over a key on the keyboard
    * For example, if you connect all the "4-Bamboo" tiles with straight lines, it falls on the `{` key.
    * The dragons are grouped with the joker to create a group of 4
    * The flowers are 'one matching set', as are the winds, and seasons.
* For the order of decoding, the following makes it doable:
    * The bamboo contains `{`
    * The number chars contain `}`
    * The dots contain obvious flag text
    * The dragons, flowers, winds, and seasons spell the characters: `cybe` at the start of the flag

By way of a diagram:

```
c y b e a r s { p 4 + h _ t 0 _ b s 1 d e 5 _ c t f _ z 3 n }
d f w s 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9 
        bamboo            dots              chars             
```

Since the text on the keyboard is in upper case, both these flags should be accepted:

- `CYBEARS{P4+H_T0_BS1DE5_CTF_Z3N}`
- `cybears{p4+h_t0_bs1de5_ctf_z3n}`


## Admin banner (inside CTF hall)

* Starting from the top left of the banner, tiles are read clockwise
* Dot tiles represent an `octal` encoding of a flag character
* Number character tiles represent a `decimal` encoding of a flag character
* Bamboo tiles represent a `hex` encoding of a character

Extra notes:

* Because there are no 'zero' tiles, I have avoided encodings that result in a zero
* Similarly, hex encodings that result in `a-f` are avoided
* The blank (white dragon) tile at the end is used as a spacer to make the string "null terminated"
* There are no clear hints as to the encoding, however:
    * The poster starts with `123` in number characters, then `61` in hex
    * Some tiles are not present (eg. 8, 9 in octal)

Here is some python code to verify what I read from the flag layout with my eyes works:

```py
# READ FROM BANNER PRINT COPY
enc = 'd123,h61,d95,o152,d79,h75,d82,o156,d61,h59,o137,h74,d48,h57,d97,o162,h64,o65,d
95,h26,d76,o61,h67,d72,o164,h45,d92,o155,h33,o116,d43,o175' 
result = '' 
for s in enc.split(','):
    if s[0] == 'd':
        result += chr(int(s[1:]))
    if s[0] == 'h':
        result += b''.fromhex(s[1:]).decode()
    if s[0] == 'o':
        result += chr(int(s[1:], 8))
print(f"Flag: {result}")
Flag: {a_jOuRn=Y_t0Ward5_&L1gHtE\m3N+}
```

Since the text case is specified, the flag is case sensitive. However, a flag is also accepted in the manifest for those that include `cybears` at the start:

- `{a_jOuRn=Y_t0Ward5_&L1gHtE\m3N+}`
- `cybears{a_jOuRn=Y_t0Ward5_&L1gHtE\m3N+}`

