1.	Optimus Prime  	286	1984
2.	Bumblebee  	268	1984
3.	Megatron  	139	1984
4.	Starscream  	104	1984
5.	Grimlock  	80	1985
6.	Sideswipe  	70	1984
7.	Ironhide  	60	1984
8.	Ratchet  	58	1984
9.	Soundwave  	55	1984
10.	Jazz  	49	1984
11.	Barricade  	40	1990
12.	Prowl  	40	1984
13.	Devastator  	34	1985
14.	Drift  	31	2010
15.	Thundercracker  	30	1984
16.	Ultra Magnus  	30	1986
17.	Brawl  	29	1986
18.	Cliffjumper  	28	1984
19.	Arcee  	27	2001
20.	Mirage  	27	1984
21.	Shockwave  	27	1985
22.	Galvatron  	26	1986
23.	Hound  	26	1984
24.	Jetfire  	26	1985
25.	Skids  	24	1985
26.	Lockdown  	23	2008
27.	Strafe  	21	1987
28.	Blackout  	20	1990
29.	Bonecrusher  	20	1985
30.	Ravage  	20	1984
31.	Skywarp  	20	1984
32.	Smokescreen  	20	1985
33.	Strongarm  	20	2004
34.	Cheetor  	19	1996
35.	Dead End  	18	1986
36.	Mudflap  	18	2005
37.	Snarl  	18	1985
38.	Wheeljack  	18	1984
39.	Blades  	17	1986
40.	Bulkhead  	17	2004
41.	Heatwave  	17	2009
42.	Inferno  	17	1985
43.	Long Haul  	17	1985
44.	Optimus Primal  	17	1996
45.	Scourge  	17	1986
46.	Thrust  	17	1985
47.	Ramjet  	16	1985
48.	Silverbolt  	16	1986
49.	Breakdown  	15	1986
50.	Cyclonus  	15	1986
51.	Frenzy  	15	1984
52.	Jolt  	15	1994
53.	Scavenger  	15	1985
54.	Scorponok  	15	1987
55.	Slug (→ Slag)	 	15	2014
56.	Steeljaw  	15	1986
57.	Air Raid  	14	1986
58.	Blurr  	14	1986
59.	Rampage  	14	1986
60.	Swindle  	14	1986
61.	Buzzsaw  	13	1984
62.	Crosshairs  	13	1987
63.	Hoist  	13	1985
64.	Hot Shot  	13	2001
65.	Rodimus (→ Rodimus Prime)	 	13	2004
66.	Sentinel Prime  	13	2008
67.	Divebomb  	12	1986
68.	Jetstorm  	12	1993
69.	Laserbeak  	12	1984
70.	Menasor  	12	1986
71.	Perceptor  	12	1985
72.	Predaking  	12	2006
73.	Red Alert  	12	1985
74.	Roadbuster  	12	1985
75.	Sideways  	12	2002
76.	Skydive  	12	1986
77.	Topspin  	12	1985
78.	Waspinator  	12	1996
79.	Bludgeon  	11	1989
80.	Chase  	11	1987
81.	Dirge  	11	1985
82.	Mixmaster  	11	1985
83.	Piranacon  	11	1988
84.	Powerglide  	11	1985
85.	Razorclaw  	11	1986
86.	Rollbar  	11	1987
87.	Springer  	11	1986
88.	Ransack  	10	1985
89.	Rhinox  	10	1996
90.	Rumble  	10	1984
91.	Swerve  	10	1986
92.	Unicron  	10	2002
93.	Windblade  	10	2014
94.	Blast Off  	9	1986
95.	Blaster  	9	1985
96.	Blitzwing  	9	1985
97.	Boulder  	9	2011
98.	Crankcase  	9	1988
99.	Demolishor  	9	2002
100.	Downshift  	9	2002
101.	Dreadwing  	9	1994
102.	Iron Man  	9	2008
103.	Kickback  	9	1985
104.	Onslaught  	9	1986
105.	Rattrap  	9	1996
106.	Scrapper  	9	1985
107.	Speed-Bot  	9	2002
108.	Sunstorm  	9	2004
109.	Swoop  	9	1985
110.	Tankor  	9	2000
111.	The Fallen  	9	2007
112.	Vortex  	9	1986
113.	Airazor  	8	1997
114.	Anakin Skywalker  	8	2006
115.	Beachcomber  	8	1985
116.	Brawn  	8	1984
117.	Bruticus  	8	1986
118.	Chromia  	8	2005
119.	Clone Pilot  	8	2006
120.	Computron  	8	1987
121.	Dragstrip (→ Drag Strip)	 	8	1994
122.	Grindor  	8	2002
123.	Groove  	8	1986
124.	Hook  	8	1985
125.	Hot Spot  	8	1986
126.	Knock Out  	8	2002
127.	Kup  	8	1986
128.	Leadfoot  	8	1994
129.	Longarm  	8	2002
130.	Nemesis Prime  	8	2003
131.	Overbite  	8	1988
132.	Sandstorm  	8	1986
133.	Scorn  	8	2014
134.	Sunstreaker  	8	1984
135.	Tracks  	8	1985
136.	Wreck-Gar  	8	1986
137.	Armorhide  	7	2001
138.	Astrotrain  	7	1985
139.	Bluestreak  	7	1984
140.	Crumplezone  	7	2002
141.	Darth Vader  	7	2005
142.	Dirt Boss  	7	2002
143.	Evac  	7	2005
144.	First Aid  	7	1986
145.	Hot Rod  	7	1986
146.	Huffer  	7	1984
147.	Nightbeat  	7	1988
148.	Obi-Wan Kenobi  	7	2005
149.	Spider-Man  	7	2008
150.	Terradive  	7	1993
151.	Transmutate  	7	2006
152.	Vehicon  	7	2011
153.	Whirl  	7	1985
154.	Windcharger  	7	1984
155.	Windrazor  	7	1993
156.	Bisk  	6	2015
157.	Blackarachnia  	6	1996
158.	Dinobot  	6	1996
159.	Fireflight  	6	1986
160.	Fracture  	6	2008
161.	Insecticon  	6	1996
162.	Kid-Bot  	6	2003
163.	Megatronus  	6	2015
164.	Monstructor  	6	1989
165.	Motormaster  	6	1986
166.	Nautilator  	6	1988
167.	Nexus Prime  	6	2005
168.	Overload  	6	1989
169.	Ratbat  	6	1986
170.	Repugnus  	6	1987
171.	Ricochet  	6	1988
172.	Rodimus Prime  	6	1986
173.	Seawing  	6	1988
174.	Sharkticon  	6	2004
175.	Silverstreak (→ Bluestreak)	 	6	2003
176.	Skyquake  	6	1992
177.	Slipstream  	6	2013
178.	Snaptrap  	6	1988
179.	Spinister  	6	1988
180.	Stinger  	6	2014
181.	Strong-Bot  	6	2003
182.	Tarantulas  	6	1996
183.	Tentakil  	6	1988
184.	Thunderhoof  	6	2015
185.	Thunderwing  	6	1989
186.	Treadshot  	6	1990
187.	Vector Prime  	6	2005
188.	Warpath  	6	1985
189.	Wheelie  	6	1986
190.	Acid Storm  	5	2008
191.	Aero-Bot  	5	2003
192.	Alpha Trion  	5	2007
193.	Blight (→ Blot)	 	5	2005
194.	Bombshell  	5	1985
195.	Breakaway (→ Getaway)	 	5	2007
196.	Broadside  	5	1986
197.	Cody Burns  	5	2011
198.	Cosmos  	5	1985
199.	Drag Strip  	5	1986
200.	Drill Bit (→ Drillbit)	 	5	1997
201.	Dropkick  	5	2007
202.	Gas-Bot  	5	2003
203.	Gears  	5	1984
204.	Hammer  	5	1990
205.	High Wire  	5	2002
206.	Hightower  	5	2001
207.	Hun-Gurrr  	5	1987
208.	Landmine  	5	1988
209.	Lugnut  	5	2006
210.	Nightscream  	5	2000
211.	Obsidian  	5	2000
212.	Omega Supreme  	5	1985
213.	Payload  	5	2002
214.	Scattorshot  	5	2005
215.	Seaspray  	5	1985
216.	Shatter  	5	2018
217.	Side Burn (→ Sideburn)	 	5	2001
218.	Skalor  	5	1988
219.	Sky Shadow  	5	1998
220.	Slag  	5	1985
221.	Slash  	5	2014
222.	Slog  	5	1989
223.	Stormcloud  	5	2003
224.	Streetwise  	5	1986
225.	Superion  	5	1986
226.	Tailgate  	5	1986
227.	Terrorsaur  	5	1996
228.	Wideload  	5	1987
229.	Backtrack  	4	2002
230.	Battle Droid  	4	2009
231.	Battletrap  	4	1987
232.	Beastbox  	4	1988
233.	Blowpipe  	4	1987
234.	Brainstorm  	4	1987
235.	Brakedown (→ Breakdown)	 	4	2005
236.	Brimstone  	4	2005
237.	Buzzer-Bot  	4	2003
238.	Comettor  	4	2003
239.	Dragonstorm  	4	2017
240.	Dreadwind  	4	1988
241.	Dune Runner  	4	2003
242.	Elita-1  	4	2009
243.	Firebot  	4	2003
244.	Fixit  	4	1989
245.	Flak  	4	1989
246.	Grapple  	4	1985
247.	Heavy Load  	4	2001
248.	Hubcap  	4	1986
249.	Iceberg  	4	2003
250.	Icepick  	4	1989
251.	King Toots  	4	2019
252.	Makeshift  	4	2003
253.	Metroplex  	4	1986
254.	Motorbreath (→ Motormaster)	 	4	2012
255.	Octopunch  	4	1989
256.	Override  	4	1988
257.	Poo Sham  	4	2019
258.	Quillfire  	4	2015
259.	Ramhorn  	4	1986
260.	Reflector  	4	1986
261.	Rescue Force  	4	1992
262.	Reverb  	4	2005
263.	Rewind  	4	1986
264.	Ro-Tor  	4	2001
265.	Runamuck  	4	1986
266.	Scattershot (→ Scattorshot)	 	4	1987
267.	Scattor  	4	2003
268.	Shrapnel  	4	1985
269.	Skyhammer  	4	1989
270.	Skystalker  	4	1989
271.	Sledge  	4	1990
272.	Sonar  	4	1999
273.	Sparkplug  	4	2002
274.	Sqweeks  	4	2017
275.	Steamhammer  	4	2005
276.	Storm Jet  	4	2002
277.	Thunderclash  	4	1992
278.	Tigatron  	4	1996
279.	Trailcutter (→ Trailbreaker)	 	4	2011
280.	Trypticon  	4	1986
281.	Waterlog  	4	2003
282.	Wildrider  	4	1986
283.	Wreckage  	4	2002
284.	Abominus  	3	1987
285.	Afterburner  	3	1987
286.	Airachnid  	3	2012
287.	Anvil  	3	2016
288.	Arctic Guzzlerush  	3	2019
289.	Backfire  	3	2010
290.	Backstop  	3	2005
291.	Barrage  	3	1985
292.	Bashbreaker  	3	2016
293.	Batsby  	3	2019
294.	Blackjack  	3	1989
295.	Blastcharge  	3	2000
296.	Bodyblock  	3	2008
297.	Bomb-burst  	3	1988
298.	Bombshock  	3	1990
299.	Bonz-eye  	3	2019
300.	Brake-Neck (→ Wildrider)	 	3	2013
301.	Burgertron  	3	2019
302.	Caliburst  	3	1987
303.	Camshaft  	3	2005
304.	Cannonball  	3	2006
305.	Captain Rex  	3	2009
306.	Cerebros  	3	1987
307.	Chop Shop  	3	1985
308.	Chromedome  	3	1987
309.	Cindersaur  	3	1988
310.	Clench  	3	1993
311.	Clogstopper  	3	2019
312.	Cogman  	3	2017
313.	Countdown  	3	1989
314.	Crosswise  	3	2001
315.	Crowbar  	3	2011
316.	Dragonus  	3	2015
317.	Dualor  	3	2002
318.	Eject  	3	1986
319.	Fallback  	3	2005
320.	Fit Ness Monster  	3	2019
321.	Fortress Maximus  	3	1987
322.	Fottle Barts  	3	2019
323.	Frohawk  	3	2019
324.	Game Over  	3	2019
325.	General Grievous  	3	2005
326.	Gnaw  	3	1986
327.	Gunbarrel  	3	2003
328.	Guzzle  	3	1988
329.	Hardhead  	3	1987
330.	Hardshell (→ Bombshell)	 	3	2009
331.	Headstrong  	3	1986
332.	Heavytread  	3	2008
333.	Highbrow  	3	1987
334.	Hulk  	3	2008
335.	Iguanus  	3	1988
336.	Junkheap  	3	2011
337.	Knockdown  	3	2006
338.	Knockout (→ Knock Out)	 	3	1990
339.	Krok  	3	1990
340.	Laceface  	3	2019
341.	Lancelon  	3	2016
342.	Lolly Licks  	3	2019
343.	Lord Doomitron  	3	2016
344.	Mega-Octane  	3	2001
345.	Metalhawk  	3	2012
346.	Mindwipe  	3	1987
347.	Movor  	3	2001
348.	Ms. Take  	3	2019
349.	Nightstick  	3	1987
350.	Nosecone  	3	1987
351.	Oceanglide  	3	2003
352.	Oil Slick  	3	2004
353.	Outback  	3	1986
354.	Over-Run  	3	1990
355.	Overcast  	3	2005
356.	Point Dexter  	3	2019
357.	Primus  	3	2006
358.	Pucksie  	3	2019
359.	Quickmix  	3	1988
360.	Quickslinger (→ Slingshot)	 	3	2013
361.	Quickstrike  	3	1998
362.	Racer-Bot  	3	2004
363.	Rapid Run  	3	2001
364.	Reptron  	3	2003
365.	Ripclaw  	3	2013
366.	Rippersnapper  	3	1987
367.	Roadblock  	3	1989
368.	Rollout  	3	1990
369.	Rook  	3	2002
370.	Runway  	3	2002
371.	Salvage  	3	2008
372.	Sawback  	3	2015
373.	Scamper  	3	1986
374.	Scrapmetal  	3	2006
375.	Screen Fiend  	3	2019
376.	Shadow Striker  	3	2003
377.	Shredder Jack  	3	2019
378.	Signal Flare  	3	2004
379.	Sinnertwin  	3	1987
380.	Sippy Slurps  	3	2019
381.	Sizzle  	3	1988
382.	Skillz Punk  	3	2019
383.	Skullgrin  	3	1988
384.	Sky High  	3	1988
385.	Sky Lynx  	3	1986
386.	Slingshot  	3	1986
387.	Sludge  	3	1985
388.	Slugslinger  	3	1987
389.	Soundblaster  	3	2013
390.	Space Case  	3	1995
391.	Spittor  	3	1997
392.	Springload  	3	2015
393.	Sprinkleberry D'uhnut  	3	2019
394.	Stakeout  	3	1989
395.	Sticky McGee  	3	2019
396.	Stinkeye Stapleton  	3	2019
397.	Strika  	3	2001
398.	Stylor  	3	1987
399.	Sureshock  	3	2002
400.	Swelter  	3	2016
401.	Tidal Wave  	3	2003
402.	Trailbreaker  	3	1984
403.	Triceradon  	3	2000
404.	Twinferno (→ Doublecross)	 	3	2015
405.	Underbite  	3	2015
406.	Undertone  	3	2016
407.	Unilla Icequeencone  	3	2019
408.	Waddlepop  	3	2019
409.	Whisper  	3	1989
410.	Wind Sheer  	3	2001
411.	Windstrike  	3	2016
412.	Wing Saber  	3	2005
413.	Wolverine  	3	2008
414.	X-Brawn  	3	2001
415.	Aimless  	2	1987
416.	Angry Cheese  	2	2019
417.	Apeface  	2	1987
418.	Apelinq  	2	2000
419.	Astroscope  	2	2002
420.	Autobot Drift  	2	2015
421.	Axer  	2	1990
422.	Back  	2	2016
423.	Banzai-Tron  	2	1990
424.	Beast-Bot  	2	2003
425.	Berserker  	2	2017
426.	Big Daddy  	2	1990
427.	Big Shot  	2	1989
428.	Blazemaster  	2	2009
429.	Blot  	2	1987
430.	Brushguard  	2	2005
431.	Buzzclaw  	2	1998
432.	Captain America  	2	2009
433.	Catgut  	2	1990
434.	Chainclaw  	2	1988
435.	Che  	2	2000
436.	Checkpoint  	2	2004
437.	Cicadacon  	2	1997
438.	Circuit  	2	1991
439.	Clampdown  	2	2015
440.	Claw Jaw  	2	1997
441.	Clocker  	2	2005
442.	Cloudburst  	2	1988
443.	Cloudraker  	2	1987
444.	Cog  	2	1987
445.	Cop-Bot  	2	2003
446.	Crankstart  	2	2010
447.	Cruellock  	2	2004
448.	Cutthroat  	2	1987
449.	Cybershark  	2	1997
450.	Darksteel  	2	2011
451.	Darth Maul  	2	2006
452.	Defensor  	2	1986
453.	Deluge  	2	1993
454.	Depth Charge (→ Depthcharge)	 	2	1998
455.	Depthcharge  	2	2003
456.	Dillo  	2	2000
457.	Dimlit  	2	2019
458.	Dirt Rocket  	2	2007
459.	Doom-Lock  	2	2004
460.	Double Clutch  	2	1995
461.	Double Punch  	2	1991
462.	Doubledealer  	2	1988
463.	Drench  	2	1993
464.	Dropshot  	2	1990
465.	Duststorm  	2	2005
466.	Eagle Eye  	2	1990
467.	Electro  	2	1994
468.	Emissary  	2	2016
469.	Emperor Palpatine  	2	2006
470.	Excellion  	2	2006
471.	Fangry  	2	1988
472.	Fastlane  	2	1987
473.	Fearswoop  	2	1993
474.	Fetch  	2	2003
475.	Fire-Bot  	2	2003
476.	Firedrive  	2	2017
477.	Firefly  	2	2015
478.	Firestrike (→ Fireflight)	 	2	2013
479.	Fisitron (→ Ironfist)	 	2	2011
480.	Flamefeather  	2	1988
481.	Flamewar  	2	2005
482.	Flintlock  	2	1988
483.	Flywheels  	2	1987
484.	Fracas  	2	1987
485.	Freezeout  	2	2014
486.	Full-Tilt  	2	1986
487.	Gatoraider  	2	1990
488.	Ginrai  	2	2014
489.	Goldbug  	2	1987
490.	Goob Tube  	2	2019
491.	Grax  	2	1987
492.	Grimstone  	2	2011
493.	Grit Sandwood  	2	2019
494.	Grotusque  	2	1987
495.	Groundbuster  	2	2014
496.	Half-Track  	2	1990
497.	Hammerstrike  	2	2001
498.	Hardtop  	2	2005
499.	Hawt Diggity  	2	2019
500.	Heatseeker  	2	2017
501.	Hi-Test  	2	1988
502.	High Tide  	2	2015
503.	Highjump  	2	1989
504.	Hot Zone (→ Hot Spot)	 	2	2008
505.	Hyperdrive  	2	1989
506.	Impactor  	2	2013
507.	Incinerator  	2	2003
508.	Jackpot  	2	1990
509.	Jhiaxus  	2	2003
510.	Kade Burns  	2	2013
511.	Kidd Klobber  	2	2019
512.	Landfill  	2	1988
513.	Landquake  	2	2005
514.	Leo Prime  	2	2006
515.	Leobreaker  	2	2005
516.	Lionizer  	2	1990
517.	Longhorn  	2	2000
518.	Luke Skywalker  	2	2005
519.	Magnaboss  	2	1997
520.	Major Lee Screwge  	2	2019
521.	Mechatron  	2	2000
522.	Medix  	2	2012
523.	Megaplex  	2	1997
524.	Meister (→ Jazz)	 	2	2004
525.	Midnight Express  	2	2001
526.	Mikaela Banes  	2	2008
527.	Misfire  	2	1987
528.	Mol  	2	2000
529.	Mototron  	2	2002
530.	Mudslinger  	2	1989
531.	Nail Byter  	2	2019
532.	Needlenose  	2	1988
533.	Nightcruz  	2	2002
534.	Nightracer  	2	1995
535.	NRJeez  	2	2019
536.	Octane  	2	1986
537.	Offshoot  	2	2004
538.	Optimal Optimus  	2	1998
539.	Oval  	2	2002
540.	Overkill  	2	1987
541.	Packrat  	2	1997
542.	Pinpointer  	2	1987
543.	Pipes  	2	1986
544.	Pounce  	2	1987
545.	Powerdive  	2	1994
546.	Powertrain  	2	1989
547.	Primal Prime  	2	2000
548.	Punch  	2	2010
549.	Quake  	2	1988
550.	Quickswitch  	2	1988
551.	Railspike  	2	2001
552.	Rapido  	2	1993
553.	Red Hot  	2	1989
554.	Road Rocket  	2	1991
555.	Roadhandler  	2	2004
556.	Roller  	2	2011
557.	Ruination  	2	2003
558.	S'up Dawg  	2	2019
559.	Sandsting  	2	2015
560.	Sawtooth  	2	2016
561.	Scalpel  	2	2009
562.	Scoop  	2	1988
563.	Scorch  	2	1992
564.	Scrap Iron  	2	2006
565.	Searchlight  	2	1987
566.	Shockblast (→ Shockwave)	 	2	2004
567.	Silver-Bot  	2	2003
568.	Singe  	2	1988
569.	Six-Speed  	2	2005
570.	Sixshot  	2	1987
571.	Skid-Z  	2	2001
572.	Sky Blast (→ Skyblast)	 	2	2002
573.	Sky-Byte  	2	2001
574.	Skyblast  	2	2004
575.	Skyboom  	2	2004
576.	Skyburst  	2	2010
577.	Skyfall  	2	1990
578.	Skyjack  	2	1995
579.	Slapper  	2	2001
580.	Slappyhappy  	2	2019
581.	Snow Cat  	2	2004
582.	Solus Prime  	2	2018
583.	Sparkstalker  	2	1988
584.	Spiral  	2	2002
585.	Spud Muffin  	2	2019
586.	Stockade  	2	2004
587.	Storm Cloud (→ Stormcloud)	 	2	1989
588.	Streetsmart  	2	2013
589.	Striker  	2	2000
590.	Submarauder  	2	1988
591.	Suppressor  	2	2008
592.	Surge  	2	1990
593.	Swindler  	2	1989
594.	Tantrum  	2	1986
595.	Thor  	2	2009
596.	Throttle  	2	1988
597.	Thunderblast  	2	2005
598.	Torox  	2	2013
599.	Tow-line  	2	2001
600.	Tracer  	2	1988
601.	Treadbolt  	2	2004
602.	Tricerashot  	2	2016
603.	Triggerhappy  	2	1987
604.	Tripredacus  	2	1997
605.	Twinstrike (→ Sinnertwin)	 	2	2013
606.	Twintwist (→ Twin Twist)	 	2	2013
607.	Undermine  	2	2005
608.	Velocirazor  	2	2015
609.	Venom  	2	1985
610.	Venus Frogtrap  	2	2019
611.	Vorath  	2	1987
612.	Walker Cleveland  	2	2011
613.	War Machine  	2	2010
614.	Wedge  	2	2001
615.	Weirdwolf  	2	1987
616.	Wildbreak  	2	2017
617.	Windsweeper  	2	1988
618.	Zapmaster  	2	2003
619.	 	1	2018
620.	Acid Wing	 	1	2013
621.	Aerobot	 	1	2002
622.	Afterbreaker	 	1	2016
623.	Afterburst	 	1	2013
624.	Ahsoka Tano	 	1	2009
625.	Air Hammer	 	1	1998
626.	Airlift	 	1	2011
627.	Airlift Andy	 	1	2002
628.	Airraptor	 	1	2000
629.	Airwave	 	1	1989
630.	Alchemist Prime	 	1	2018
631.	Alpha Bravo	 	1	2015
632.	Alpha Trizer	 	1	2014
633.	Alpha-Quintesson	 	1	2005
634.	Amalgamous Prime	 	1	2018
635.	Anakin	 	1	2012
636.	Antagony	 	1	1998
637.	Anti-Blaze	 	1	2006
638.	Ape-Linq	 	1	2004
639.	Apex	 	1	2016
640.	Aquablast	 	1	1993
641.	Aquafend	 	1	1993
642.	Arachnid	 	1	1996
643.	Arachnoids	 	1	2015
644.	Arcana	 	1	1987
645.	Ark	 	1	2006
646.	Armordillo	 	1	1996
647.	Ascentor	 	1	2006
648.	Astro-Hook	 	1	2006
649.	Astro-Line	 	1	2006
650.	Astro-Sinker	 	1	2006
651.	AT-AT Driver	 	1	2007
652.	Attack Cruiser	 	1	1990
653.	Autobot Jazz	 	1	2011
654.	Autobot Spike	 	1	2015
655.	Autofire	 	1	2007
656.	Axalon	 	1	2006
657.	Axel Frazier	 	1	2011
658.	Axor	 	1	2010
659.	B.A.T.	 	1	2016
660.	B'Boom	 	1	1997
661.	Backblast	 	1	2006
662.	Backstreet	 	1	1988
663.	Backwind	 	1	2010
664.	Bantor	 	1	1998
665.	Banzaitron	(→ Banzai-Tron)	 	1	2010
666.	Basher	 	1	1991
667.	Battle Unicorn	 	1	2001
668.	Battleslash	 	1	2018
669.	Beacon	 	1	2010
670.	Beast Changer	 	1	2001
671.	Beast-Bot II	 	1	2003
672.	Big Hauler	 	1	1990
673.	Big Hoss	 	1	2011
674.	Birdbrain	 	1	1989
675.	Bitstream	 	1	2013
676.	Blackwing	 	1	2018
677.	Blast Master	 	1	1990
678.	Blastwave	 	1	2017
679.	Blaze	 	1	1994
680.	Blaze Master	(→ Blazemaster)	 	1	1990
681.	Blowout	 	1	1995
682.	Blunderbuss	 	1	2017
683.	Boba Fett	 	1	2006
684.	Boltflash	 	1	2008
685.	Boomer	 	1	1988
686.	Booster	 	1	2007
687.	Boss	 	1	1992
688.	Brake Neck	 	1	2016
689.	Breacher	 	1	2010
690.	Brisko	 	1	1988
691.	Bristleback	 	1	1989
692.	Brunt	 	1	2019
693.	Buckethead	 	1	2004
694.	Bug Bite	 	1	2007
695.	Bugly	 	1	1988
696.	Bulletbike	 	1	1995
697.	Bullhorn	 	1	2014
698.	Bullwark	 	1	2014
699.	Burnout	 	1	2015
700.	Buzz Saw	(→ Buzzsaw)	 	1	1996
701.	Cad Bane	 	1	2010
702.	Calcar	 	1	1993
703.	Caliburn	 	1	2004
704.	Carnage	 	1	2009
705.	Carnivac	 	1	1988
706.	Carzap	 	1	2015
707.	Catilla	 	1	1988
708.	CatSCAN	 	1	2002
709.	Cement-Head	 	1	1990
710.	Centuritron	 	1	2014
711.	Charger	 	1	1991
712.	Charlie Burns	 	1	2011
713.	Charlie Chopper	 	1	2002
714.	Chasm	 	1	2017
715.	Chewbacca	 	1	2006
716.	Chief Charlie Burns	 	1	2013
717.	Chilla Gorilla	 	1	2019
718.	Chopsaw	 	1	2011
719.	Chopster	 	1	2010
720.	Cincersaur	 	1	2013
721.	Cline	 	1	2015
722.	Clobber	 	1	2016
723.	Clone Captain Keeli	 	1	2011
724.	Clone Commander Cody	 	1	2007
725.	Clone Commander Wolffe	 	1	2011
726.	Clone Gunner	 	1	2010
727.	Cocoa Crazy	 	1	2019
728.	Convex	 	1	2017
729.	Cop-Tur	 	1	2010
730.	Counterpunch	 	1	2018
731.	Crackback	 	1	2014
732.	Cranks	 	1	2019
733.	Crashbash	 	1	2016
734.	Crazybolt	 	1	2016
735.	Crossblades	 	1	1989
736.	Crosscut	 	1	2014
737.	Crumble	 	1	1990
738.	Cryotek	 	1	2002
739.	Crystal Widow	 	1	2004
740.	Cuddletooth	 	1	2019
741.	Cybertron	 	1	2017
742.	Cyberwarp	 	1	2017
743.	Daburu	 	1	2017
744.	Dairu	 	1	2018
745.	Daniel Witwicky	 	1	2017
746.	Dark Scream	 	1	2001
747.	Darkmoon	 	1	2016
748.	Darkmount	(→ Straxus)	 	1	2010
749.	Darkray	 	1	2010
750.	Darksaber	 	1	2003
751.	Darkstream	 	1	2010
752.	Darkwind	 	1	2009
753.	Darkwing	 	1	1988
754.	Daytonus	 	1	2001
755.	Dazlestrike	 	1	2019
756.	Deadlift	 	1	2010
757.	Death's Head	 	1	2014
758.	Deathsaurus	 	1	2005
759.	Decepticharge	 	1	2005
760.	Deep Dive	(→ Deepdive)	 	1	2011
761.	Deepdive	 	1	2006
762.	Deftwing	 	1	1993
763.	Derak	 	1	2014
764.	Detour	 	1	1989
765.	Devcon	 	1	2014
766.	Dia	 	1	2015
767.	Diac	 	1	2017
768.	Digital Dagger	 	1	2007
769.	Dino	 	1	2018
770.	Dino-Bot	 	1	2003
771.	Dinotron	 	1	2000
772.	Dion	 	1	2010
773.	Direct-Hit	 	1	1990
774.	Dirtbag	 	1	1995
775.	Dispensor	 	1	2008
776.	Doc Green	 	1	2013
777.	Dogfight	 	1	1988
778.	Doggie	 	1	2003
779.	Doombox	 	1	2018
780.	Doomshot	 	1	2017
781.	Doublecross	 	1	1987
782.	Doubleheader	 	1	1989
783.	Doublepunch	 	1	2010
784.	Dr. Arkeville	 	1	2015
785.	Dr. Moggly	 	1	2019
786.	Dr. Morocco	 	1	2013
787.	Dread Pirate Crew	 	1	2014
788.	Dreadnaut	 	1	2017
789.	Drillbit	 	1	2006
790.	Duderoni	 	1	2019
791.	Duros	 	1	1987
792.	Eclipse	 	1	2014
793.	Ejector	 	1	2009
794.	Elita-One	(→ Elita-1)	 	1	2007
795.	Energon Blaster	 	1	2004
796.	Energon Saber	 	1	2006
797.	Energon Shock Sword	 	1	2011
798.	Energon Sword	 	1	2004
799.	Enigma of Combination	 	1	2016
800.	Erector	 	1	1989
801.	Excavator	 	1	1990
802.	Falcon	 	1	1992
803.	Farside	 	1	2014
804.	Farsight	 	1	2008
805.	Fast-Bot	 	1	2005
806.	Fengul	 	1	2017
807.	Filch	 	1	2015
808.	Finback	 	1	1988
809.	Fire Beast	 	1	1991
810.	Fire Marshal Mike	 	1	2002
811.	Firebolt	 	1	1987
812.	Firecracker	 	1	1995
813.	Fireplug	 	1	2017
814.	Fireshot	 	1	1990
815.	Firetrap	 	1	2010
816.	Fistfight	 	1	1990
817.	Fizzle	 	1	1988
818.	Flare Up	 	1	2014
819.	Flareup	 	1	2005
820.	Flash	 	1	1992
821.	Flash Bang	 	1	2007
822.	Flash-Bot	 	1	2003
823.	Flatfoot	 	1	2008
824.	Flattop	 	1	1989
825.	Flight Pack	 	1	1990
826.	Floodgate	 	1	2014
827.	Fomo	 	1	2019
828.	Forklift	 	1	2002
829.	Forth	 	1	2016
830.	Fractyl	 	1	1997
831.	Free Wheeler	 	1	1989
832.	Freefall	 	1	2014
833.	Freeway	 	1	1987
834.	Frostbite	 	1	2005
835.	Frostferatu	 	1	2019
836.	Full-Barrel	 	1	1990
837.	Fun Gus	 	1	2019
838.	Furos	 	1	2016
839.	G.B. Blackrock	 	1	2015
840.	Galva Convoy	 	1	2015
841.	Galvatronus	 	1	2017
842.	Gas Skunk	 	1	2001
843.	Gasket	 	1	1987
844.	Gatorface	 	1	2017
845.	Gearhead	 	1	1995
846.	Geckobot	 	1	2000
847.	Getaway	 	1	1988
848.	Ghost Rider	 	1	2010
849.	Gigatron	 	1	2012
850.	Glacius	 	1	2016
851.	Glitch	 	1	1990
852.	Glug	 	1	2017
853.	Glyph	 	1	2002
854.	Gobots	 	1	1993
855.	Goldfire	(→ Goldbug)	 	1	2014
856.	Gorge	 	1	2017
857.	Gorilla-Bot	 	1	2005
858.	Gorillabot	 	1	2002
859.	Gort	 	1	1987
860.	Grabuge	 	1	2016
861.	Graham Burns	 	1	2013
862.	Grand Slam	 	1	1988
863.	Grappel	(→ Grapple)	 	1	2011
864.	Greasepit	 	1	1989
865.	Greaser	 	1	1990
866.	Great Byte	 	1	2017
867.	Greenlight	 	1	2019
868.	Grimwing	 	1	2013
869.	Grindcore	 	1	2008
870.	Grit	 	1	1990
871.	Grizzly-1	 	1	1998
872.	Grommet	 	1	1987
873.	Ground Hog	 	1	1990
874.	Groundbreaker	 	1	1988
875.	Groundpounder	 	1	1990
876.	Groundshaker	 	1	1989
877.	Groundspike	 	1	2011
878.	Growl	 	1	1990
879.	Gunrunner	 	1	1988
880.	Gusher	 	1	1990
881.	Gutcruncher	 	1	1990
882.	Hacker X-3	 	1	2008
883.	Hailstorm	 	1	2010
884.	Hairsplitter	 	1	1988
885.	Han Solo	 	1	2006
886.	Hangnail	 	1	2014
887.	Hatchet	 	1	2011
888.	Hauler-Bot	 	1	2003
889.	Haywire	 	1	1987
890.	Hazard	 	1	2017
891.	Headlock	 	1	2013
892.	Headmaster	 	1	2015
893.	Heater	 	1	1988
894.	Heave	 	1	1990
895.	Heavy Tread	(→ Heavytread)	 	1	1990
896.	Heli-Pack	 	1	1990
897.	Hemocron	 	1	2014
898.	High Beam	 	1	1995
899.	High Score	 	1	2007
900.	Highline 1070	 	1	2008
901.	Hightail	 	1	2006
902.	HiQ	 	1	1988
903.	Holepunch	 	1	1988
904.	Hooligan	 	1	1995
905.	Horri-bull	 	1	1988
906.	Hosehead	 	1	1988
907.	Hot House	 	1	1989
908.	Hotlink	 	1	2013
909.	Hotwire	 	1	1988
910.	Howlback	 	1	2018
911.	Hubs	 	1	1990
912.	Human Torch	 	1	2008
913.	Hurricane	 	1	1992
914.	Hydradread	 	1	1993
915.	Hydraulic	 	1	1990
916.	Hydro-Pack	 	1	1990
917.	Hyperfire	 	1	2016
918.	Icebird	 	1	2000
919.	Imperial Trooper	 	1	2008
920.	Infernocus	 	1	2017
921.	Infinitus	 	1	2016
922.	Injector	 	1	1998
923.	Iquanox	 	1	2014
924.	Ironeye	 	1	2014
925.	Ironfist	 	1	1993
926.	Ironworks	 	1	1989
927.	Jack Tracker	 	1	2011
928.	Jango Fett	 	1	2006
929.	Jawbreaker	 	1	1999
930.	Jeorge Figueroa	 	1	2008
931.	Jetblade	 	1	2010
932.	Joyride	 	1	1988
933.	Jumpstream	 	1	2016
934.	K-9	 	1	1997
935.	Kick-Off	 	1	1990
936.	Kick-Over	(→ Kick-Off)	 	1	2012
937.	King Atlas	 	1	2004
938.	Kit Fisto	 	1	2009
939.	Knok	 	1	1988
940.	Kobushi	 	1	2006
941.	Kreb	 	1	1988
942.	Krunix	 	1	2010
943.	Krunk	 	1	1987
944.	Landshark	 	1	2009
945.	Landslide	 	1	2006
946.	Lazerback	 	1	2013
947.	Lazerbolt	 	1	2014
948.	Lazorbeak	(→ Laserbeak)	 	1	1997
949.	Leader-1	 	1	2002
950.	Leozack	 	1	2009
951.	Liege Maximo	 	1	2018
952.	Lieutenant Thire	 	1	2010
953.	Lift-Ticket	 	1	2015
954.	Liftoff	 	1	2014
955.	Lightspeed	 	1	1987
956.	Lightsteed	 	1	2016
957.	Lightstorm	 	1	2013
958.	Lio Convoy	 	1	2015
959.	Liokaiser	 	1	2016
960.	Lokos	 	1	1988
961.	Longrack	 	1	2005
962.	Longtooth	 	1	1989
963.	Longview	 	1	2007
964.	Lord Zarak	 	1	1987
965.	Loudmouth	 	1	2016
966.	Lube	 	1	1988
967.	Lug	 	1	1988
968.	Lugmutt	 	1	2014
969.	Lugnutz	 	1	2007
970.	Mace Windu	 	1	2007
971.	Magmatron	 	1	2000
972.	Magna Stampede	 	1	2004
973.	Magnaguard	 	1	2009
974.	Mainframe	 	1	1990
975.	Major Mayhem	 	1	2016
976.	Manta Ray	 	1	1994
977.	Manterror	 	1	1997
978.	Marissa Faireborn	 	1	2016
979.	Meanstreak	 	1	1995
980.	Meantime	 	1	2007
981.	Megabolt	 	1	2003
982.	Megazarak	 	1	2004
983.	Meltdown	 	1	1990
984.	Micronus	 	1	2018
985.	Midnighter XR-4	 	1	2008
986.	Mindset	 	1	2011
987.	Minerva	 	1	2011
988.	Missile Master	 	1	1990
989.	Mollox	 	1	2014
990.	Monocle	 	1	2006
991.	Monxo	 	1	2016
992.	Monzo	 	1	1987
993.	Moonracer	 	1	2018
994.	Moonrock	 	1	1990
995.	Motorcycle Drone	 	1	2001
996.	Motorhead	 	1	1990
997.	Motormouth	 	1	1995
998.	Murk	 	1	2017
999.	Muzzle	 	1	1988
1000.	Nacelle	 	1	2015
1001.	Nautica	 	1	2017
1002.	Nebulon	 	1	2015
1003.	Necro	 	1	2017
1004.	Needler	 	1	1991
1005.	Nemesis Breaker	 	1	2006
1006.	Neutro	 	1	1990
1007.	Neutro-Fusion Tank	 	1	1990
1008.	Night Beat	(→ Nightbeat)	 	1	2007
1009.	Night Viper	 	1	2000
1010.	Nightbird	 	1	2015
1011.	Nightflight	 	1	1989
1012.	Nightglider	 	1	1999
1013.	Nitro	 	1	2017
1014.	Nobeeoh	 	1	2019
1015.	Noble-Savage	 	1	2001
1016.	Noctorro	 	1	1998
1017.	Nosedive	 	1	2014
1018.	Nova Prime	 	1	2015
1019.	Novastar	 	1	2018
1020.	NRJee	 	1	2019
1021.	Nucleon	 	1	2016
1022.	Offroad	 	1	2015
1023.	Oil Pan	 	1	2010
1024.	Oiler	 	1	1990
1025.	Oilmaster	 	1	2015
1026.	Old Snake	 	1	2016
1027.	Omega Sentinel	 	1	2005
1028.	Omega Spreem	 	1	1991
1029.	Ominus	 	1	2017
1030.	Onshaught	 	1	1986
1031.	Onyx Primal	 	1	1996
1032.	Optimus Maximus	 	1	2012
1033.	Optimus Minor	 	1	1999
1034.	Optimus Prim	 	1	2013
1035.	Orchanoch	 	1	1996
1036.	Orion Pax	 	1	2013
1037.	Overboard	 	1	2017
1038.	Overflow	 	1	1990
1039.	Overhaul	 	1	2005
1040.	Overlord	(→ Gigatron)	 	1	2017
1041.	Overrun	 	1	2003
1042.	Paralon	 	1	2016
1043.	Patrick Donnelly	 	1	2008
1044.	Peacemaker	 	1	1987
1045.	Phaser	 	1	1990
1046.	Photon	 	1	2008
1047.	Pincher	 	1	1989
1048.	Pinpoint	 	1	2010
1049.	Pipeline	 	1	1990
1050.	Pirate Hunter	 	1	2014
1051.	Plo Kloon	 	1	2009
1052.	Pointblank	 	1	1987
1053.	Poison Bite	 	1	2000
1054.	Polar Claw	 	1	1996
1055.	Policeman Pete	 	1	2002
1056.	Power Punch	 	1	1990
1057.	Power Run	 	1	1990
1058.	Power Surge	 	1	2014
1059.	Power Up	 	1	2007
1060.	Powerflash	 	1	1991
1061.	Powerpinch	 	1	1997
1062.	Predacon	 	1	2003
1063.	Predacon Rippersnapper	 	1	2013
1064.	Preditron	 	1	2004
1065.	Prima	 	1	2018
1066.	Prima Prime	 	1	2018
1067.	Professor Wellread	 	1	2019
1068.	Pteraxadon	 	1	2019
1069.	Ptero	 	1	2017
1070.	Punch-Counterpunch	 	1	1987
1071.	Push-Button	 	1	1990
1072.	Pyra Magna	 	1	2016
1073.	Pyro	 	1	1993
1074.	Quick Bow	 	1	2007
1075.	Quicksilver	 	1	2013
1076.	Quig	 	1	1988
1077.	Quintessa	 	1	2017
1078.	Quintus Prime	 	1	2018
1079.	R.E.V.	 	1	2001
1080.	Rad	 	1	1990
1081.	Raddhaxx	 	1	2019
1082.	Rage	 	1	1993
1083.	Raindance	 	1	1988
1084.	Ram Horn	 	1	1997
1085.	Ramulus	 	1	1999
1086.	Rapticon	 	1	2001
1087.	Rav	 	1	2001
1088.	Rav/Cro	 	1	2000
1089.	Razor Claw	(→ Razorclaw)	 	1	2000
1090.	Razor-Sharp	 	1	1990
1091.	Razorbeam	 	1	2010
1092.	Razorbeast	 	1	1996
1093.	Recoil	 	1	1987
1094.	Refute	 	1	2003
1095.	Remorsel	 	1	2019
1096.	Reptilion	 	1	2003
1097.	Rescue Roy	 	1	2001
1098.	Retrax	 	1	1997
1099.	Retro	 	1	1990
1100.	Rev	 	1	1988
1101.	Revolver	 	1	2017
1102.	Riotgear	 	1	2017
1103.	Road Hugger	 	1	1989
1104.	Road Pig	 	1	1995
1105.	Roadburner	 	1	1990
1106.	Roadgrabber	 	1	1988
1107.	Roadhammer	 	1	1989
1108.	Roadhound	 	1	2014
1109.	Roadtrap	 	1	2018
1110.	Robert Epps	 	1	2008
1111.	Robot Master	 	1	2015
1112.	Rodimus Major	(→ Hot Rod)	 	1	2002
1113.	Rodimus Unicronus	 	1	2018
1114.	Roller Force	 	1	1990
1115.	Rootwing	 	1	2019
1116.	Rot Gut	 	1	2014
1117.	Rotorstorm	 	1	1992
1118.	Roughneck	 	1	2014
1119.	Roughstuff	 	1	1989
1120.	Roulette	 	1	2003
1121.	Ruckus	 	1	1988
1122.	Rumbler	 	1	1991
1123.	Run-Over	 	1	2003
1124.	Runabout	 	1	1986
1125.	Rupture	 	1	2017
1126.	Rust Dust	 	1	2016
1127.	S.A. Cheez	 	1	2019
1128.	Saesee Tiin	 	1	2011
1129.	Saesee Tinn	 	1	2007
1130.	Safeguard	 	1	2005
1131.	Sam Witwicky	 	1	2008
1132.	Sawyer Storm	 	1	2011
1133.	Scarem	 	1	1999
1134.	Scattorbrain	 	1	2006
1135.	Scorpulator	 	1	1990
1136.	Scowl	 	1	1989
1137.	Scrap-Bot	 	1	2003
1138.	Scrapheap	 	1	2016
1139.	Scrapmaster	 	1	2014
1140.	Screech	 	1	1991
1141.	Scrounge	 	1	2016
1142.	Scythe	 	1	2006
1143.	Sea Clamp	 	1	1997
1144.	Seaclamp	 	1	2016
1145.	Seawatch	 	1	1989
1146.	Sentinel Maximus	 	1	2004
1147.	Sentry	 	1	2019
1148.	Serpentor	 	1	2015
1149.	Seymour Simmons	 	1	2008
1150.	Sgt. Scrubadub	 	1	2019
1151.	Shadow Raider	 	1	2018
1152.	Shakar	 	1	2015
1153.	Shatterspike	 	1	2016
1154.	Shock Trooper	 	1	2009
1155.	Shokaract	 	1	2000
1156.	Shortround	 	1	2006
1157.	Shuffler	 	1	2017
1158.	Side Swipe	 	1	2006
1159.	Sideburn	 	1	2003
1160.	Sidetrack	 	1	1989
1161.	Sights	 	1	1990
1162.	Silberbolt	 	1	1986
1163.	Silencer	 	1	1988
1164.	Siren	 	1	1988
1165.	Six Shot	 	1	2005
1166.	Six-Gun	 	1	1986
1167.	Sixgun	 	1	2019
1168.	Skar	 	1	1988
1169.	Skram	 	1	1993
1170.	Skrapnel	 	1	2014
1171.	Skulk	 	1	2017
1172.	Skullcruncher	 	1	1987
1173.	Skullitron	 	1	2017
1174.	Skullsmasher	 	1	2016
1175.	Sky Rage	 	1	2018
1176.	Sky-Bite	 	1	2010
1177.	Skyfire	 	1	2001
1178.	Skyhopper	 	1	1989
1179.	Skylynx	 	1	2013
1180.	Skysledge	 	1	2017
1181.	Skytread	 	1	2016
1182.	Slamdance	 	1	1988
1183.	Slap Dash	 	1	2010
1184.	Slapdash	 	1	1988
1185.	Slappy Happy	 	1	2019
1186.	Slashmark	 	1	2017
1187.	Slice	 	1	2010
1188.	Slicer	 	1	1991
1189.	Slide	 	1	1990
1190.	Slipstrike	 	1	2014
1191.	Slobber Rock	 	1	2019
1192.	Slow Poke	 	1	1990
1193.	Slugfest	 	1	1987
1194.	Smashdown	 	1	2019
1195.	Smokejumper	 	1	2003
1196.	Smolder	 	1	2010
1197.	Snapdragon	 	1	1987
1198.	Snapper	 	1	1996
1199.	Snare	 	1	1992
1200.	Snarler	 	1	1988
1201.	Snippy Snappy	 	1	2019
1202.	Soverign	 	1	2016
1203.	Spaceshot	 	1	1990
1204.	Spark	 	1	2010
1205.	Sparkcrusher	 	1	2010
1206.	Spasma	 	1	1987
1207.	Speed Dial	 	1	2007
1208.	Speedstream	 	1	1993
1209.	Spike Witwicky	 	1	1987
1210.	Splashdown	 	1	1988
1211.	Spoilsport	 	1	1987
1212.	Spraxus			1	2012
1213.	Sprocket	 	1	1990
1214.	Spy Shot	 	1	2007
1215.	Spy Streak	 	1	2001
1216.	Squawkbox	 	1	1988
1217.	Squawktalk	 	1	1988
1218.	Squeezeplay	 	1	1988
1219.	Stalker	 	1	1992
1220.	Starcatcher	 	1	2006
1221.	Starsaber	 	1	2003
1222.	Stasis Pod	 	1	2016
1223.	Staxx	 	1	1995
1224.	Steel Wind	 	1	2007
1225.	Steelbane	 	1	2017
1226.	Steelshot	 	1	2010
1227.	Stepper	 	1	2015
1228.	Stinkasaurus Rex	 	1	2019
1229.	Stinkbomb	 	1	1999
1230.	Stonecruncher	 	1	1990
1231.	Stormclash	 	1	2016
1232.	Stormhammer	 	1	2017
1233.	Stormshot	 	1	2017
1234.	Stranglehold	 	1	1989
1235.	Stratotronic Jet	1	1990
1236.	Streetstar	 	1	2010
1237.	Strikedown	 	1	1990
1238.	Stripes	 	1	2016
1239.	Stripmine	 	1	2006
1240.	Strong Arm	 	1	2005
1241.	Stronghorn	 	1	2014
1242.	Stuntwing	 	1	2017
1243.	Sudsbeard	 	1	2019
1244.	Sunbeam	 	1	1988
1245.	Sunder	 	1	2013
1246.	Sunrunner	 	1	1989
1247.	Sunspot	 	1	2010
1248.	Sureshot	 	1	1987
1249.	Sweep	 	1	2009
1250.	T-Wrecks	 	1	2000
1251.	Tailpipe	 	1	2011
1252.	Tailspin	 	1	1989
1253.	Tailwhip	 	1	2010
1254.	Tailwind	 	1	1989
1255.	Take-Off	 	1	1991
1256.	Takedown	 	1	1990
1257.	Talon	 	1	1992
1258.	Tank Drone	 	1	2001
1259.	Tap-Out	 	1	2002
1260.	Tarantulus	 	1	2006
1261.	Taxi-Bot	 	1	2004
1262.	Terragator	 	1	1998
1263.	Terranotron	 	1	2001
1264.	Terrashock	 	1	2015
1265.	Terri-Bull	 	1	2016
1266.	Terror-Tread	 	1	1990
1267.	Teslor	 	1	2016
1268.	The Motor Master	1	2011
1269.	The Plop Father	 	1	2019
1270.	Thermidor	 	1	2017
1271.	Thrash	 	1	2017
1272.	Throttler	 	1	2010
1273.	Thunderhead	 	1	2011
1274.	Thundertron	 	1	2012
1275.	TIE Pilot	 	1	2008
1276.	Tigerhawk	 	1	1999
1277.	Tigertron	 	1	2003
1278.	Tight Shot	 	1	2007
1279.	Tiptop	 	1	1988
1280.	Tomahawk	 	1	2010
1281.	Top Spin	 	1	2011
1282.	Top-Heavy	 	1	1990
1283.	Torca	 	1	1998
1284.	Tote	 	1	1989
1285.	Totes Magotes	 	1	2019
1286.	Toxitron	 	1	2011
1287.	Transquito	 	1	1997
1288.	Treadshock	 	1	2017
1289.	Tred Bolt	 	1	1990
1290.	Trenchmouth	 	1	2010
1291.	Trickout	 	1	2017
1292.	Trip-Up	 	1	1990
1293.	Tripredacus Agent	 	1	2001
1294.	Truck-Bot	 	1	2004
1295.	Turbo Board	 	1	1990
1296.	Turbo Master	 	1	1991
1297.	Turbo Racer	 	1	1990
1298.	Turbo-Pack	 	1	1990
1299.	Turbofire	 	1	1993
1300.	Turbomaster	 	1	2010
1301.	Twerple Burple	 	1	2019
1302.	Twin Cast	 	1	2016
1303.	Twin Twist	 	1	1985
1304.	Twitcher F451	 	1	2008
1305.	Tyrannitron	 	1	1990
1306.	Ultra Bee	 	1	2017
1307.	Ultra Mammoth (Magnus)	 	1	2013
1308.	Underbit	 	1	2015
1309.	Undertow	 	1	2011
1310.	Unit-3	 	1	2016
1311.	Uriad	 	1	2019
1312.	Uruaz	 	1	2018
1313.	Vader	 	1	2012
1314.	Vanguard	 	1	1991
1315.	Vanquish	 	1	1990
1316.	Vector Sigma	 	1	2007
1317.	Venin	 	1	2014
1318.	Vertebreak	 	1	2013
1319.	Vice Grip	 	1	1998
1320.	Victorion	 	1	2016
1321.	Viper	 	1	2015
1322.	Virulent Clones	 	1	2005
1323.	Volcanicon	 	1	2014
1324.	Volks	 	1	1985
1325.	Volt	 	1	1994
1326.	Vroom	 	1	1989
1327.	W.A.R.S.	 	1	2001
1328.	Warpat	 	1	2015
1329.	Waruders	 	1	2015
1330.	Waverider	 	1	1988
1331.	Wheel Blaze	 	1	1990
1332.	Wide Load	 	1	2014
1333.	Wildfly	 	1	1989
1334.	Windbreaker	 	1	1993
1335.	Windburn	 	1	2010
1336.	Windmill	 	1	1991
1337.	Windshear	 	1	2014
1338.	Wingblade	 	1	2008
1339.	Wingspan	 	1	1987
1340.	Wingthing	 	1	1990
1341.	Wire Tap	 	1	2007
1342.	Wolfang	 	1	1996
1343.	Wolfwire	 	1	2016
1344.	Wreckloose	 	1	2005
1345.	Xort	 	1	2016
1346.	Y-Wing Pilot	 	1	2010
1347.	Yoda	 	1	2010
1348.	Zaptrap	 	1	2015
1349.	Zauru	 	1	2019
1350.	Zigzag	 	1	1988
1351.	Zoom Out	1	2007
