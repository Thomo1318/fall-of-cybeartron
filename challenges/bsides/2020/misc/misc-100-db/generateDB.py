import argparse
import random

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--outfile', type=str, default="tf.db", help="Output filename")
parser.add_argument('-s', '--seed', default=None, type=int, help='RNG seed')
args = parser.parse_args()


outfile = args.outfile
rng_seed = args.seed

if rng_seed != None:
    random.seed(rng_seed)

with open("TransformersNames.txt", "r") as f:
    d= f.readlines()

H = []

for x in d:
        l = x.split("\t")
        if len(l) == 4:
                H.append({"name":l[1].strip(),
                        "number":l[2],
                        "year":int(l[3].strip())
                        })

        if len(l) > 4 and l[2] == ' ':
                H.append({"name":l[1].strip(),
                        "number":l[3],
                        "year":int(l[4].strip())
                        })

        if len(l) > 4 and l[2] != ' ':
                H.append({"name":l[1].strip(),
                        "number":l[4],
                        "year":int(l[5].strip())
                        })

#remove names with -> arrows
for h in H:
        if "(" in h['name']:
                h['name'] = h['name'].split("(")[0]

import sqlite3
#conn = sqlite3.connect('tf.db')
conn = sqlite3.connect(outfile)

# Create toys table
conn.execute('''CREATE TABLE toys
             (id INTEGER PRIMARY KEY AUTOINCREMENT ,
                name TEXT,
                number INTEGER ,
                year_introduced INTEGER)''')

for h in H:
        conn.execute("INSERT INTO toys (name, number, year_introduced) VALUES (?,?,?)", (h['name'], h['number'], h['year']))

t = {}
for h in H:
        t[h['year']] = t.get(h['year'],0) + 1

#create likes table
conn.execute('''CREATE TABLE likes(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                like INTEGER,
                datetime INTEGER,
                toyid INTEGER,
                FOREIGN KEY(toyid) REFERENCES toys(id)
                );''')



import datetime
datetime.datetime.fromtimestamp(1487102189)
#Out[83]: datetime.datetime(2017, 2, 15, 6, 56, 29)

#Bsides2019
t1 = datetime.datetime(2019,3,15)
t1ts = int(t1.timestamp())
#Bsides2020
t2 = datetime.datetime(2020,3,15)
t2ts = int(t2.timestamp())
#t1 + 1 month
t3 =  datetime.datetime(2019,4,15)
t3ts = int(t3.timestamp())

for i in range(0,2048):
        conn.execute("INSERT INTO likes (like, datetime, toyid) VALUES (?,?,?)", (1, random.randrange(t1ts,t3ts), random.randint(0, 1351)))

conn.commit()
conn.close()

