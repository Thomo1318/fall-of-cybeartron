misc-100-DeeBee
=================

## Notes - Walkthrough
* This challenge introduces players to SQL database analysis
* One line query answers to the questions are as follows
* A: `select year_introduced, sum(1) as s from toys group by year_introduced order by s desc limit 1;`
* B: `select toys.name, SUM(likes.like) as s from likes join toys on toys.id=likes.toyid group by toys.name order by s desc limit 1;`
* C: `select * from toys order by number desc limit 1;`

