import random
from string import printable
from time import time
timed = True
class bucket:
    def __init__(self,name,size):
        self.name = name
        self.size = size
        self.amt = 0
        self.fill_count = 0
        self.empty_count = 0
        self.move_from_count = 0
        self.move_to_count = 0
        self.rand_buff = re_rand()

    def empty(self):
        self.amt = 0
        self.empty_count += 1

    def fill(self):
        self.fill_count += 1
        if self.amt < self.size:
            self.rand_buff = re_rand()
        self.amt = self.size

    def move(self,target):
        self.move_from_count += 1
        target.move_to_count += 1
        if self == target:
            return 0
        target.amt = self.amt + target.amt
        if target.amt > target.size:
            self.amt = target.amt-target.size
            target.amt = target.size
        else:
            self.amt = 0
            target.rand_buff = self.rand_buff

    def print_tail(self):
        msg = "Buffer#%d,Block#%d="%(self.name,self.amt)
        try:
            if self.name != 0 or self.amt != 1:
                if self.amt == 0:
                    raise Exception
                #Check randomisation
                print(msg,self.rand_buff)
            else:
                print(msg,flag)
        except Exception as e:
            print("Print tail failed, likely empty buffer")

def noncompliance():
    print("I'm afraid this is noncompliance. Goodbye.")
    exit()

def print_intro():
    print("""\t--- SIMON'S UNPATENTED RANDOM SOURCE ---
\t\tType 'h' for commands.""")

def print_help():
    print("""Separate multiple commands with ';'

    pX  - Print the last block of buffer X
    eX  - Empty the contents of buffer X
    fX  - Fill the contents of buffer X
    u   - Print buffer usage statistics
    mXY - Move the contents of buffer X to buffer Y.
    r   - Repeat the last command string
    a   - Answer
    h   - Print this help message
    """)
def print_memo():
    print("""As I was going to St. Ives?
  I met a man with seven wives.
    Every wife had seven sacks,
      Every sack had seven cats,
        Every cat had seven kittens.
          Kittens, cats, sacks, wives,
            How many were going to St. Ives?

My flag is in the smallest buffer, the block is the answer. Call me in 30 seconds or ^D.""")

def print_usage():
    header = "B#\tCurrent\tMax\t#Fill\t#Empt\t#MOut\t#MIn"
    print(header)
    for b in buckets:
        print("B%d\t%d\t%d\t%d\t%d\t%d\t%d"%(b.name,b.amt,b.size,b.fill_count,b.empty_count,b.move_from_count,b.move_to_count))

def validate_command(cmd):
    if cmd == "":
        pass
    else:
        try:
            i = cmd[0]
            if i == "a":
                print_memo()
            elif i == "p":
                t = int(cmd[1])
                buckets[t].print_tail()
            elif i == "e":
                t = int(cmd[1])
                buckets[t].empty()
            elif i == "f":
                t = int(cmd[1])
                buckets[t].fill()
            elif i == "u":
                print_usage()
            elif i == "m":
                t = int(cmd[1])
                t2 = int(cmd[2])
                buckets[t].move(buckets[t2])
            elif i == "h":
                print_help()
            else:
                print("Unknown command '%s'"%i)
        except Exception as e:
            print("Not sure how to handle that, try 'h'")

primes = [101, 103, 107, 109, 113,
127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
179, 181, 191, 193, 197, 199, 211, 223, 227, 229,
233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
283, 293, 307, 311, 313, 317, 331, 337, 347, 349,
353, 359, 367, 373, 379, 383, 389, 397, 401, 409,
419, 421, 431, 433, 439, 443, 449, 457, 461, 463,
467, 479, 487, 491, 499, 503, 509, 521, 523, 541,
547, 557, 563, 569, 571, 577, 587, 593, 599, 601,
607, 613, 617, 619, 631, 641, 643, 647, 653, 659,
661, 673, 677, 683, 691, 701, 709, 719, 727, 733,
739, 743, 751, 757, 761, 769, 773, 787, 797, 809,
811, 821, 823, 827, 829, 839, 853, 857, 859, 863,
877, 881, 883, 887, 907, 911, 919, 929, 937, 941,
947, 953, 967, 971, 977, 983, 991, 997]

i = random.randint(0,len(primes)-1)
j = i
while j == i:
    j = random.randint(0,len(primes)-1)
k = i
while (k == i) or (k == j):
    k = random.randint(0,len(primes)-1)
#Initialise the buckets
p = [primes[i],primes[j],primes[k]]
p.sort()
def re_rand():
    return ''.join([random.choice(printable) for _ in range(32)])

buckets = [bucket(0,p[0]*p[1]),bucket(1,p[0]*p[2]),bucket(2,p[1]*p[2])]

#Set the flag
target_bucket = min([0,1,2],key=lambda b : buckets[b].size)
target_block = 1
flag = "cybears{y1pp13_k@y@k_0th3r_Buck3t5}"
print_intro()
last_c = []
start_time = time()
while True:
    command = input("CMD: ")
    if len(command) == 4095:
        print("Maximum command length: 4094 characters!")
        noncompliance()
    else:
        cur_time = time()
        if timed and (cur_time > start_time + 30):
            noncompliance()
        commands = command.split(";")
        while "r" in commands:
            i = commands.index("r")
            commands = commands[:i] + last_c + commands[i+1:]
        for c in commands:
            validate_command(c)
        last_c = commands
