#!/usr/bin/env python3

import random
import hashlib
import pathlib
import time
from binascii import hexlify
from itertools import zip_longest

SCRIPT_DIR = pathlib.Path(__file__).parent

# Some script-wide constants and global variables
NUM_PASSWORDS_TO_GUESS_SANS_LENGTH = 200
NUM_PASSWORDS_TO_GUESS_WITH_LENGTH = 200
NUM_INCORRECT_GUESSES_SANS_LENGTH = 6000
NUM_INCORRECT_GUESSES_WITH_LENGTH = 3000
MAX_BATCH_SIZE = 20  # Make this number smaller to make the challenge take longer
PER_BATCH_DELAY = 0.1  # Make this number bigger to make the challenge take longer

withlength = False
perfect_run = False
num_passwords_to_guess = num_passwords_remaining = NUM_PASSWORDS_TO_GUESS_SANS_LENGTH
num_guesses_remaining = NUM_INCORRECT_GUESSES_SANS_LENGTH

status_symbols = {'incorrect_guess': '0',
                  'correct_password': '1',
                  'correct_prefix': '_'}

# Load the password components
pwd_comps = []
with (SCRIPT_DIR / 'pwd_comps.txt').open('r') as c:
    for line in c:
        pwd_comps.append(line.strip().split(';'))

# Set the time var
time_since_last_response = time.time()


def get_flag(num):
    with (SCRIPT_DIR / f'flag{int(num)}.txt').open('r') as f:
        return f.read().strip()


def gen_pwd():
    """
    Generate a new password according to the challenge
    :return: a string to be matched as a 'password', and a UUID to track solves
    """
    pwd = ''
    for component_list in pwd_comps:
        component = random.choice(component_list)
        pwd += component
    # print(pwd)
    m = hashlib.md5()
    m.update(pwd.encode())
    h = hexlify(m.digest()).decode()
    uid = f"{h[:8]}-{h[8:12]}-{h[12:16]}-{h[16:20]}-{h[20:]}"
    return pwd, uid


def gen_multiple_pwds(num, current_pwds, old_pwds):
    """
    Generate multiple new passwords according to the challenge.
    Ensure that at most NUM_PASSWORDS_TO_GUESS*LENGTH unique passwords are given out.
    If a solver wants to request more passwords than they need, send them duplicates of passwords they already have
    :param num: How many passwords are being requested
    :param current_pwds: Dictionary of what passwords have already been sent. May be modified in-place by this function
    :return: The string to send back to the solver containing the requested number of IDs
    """
    if num > num_passwords_to_guess:
        return f"You don't need more than {num_passwords_to_guess} IDs."
    if num <= 0:
        return "I can't give that many IDs."
    retvals = []
    assert 1 <= num <= num_passwords_to_guess
    for _ in range(num):
        if len(current_pwds) >= num_passwords_to_guess:
            # They've already got as many as they need, send back items from current_pwds
            uid = random.choice(tuple(current_pwds.keys()))
            if withlength:
                retvals.append(f"{uid}:{len(current_pwds[uid])}")
            else:
                retvals.append(uid)
        else:
            while True:  # Loop until we get a password we haven't seen before
                pwd, uid = gen_pwd()
                if uid not in current_pwds and uid not in old_pwds:
                    break
            current_pwds[uid] = pwd
            if withlength:
                retvals.append(f"{uid}:{len(pwd)}")
            else:
                retvals.append(uid)

    return ','.join(retvals)


def parse_response(response, current_pwds):
    global num_guesses_remaining, num_passwords_remaining, time_since_last_response

    # Sleep for as long as it takes until the time since last response is PER_BATCH_DELAY
    time_delay = PER_BATCH_DELAY - (time.time() - time_since_last_response)
    if time_delay > 0:
        time.sleep(time_delay)
    time_since_last_response = time.time()

    retvals = []
    for count, response_component in enumerate(response.split(',')):
        if count > MAX_BATCH_SIZE:
            retvals.append("Maximum batch size reached")
            break
        if ':' not in response_component:
            retvals.append(f"Couldn't interpret '{response_component}' as an ID/guess pair.")
            break
        else:
            uid, pwd_guess = response_component.split(':', maxsplit=1)
            if uid not in current_pwds:
                retvals.append(f"'{uid}' is not an ID you've been issued.")
                break
        pwd = current_pwds[uid]
        if pwd is None:
            # Already solved this password
            retvals.append(status_symbols['correct_password'])
            # Move on to the next one, don't get credit for solving this again
            continue
        res = check_pwd(pwd_guess, pwd)
        if res is None:  # pwd_guess is a prefix of the password (or vice versa)
            retvals.append(status_symbols['correct_prefix'])
        elif res is False:
            retvals.append(status_symbols['incorrect_guess'])
            num_guesses_remaining -= 1
        elif res is True:
            retvals.append(status_symbols['correct_password'])
            num_passwords_remaining -= 1
            current_pwds[uid] = None  # Can't get credit for solving this one again
        # Check if we've run out of guesses
        if not num_guesses_remaining:
            retvals.append("You're out of guesses. Maybe try a different approach.")
            break
        # Check if we've run out of passwords
        if not num_passwords_remaining:
            assert all(x is None for x in current_pwds.values())
            retvals.append("Congratulations! You've guessed all the passwords!")
            break
    return ','.join(retvals)


def check_pwd(sample, pwd):
    for s, p in zip_longest(sample, pwd):
        if s is None:
            # Submitted guess is too short
            return None  # Not an incorrect guess, but neither is it a complete password
        if p is None:
            # Submitted guess is too long, but has the correct pwd as a prefix.
            # Do I want to treat that differently? Not at this stage
            return None  # Not an incorrect guess, but neither is it a complete password
        if p != s:
            return False
        # If we get to here, the two characters must be equal
        assert p == s
    # If we get to here, then the sample exactly matches the password
    return True


def do_chal(passwords_guessed_last_time=None):
    global num_guesses_remaining, num_passwords_remaining
    if passwords_guessed_last_time is None:
        passwords_guessed_last_time = {}

    passwords_to_guess = {}

    initial_prompt = '''You need to prove your efficiency at password guessing.
Send a single natural number to get that many IDs, each representing a password to guess.
Send a comma separated list of ID:guess (up to {} at a time) to get back a comma separated list of results.
'''.format(MAX_BATCH_SIZE)
    if withlength:
        initial_prompt = '''You need to prove even more efficiency at password guessing.
Send a single natural number to get that many IDs, each representing a password to guess.
Send a comma separated list of ID:guess (up to {} at a time) to get back a comma separated list of results.

This time, you're given the length of each password alongside its ID.
'''.format(MAX_BATCH_SIZE)
    print(initial_prompt)
    while True:
        prompt = f'You still need to recover {num_passwords_remaining} passwords.' + \
         f' You have {num_guesses_remaining} incorrect guesses remaining.'
        response = input(prompt + '\n')
        try:
            num_uids_to_send = int(response)
            print(gen_multiple_pwds(num_uids_to_send, passwords_to_guess, passwords_guessed_last_time))
        except ValueError:  # Couldn't convert response to int. Try to parse as a set of guesses.
            print(parse_response(response, passwords_to_guess))

        # Check if we've run out of guesses
        if not num_guesses_remaining:
            return False
        # Check if we've run out of passwords
        if not num_passwords_remaining:
            print(get_flag(withlength))
            # Check if they had a perfect run
            if withlength and perfect_run:
                if num_guesses_remaining == NUM_INCORRECT_GUESSES_WITH_LENGTH:
                    print("Wow, a perfect run!")
                    print(get_flag(2))

            return passwords_to_guess, num_guesses_remaining == NUM_INCORRECT_GUESSES_SANS_LENGTH
            #  ^ The return value is only relevant for the sans length portion of the challenge.


if __name__ == '__main__':
    chal_part_1, perfect_run = do_chal()
    # chal_par_1 now holds all the unids we gave in part 1, so that we don't give them again in part 2. Or it's False.
    # perfect_run holds (the inverse of) whether they lost any guesses
    if chal_part_1:
        withlength = True
        num_passwords_to_guess = num_passwords_remaining = NUM_PASSWORDS_TO_GUESS_WITH_LENGTH
        num_guesses_remaining = NUM_INCORRECT_GUESSES_WITH_LENGTH
        do_chal(chal_part_1)
