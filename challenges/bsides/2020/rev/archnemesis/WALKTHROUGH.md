# Archnemesis

A multi-layered reverse engineering journey.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

The key is used to decrypt something, which is then run. I'd guess it's an ELF,
but maybe not the _arch_ you're thinking?

</details>

1. Second hint
<details>
<summary>Spoiler warning</summary>

That archnemesis is tricky, can one have several archnemesis's?? Would the
filename of each one be the same too?

</details>

## Steps

<details>
<summary>Spoiler warning</summary>

The binary presents first as a Linux ELF running on aarch64. It is possible to
run the binary on platforms like a Raspberry Pi, or with tools like QEMU's
usermode emulation; however, it is intended that a solution can be achieved
through static reverse-engineering only.

When run, or disassembled, the binary starts by asking for a key, then reading
input in the form of 16 hex-characters from `scanf`. This key is XOR'd against
a rather large global variable. After this, an in-memory file descriptor is
created (`memfd_create`) where the XOR'd result of the global variable is
written. This file descriptor is passed into another function, which
immediately forks, with the parent returning and `wait`ing. The child formats
the string `"/proc/<current pid>/fd/<file descriptor>"`, then runs (with
`execve`). This technique for "in-memory ELF loading" is documented at
https://magisterquis.github.io/2018/03/31/in-memory-only-elf-execution.html.

With this in mind, the assumption should be that the data underneath is another
ELF, with a fixed 8-byte key. This should be enough to determine the key, with:

* the first four bytes being a fixed `b'\x7fELF'`
* the fifth byte is likely `ELFCLASS32` (1) or `ELFCLASS64` (2)
* the sixth byte is likely `ELFDATA2LSB` (1) or `ELFDATA2MSB` (2)
* the seventh byte should be `EV_CURRENT` (1)
* the eigth byte should be `ELFOSABI_SYSV` (0)

Or alternatively, it is even easier since the next 8 bytes should be all NULs -
actually just giving the key!

After extracting this next stage, another binary asking for an 8-byte key in
hex format is presented. It follows the same flow of XORing the key against a
global variable, and writing the new contents into an in-memory file. However,
now it takes this file descriptor expands its size (`ftruncate`) and maps it
into memory (`mmap`). This memory-mapped copy of the in-memory file is passed
into zlib's `inflate` function, synched (`msync`) and unmapped (`munmap`). From
here it follows the normal fork-and-run steps. With this in mind, it is clear
the next file must be a GZIP file. This file format contains (see RFC1952):

* the first two bytes are `b'\x1f\x8b'`
* the third byte should be the number 8
* the fourth byte are the flags, typically just the `FNAME` flag is set (8)
* the final four bytes are UNIX time in little-endian of the time of either
  compression or the modified time of the file - with this we can really only
  assume the last byte (0x60) based on the current time

Since we still have three bytes that are unknown, one could brute-force at this
point, or we could look at those bytes in the next set of 8 bytes. Running with
the assumption that the only flag set is `FNAME`, those bytes are the 3rd
through to 5th characters of the filename. If we assume the file was called
something along `archnemesis` they should correspond to the characters `'chn'`.

The next stage presents similarly, but the key is now taken as 12 characters
and is run through a loop manipulating the data, before XORing. This loop
should be reverse engineered to the point of understanding that is base64.
Leaving a key of between 7 and 9 characters. But the rest is the same, just
a GZIP file. We use the steps above to determine the key, but this time we
should run with the assumption that the filename is there again. This should
result in a key lenght of 9 working and the final stage presenting a flag!

</details>
