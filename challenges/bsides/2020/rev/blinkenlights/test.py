# Checks that the same encoded flag in WALKTHROUGH.md is present in the binary

import base64
import os.path
import sys

EXPECTED_KEY = "0zEREzEzEQvRCzO70TMQu7M70zC9EzEQvRCzO7M7uzO70LvTMLvRMwu9CzPRETMTMQszvRMxETMRC9ELM9ELszu9MxC7M70RMwu9C" \
               "zPRETMQuzO9Mwu9EzEzELM70RMxC7M7sz0Lszu7sz0zEQvRCzO9ETMTMLM7uzO70LvTML0TMTMLM7vQu9Mwszu7szuzO7szu9C70z" \
               "C9CzO70zCzO70LvTML0LM9EzCzPQvTMRC9ELM70zEQvRCzO7sz0QvTMREzCzOzPQszu9C70zCzO9EzELM9EL0zELvRCzO9ELvTMQu" \
               "9ELM9EzETMLvRMxMwu9EL0zC70TMRCzO9EzELuzPRC9Mwu7szvRMxCzPRC7M7"


def test():
    if len(sys.argv) < 2:
        print("usage: %s <binary>" % sys.argv[0])
        return

    binary_file = sys.argv[1]
    if not os.path.isfile(binary_file):
        raise Exception("file %s does not exist" % binary_file)

    # Load whole binary into memory
    binary = open(binary_file, "rb").read()

    # Check the binary includes the expected encoded key
    expected_key = base64.b64decode(EXPECTED_KEY)
    if expected_key not in binary:
        raise Exception("Could not find key in binary")
    print("Confirmed that the expected encoded key is in the binary")


if __name__ == "__main__":
    test()
