//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDI_APP                         101
#define IDI_BOTH                        102
#define IDI_DISCONNECTED                103
#define IDI_LEFT                        104
#define IDI_RIGHT                       105
#define IDS_CONNECTED                   106
#define IDS_DISCONNECTED                107
#define IDR_MENU1                       107
#define IDS_TITLE                       108
#define IDD_LOGIN                       108
#define IDS_NOWCONNECTED                109
#define IDI_CONNECTED                   110
#define IDS_ALREADYCONNECTED            110
#define IDS_NOWDISCONNECTED             111
#define IDS_STRING112                   112
#define IDS_BADPASSWORD                 112
#define IDC_PASSWORD                    1001
#define ID_TRAYMENU_CONNECTTOVPN        40001
#define ID_TRAYMENU_EXITVPNCLIENT       40002

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40003
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
