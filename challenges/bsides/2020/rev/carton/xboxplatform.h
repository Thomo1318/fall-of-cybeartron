/**
 * Carton: Xbox platform implementation
 **/
#pragma once
#include "platform.h"

#include <hal/video.h>
#include <hal/xbox.h>
#include <hal/debug.h>

// FUTURE: led.h doesn't have extern "C" in nxdk
extern "C" {
#include <hal/led.h>
}

#include <cstdarg>


class XboxPlatform : public GamePlatform
{
    public:
        XboxPlatform()
        {
            // Initialize display
            XVideoSetMode(640, 480, 32, REFRESH_DEFAULT);
        }

        virtual void Exit()
        {
            XReboot();
        }

        virtual void FlashLED(LedColor t1, LedColor t2, LedColor t3, LedColor t4)
        {
            XSetCustomLED((XLEDColor) t1, (XLEDColor) t2,  (XLEDColor)t3,  (XLEDColor)t4);
        }

        virtual void DebugPrint(const char * message, ...)
        {
            va_list args;
	        va_start(args, message);
	        debugPrint(message, args);
	        va_end(args);
        }
};
