/**
 * Carton: Good password state. Displays the flag.
 **/
#include "game.h"
#include <cstdio>

extern "C" {
#include <WjCryptLib_AesCbc.h>
#include <WjCryptLib_Md5.h>
}

// Prevent using a rainbow table to crack the hash
const char kPasswordHashSalt[] = "cybears{todo_choose_a_flag}";

// MD5("DIRECTXBORKScybears{todo_choose_a_flag}") = 2b0c24d17ffa8f3221334aeddf1a2f30 
// https://gchq.github.io/CyberChef/#recipe=MD5()&input=RElSRUNUWEJPUktTY3liZWFyc3t0b2RvX2Nob29zZV9hX2ZsYWd9

// IV is: "TODO: CHOOSE IV!": 544f444f3a2043484f4f534520495621
const uint8_t kEncryptedFlagIV[16] = {
    0x54, 0x4f, 0x44, 0x4f, 0x3a, 0x20, 0x43, 0x48, 0x4f, 0x4f, 0x53, 0x45, 0x20, 0x49, 0x56, 0x21
};


// Flag encrypted with AES-128-CBC
// https://gchq.github.io/CyberChef/#recipe=AES_Encrypt(%7B'option':'Hex','string':'2b0c24d17ffa8f3221334aeddf1a2f30'%7D,%7B'option':'Hex','string':'544f444f3a2043484f4f534520495621'%7D,'CBC','Raw','Raw')To_Hex('0x%20with%20comma',0)&input=YV9jYXJ0b25faXNfYV9raW5kX29mX2VnZ3NfYm94
const uint8_t g_EncryptedFlag[] = {
    0xc3,0x3e,0x73,0xe7,0x6d,0xca,0x8f,0xb9,0x9a,0xfb,0x6b,0xa2,0xdd,0x52,0xbc,0x6f,0xd7,0x2f,0x48,0x5f,0x4a,0x39,0x6c,0xcb,0x8d,0x1a,0x2c,0x45,0x29,0x1a,0xbc,0xfb
};


void CGoodPasswordState::HandleInput(SDL_Keycode keycode)
{
    // do nothing
}


CGameState * CGoodPasswordState::Update()
{
    // Decrypt the flag on the first frame
    if (frames == 0)
    {
        // Generate MD5 of password
        Md5Context md5ctx;
        Md5Initialise(&md5ctx);

        // Add password
        Md5Update(&md5ctx, password, strlen(password));

        // Add salt
        Md5Update(&md5ctx, kPasswordHashSalt, strlen(kPasswordHashSalt));

        MD5_HASH hash;
        Md5Finalise(&md5ctx, &hash);

        // Decrypt the flag
        AesCbcContext aesctx;
        AesCbcInitialiseWithKey(&aesctx, hash.bytes, sizeof(hash.bytes), kEncryptedFlagIV);

        char decrypted_flag[sizeof(g_EncryptedFlag) + 1] = { 0 };
        int decrypt_result = AesCbcDecrypt(&aesctx, g_EncryptedFlag, decrypted_flag, sizeof(g_EncryptedFlag));

        sprintf(decrypted_flag_message, "The flag is:\ncybears{%s}", decrypted_flag);
        this->game.RequestRepaint();
    }
    frames++;

    return nullptr;
}

void CGoodPasswordState::Render()
{
    if (frames > 0)
    {
        this->game.GetDefaultFont().Render(decrypted_flag_message, 32, 32);
    }
}