/**
 * Carton: Platform-specific functions. This is used to enable debugging on Windows and cross-compiling for Xbox.
 **/
#pragma once
#include <cstdint>

// Colors for the front LEDs on the Xbox
enum LedColor {
    kOff = 0,
    kGreen = 1,
    kRed = 0x10,
    kOrange = 0x11
};

// Provides all of the platform-specific functions that aren't abstracted by SDL or the Windows headers
class GamePlatform
{
    public:

        // Windows: Exit the process
        // Xbox: Reboot the console
        virtual void Exit() = 0;

        // Windows: do nothing
        // Xbox: Set the front LEDs 
        virtual void FlashLED(LedColor t1, LedColor t2, LedColor t3, LedColor t4) = 0;

        // Windows: Print to the debugger
        // Xbox: Print to the screen using NXDK's debug functions
        virtual void DebugPrint(const char * message, ...) = 0;
};
