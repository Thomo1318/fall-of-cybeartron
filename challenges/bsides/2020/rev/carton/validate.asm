; Shellcode for validating the password
; Expects to be called with the following calling convention:
; int __cdecl ValidatePassword(const char * flag);
; Returns non-zero if the password is correct

BITS 32

main:
    ;db 0xcc        ; debugging
    ;nop
    xor eax, eax

    mov edx, [esp+4]

    cmp byte [edx], 'D'
    jnz done

    cmp dword [edx+1], 'IREC'
    jnz done

    cmp dword [edx+5], 'TXBO'
    jnz done

    
    cmp word [edx+9], 'RK'
    jnz done
    
    cmp byte [edx+11], 'S'
    jnz done

    
    ; Valid
    inc eax
done:
    ret
    
