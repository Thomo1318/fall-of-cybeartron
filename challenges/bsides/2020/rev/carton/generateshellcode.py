import subprocess
import os
import hashlib
import random
import binascii
from PIL import Image

RANDOM_SEED = 42


def assemble_shellcode(shellcode_file, output_file):
    args = [
        "nasm",
        "-f", "bin",
        "-o", output_file,
        "-l", shellcode_file + ".lst",
        shellcode_file
    ]

    print(args)

    subprocess.check_call(args)

    with open(output_file, "rb") as assembled_code:
        return assembled_code.read()


def hash_data(data):
    data_hash = hashlib.md5()
    data_hash.update(data)
    return data_hash.hexdigest()


def hash_file(file_path):
    file_hash = hashlib.md5()
    with open(file_path, "rb") as hash_file:
        while True:
            chunk = hash_file.read(4096)
            if not chunk:
                break
            file_hash.update(chunk)
    return file_hash.hexdigest()


def random_data(number_of_bytes):
    return bytearray(random.getrandbits(8) for _ in range(number_of_bytes))


def generate_anticheat_hashes(filename_list, shellcode_egg):
    hashes = list()

    # Generate some bogus hashes
    for _ in range(3):
        hashes.append(hash_data(random_data(10)))

    # Hash the files we care about
    for filename in filename_list:
        hashes.append(hash_file(filename))

    # Add another bogus hash
    hashes.append(hash_data(random_data(10)))

    # Add a hash that ends with the start of the shellcode egg
    start_hash = hash_data(random_data(10))
    start_bytes = 2
    start_hash = start_hash[0:32 - 2 * start_bytes] + binascii.hexlify(shellcode_egg[0:start_bytes]).decode("utf-8")
    hashes.append(start_hash)

    # Encode the rest of the shellcode
    shellcode_size = len(shellcode_egg)
    pos = start_bytes
    while pos < shellcode_size:
        this_chunk = shellcode_egg[pos:pos + 16]
        pos += len(this_chunk)

        # Pad with random data
        if len(this_chunk) < 16:
            this_chunk += random_data(16 - len(this_chunk))

        hashes.append(binascii.hexlify(this_chunk).decode("utf-8"))

    # Add another bogus hash
    hashes.append(hash_data(random_data(10)))

    return "\n".join(hashes)


def generate_font_bitmap(template_bitmap_filename, output_bitmap_filename, shellcode_egg):
    # Load template bitmap

    template_bitmap = Image.open(template_bitmap_filename)
    assert len(shellcode_egg) < (template_bitmap.width * 3)

    # Each pixel is three bytes
    if len(shellcode_egg) % 3 != 0:
        shellcode_egg += b'\x90' * (3 - (len(shellcode_egg) % 3))

    bitmap_pixels = template_bitmap.load()
    for i in range(0, len(shellcode_egg) // 3):
        red = shellcode_egg[3 * i]
        green = shellcode_egg[3 * i + 1]
        blue = shellcode_egg[3 * i + 2]
        bitmap_pixels[i, template_bitmap.height - 1] = (red, green, blue)

    # Convert the bitmap to an 256 color bitmap with a custom palette
    generated_bitmap = template_bitmap.convert("P", dither=Image.NONE, palette=Image.ADAPTIVE)
    generated_bitmap.save(output_bitmap_filename)


def main():
    # Parameters
    number_of_pieces = 2

    random.seed(RANDOM_SEED)

    # Ensure that the bin directory exists
    if not os.path.isdir("bin"):
        os.mkdir("bin")

    # Asssemble the validation shellcode
    validator = assemble_shellcode("validate.asm", "validate.bin")

    print("Assembled shellcode size: %d bytes" % len(validator))

    # Pad the shellcode with NOPs
    remainder = len(validator) % number_of_pieces
    if remainder > 0:
        validator = b'\x90' * (number_of_pieces - remainder) + validator

    chunk_size = len(validator) // number_of_pieces
    print("Number of eggs: %d" % number_of_pieces)
    print("Egg size: %d" % chunk_size)

    chunks = list()
    for i in range(number_of_pieces):
        tag = b'EGG' + chr(ord('a') + i).encode("utf-8")
        chunk = tag + validator[i * chunk_size:(i + 1) * chunk_size]
        chunks.append(chunk)

    print("Generating font bitmap file")
    shellcode_egg = chunks[0]
    generate_font_bitmap("barcade.bmp", "bin/barcade.bmp", shellcode_egg)

    print("Generating anticheat hash file")
    anticheat_file_list = [
        "bin/default.xbe",
        "bin/barcade.bmp"
    ]

    shellcode_egg = chunks[1]
    anticheat_hashes = generate_anticheat_hashes(anticheat_file_list, shellcode_egg)
    with open("bin/anticheat.txt", "w") as anticheat_file:
        anticheat_file.write(anticheat_hashes)


if __name__ == "__main__":
    main()
