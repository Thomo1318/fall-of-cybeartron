/**
 * Carton: Entrypoint
 **/
#include "platform.h"
#ifdef XBOX
#include "xboxplatform.h"
#else
#include "windowsplatform.h"
#endif

#include <SDL.h>
#include <cstdlib>
#include "platform.h"

#include "egghunter.h"
#include "game.h"

const int kScreenWidth = 640;
const int kScreenHeight = 480;


// SDL Logger callback
void WriteLogToFile(void* userdata, int category, SDL_LogPriority priority, const char* message)
{
    char header[32];
    SDL_RWops * log_file = (SDL_RWops*)userdata;
    if (log_file)
    {
        sprintf(header, "\r\np=%d c=%d ", priority, category);
        SDL_RWwrite(log_file, header, 1, strlen(header));
        SDL_RWwrite(log_file, message, 1, strlen(message));

    }
}

// Entrypoint, called by SDLmain
int main(int argc, char* argv[])
{
    int result = 0;
    BOOL initialized_video = FALSE;
    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_GameController * controller = NULL;
    SDL_RWops* log_file = NULL;

#ifdef XBOX
    XboxPlatform platform;
#else
    WindowsPlatform platform;
#endif

    // Init SDL
    result = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);
    if (result != 0)
    {
        platform.DebugPrint("SDL_Init: %s", SDL_GetError());
        goto cleanup;

    }
    initialized_video = TRUE;

    // Set up logging
#ifdef XBOX
    log_file = SDL_RWFromFile("log.txt", "wb");
    if (log_file != NULL)
    {
        SDL_LogSetOutputFunction(WriteLogToFile, log_file);
        SDL_LogSetAllPriority(SDL_LOG_PRIORITY_DEBUG);
        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Opened log file");
    }
    else
    {
        // Disable SDL logging
        SDL_LogSetOutputFunction(NULL, NULL);

#ifdef CTF_DEBUG
        platform.DebugPrint("Error opening log file\n");
#endif // CTF_DEBUG

    }
#endif // XBOX

    // Create window
    window = SDL_CreateWindow("Carton", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, kScreenWidth, kScreenHeight, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL_CreateWindow failed: %s", SDL_GetError());
        goto cleanup;
    }

    // Create renderer
    renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL_CreateRenderer failed: %s", SDL_GetError());
        goto cleanup;
    }

    // Required for gamepad events to work
    SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");

    // Perform "anti-cheat" integrity check
    DoAntiCheat(platform);

    // Start the game
    {
        CGame game(platform);

        CGameState * initial_state = new CEnterPasswordState(game);
        if (game.Initialize(renderer, initial_state))
        {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Starting game loop");
            game.MainLoop();
        }
    }

    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Exiting game");

cleanup:
    if (controller)
        SDL_GameControllerClose(controller);
    if (renderer)
        SDL_DestroyRenderer(renderer);
    if (window)
        SDL_DestroyWindow(window);
    if (initialized_video)
        SDL_VideoQuit();
    if (log_file)
    {
        SDL_RWclose(log_file);
    }

    platform.Exit();

    return 0;
}
