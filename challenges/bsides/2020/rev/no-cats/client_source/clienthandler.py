import threading
import uuid
from other_player import OtherPlayer
import datetime
import math
import time

class ClientController:

    __instance = None

    @staticmethod
    def getInstance():
        if ClientController.__instance is None:
            ClientController()
        return ClientController.__instance

    def __init__(self):
        if ClientController.__instance is not None:
            raise Exception('ClientController instance was called when it already exists.')
        else:
            ClientController.__instance = self
        self.flag = None
        self.uid = uuid.uuid4()
        self.player_x = 0
        self.players = []
        self.blit_data = []
        self.players_lock = threading.Lock()
        self.worker = threading.Thread(target=self.age_off)
        self.worker.start()


    def handle_player_data(self, player_data):
        if '0' in player_data.keys():
            self.flag = player_data['0']
            return
        for uid, pos in player_data.items():

            found = False
            with self.players_lock:
                for ply in self.players:

                    if ply.uid == uid:
                        found = True
                        ply.destination = pos
                        ply.last_seen = datetime.datetime.now().timestamp()
                        ply.alpha = 190

                if not found:
                    self.players.append(OtherPlayer(uid, pos))

    def age_off(self):
        while True:
            with self.players_lock:
                for ply in self.players:
                    curtime =  datetime.datetime.now().timestamp()
                    since = ply.last_seen
                    if since < (curtime - 10):
                        ply.alpha = math.ceil(ply.alpha / 2)
                    if since < (curtime - 60):
                        self.players.remove(ply)
                        del ply

            time.sleep(.3)