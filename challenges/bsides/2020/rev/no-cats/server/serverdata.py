import threading
import player
import json
import datetime


class ServerController:

    __instance = None

    @staticmethod
    def getInstance():
        if ServerController.__instance is None:
            ServerController()
        return ServerController.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if ServerController.__instance is not None:
            raise Exception("Singleton Pattern already instantiated.")
        else:
            ServerController.__instance = self

        self.players = []
        self.playerdata_lock = threading.Lock()
        self.worker = threading.Thread(target=self.age_off)
        self.worker.start()

    def handle_player_data(self, uid, pos, peer):
        found = False
        with self.playerdata_lock:

            for ply in self.players:
                if ply.private_uid == uid:
                    if ply.addr != peer:
                        print(ply.private_uid, ply.addr)
                        print(ply.addr, peer)
                        return False
                    found = True
                    ply.last_contact = datetime.datetime.now().timestamp()
                    ply.x = pos

        if not found:
            self.new_player(peer, uid, 0, pos)

        return True


    def new_player(self, addr, uid, priv=0, x=0):
        with self.playerdata_lock:
            for ply in self.players:
                if str(ply.private_uid) == uid:
                    return
            newplayer = player.Player(uid, addr, priv, x)
            self.players.append(newplayer)

    def prepare_public_details(self, private_uid):
        with self.playerdata_lock:
            packet = {}
            for ply in self.players:
                if str(ply.private_uid) == private_uid:
                    continue
                packet[str(ply.public_uid)] = ply.x
        return bytes(json.dumps(packet), encoding='utf-8')

    def age_off(self):
        while True:
            for player in self.players:
                if player.last_contact < datetime.datetime.now().timestamp() - 15:
                    with self.playerdata_lock:

                        self.players.remove(player)