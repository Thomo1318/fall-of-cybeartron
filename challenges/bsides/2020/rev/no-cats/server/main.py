#!/usr/bin/python3

import config, tcp_networking, serverdata


def main():

    serverdata.ServerController()
    endpoint = tcp_networking.TCP4ServerEndpoint(tcp_networking.reactor, config.PORT)
    endpoint.listen(tcp_networking.EnlightenmentFactory())
    tcp_networking.reactor.run()


if __name__ == "__main__":
    main()
