# Unbeatable Game Walkthrough

Play scissors paper rock against a computer that is learning the transition
between the players moves as the game is played.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

Have you seen that the program is learning? You can see its memory with the
`?` command.

</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

It's learning from you! Can you teach it to do something, then do something
else?

</details>

## Steps

<details>
<summary>Spoiler warning</summary>

The challenge provides information that the computer is learning from your
actions. This will mean that the first few games you play are pure chance -
it's quite possible to win these games. However, as play continues the
algorithm will measure the bias of moves from the human player and will more
likely win.

One idea could be that, if a purely random set of moves were passed, the player
wouldn't have any bias. This is correct, however the aim is to win 32
consecutive games.

Thankfully the game lets you play as much as you like. With this in mind, if
you could train the machine that you always played one set of moves, then
played a completely different set it would be biased to lose.

The key here is to understand how the machine is learning the player's bias.
This can be done in two ways:
1. Reverse engineer the whole binary in order to understand the algorithm - the
   key here is the `Player` class, which tracks played moves and determines the
   computer's next move
2. Through reverse engineering, find the `?` command in order to print the move
   table and infer the learning algorithm

The challenge with reverse engineering the binary is that Ghidra hasn't been
able to extract the Objective C class information, so some more manual analysis
is required to find the called functions through the objc msg boundary.

At this point, the player should have observed that the learning algorithm is a
Markov model recalling the transition from previous move to next move. To
generate a next move, the computer will take the most recent move played and
use the transition probabilties for that move in order to bias the random
number generator and select a next move.

A suggested simple solution to biasing the computer would be to play the same
move over and over (S -> S, P -> P, R -> R) in order to build up a table like
so:

```
30 moves, 28 games, 3 wins
Last Move: Rock
         |  Scissors  |    Paper   |    Rock
Scissors |          9 |          1 |          0
   Paper |          0 |          9 |          1
    Rock |          0 |          0 |          9
```

With this done enough, the computer will expect that after rock is played that
another rock will follow, therefore it will play paper to beat the rock. If the
player were to play scissors, they would win that game. Importantly, the player
has now played scissors, therefore the computer will expect a scissors to be
played and will counter with rock. The player must therefore play paper to win.
This sequence of R -> P -> S -> ... should be played 32 times to win.

</details>

### Discussion of Probability

This provides a discussion of the likelihood of succeeding at the game after
training the computer with a given sequence of moves. This was done by a
non-mathematician. This is likely to be entirely wrong. I'm sorry in advance
for those that read this.

<details>
<summary>Spoiler warning</summary>

The output of `random(3)` is a number between 0 and 2147483647 inclusive. To
generate the first two moves for a given previous move, the range is divided
into three even-ish[^1] blocks:
* 0 -> 715827882: Computer plays Rock
* 715827883 -> 1431655764: Computer plays Scissors
* 1431655765 -> 2147483647: Computer plays Paper

From there, the transition table is consulted to determine the likelihood of a
particular move being played. In this, we use the term $`M_{p,S}`$ to denote
the count of moves coming from the previous to Scissors, $`M_{p,P}`$ for
previous move to Paper and $`M_{p,R}`$ for previous move to Rock.
* 0 -> $`\frac{2147483647 * M_{p,S}}{\sum_{n \in \{ S, P, R \}} M_{p,n}}`$: Rock
* $`\frac{2147483647 * M_{p,S}}{\sum_{n \in \{ S, P, R \}} M_{p,n}} + 1`$ -> $`\frac{2147483647 * (M_{p,S} + M_{p,P})}{\sum_{n \in \{ S, P, R \}} M_{p,n}}`$: Scissors
* $`\frac{2147483647 * (M_{p,S} + M_{p,P})}{\sum_{n \in \{ S, P, R \}} M_{p,n}} + 1`$ -> 2147483647: Paper

Taking the current move as Rock to illustrate the probability, if we played the
R -> R transition 1000 times, the random ranges are:
* 0 -> 0: Computer plays Rock
* 0 -> 0: Computer plays Scissors
* 1 -> 2147483647: Computer plays Paper

There is a bug that if a 0 comes out of `random(3)` that a Rock will always be
played, but this is a 1 in 2147483647 chance. For this move, it is 99.99999995%
likely that a Paper will be played. Therefore we play a Scissors to counter
this. This brings the total number of moves played from Rock to 1001 and the
number of R -> S transitions to 1. The next time Rock is the current move, the
probabilites are now:
* 0 -> 2145338: Computer plays Rock (0.0999%)
* 2145338 -> 2145338: Computer plays Scissors (0.00%)
* 2145339 -> 2147483647: Computer plays Paper (99.9%)

With another Scissors played (1002 moves; 2 R -> S transitions; and 1000
R -> R transitions):
* 0 -> 4286394: Computer plays Rock (0.200%)
* 4286394 -> 4286394: Computer plays Scissors (0.00%)
* 4286395 -> 2147483647: Computer plays Paper (99.8%)

From this, we can derive a general solution for success of any particular
round $`(n)`$, given a number of training rounds $`(R)`$ as:
$`1 - \frac{n}{R + n}`$.

Since we play this game 3 times for each of the move types, the same
probability is repeated 3 times (power of 3). Finally, we do this for 11
rounds. Another quick observation is that because we had to do one transition
during the training round, we can only consider the first round ($`n = 1`$)
once, the rest begin at round 2. So we do 1 move with round 1; 10 moves from
round 2 to 11, taking us to 31 games; and 1 move within round 12.

Therefore, the general solution for our likelihood of success with $`R`$
training rounds is:
$`P(s) = (1 - \frac{1}{R}) * \prod_{n=2}^{11} (1 - \frac{n}{R + n})^3 * (1 - \frac{12}{R + 12})`$

Therefore, if we play each move 1000 times, then begin to counter the computer,
aiming for the 32 consecutive plays, we have a 92.5% chance of succeeding. For
other training rounds we get the following table:

| Training Rounds | Probabilty of Success |
| --------------: | --------------------: |
|              10 |                0.323% |
|             100 |               47.3%   |
|            1000 |               92.5%   |
|           10000 |               99.2%   |

Of note, our challenge health-check tool uses 1024 training rounds, which was
picked as a nice number before any attempt at a statistical analysis. This
means there's a 7.31% chance of any one health-check failing. Therefore the
likelihood of $`m`$ health checks failing is $`1 - P(s)^m`$, so after just 10
checks there's a 53.2% chance of failure. After 100, there's a 99.9% chance of
failure. No big deal - we just restart and try again :)

[^1]: Scissors is slightly less likely than the others

</details>
