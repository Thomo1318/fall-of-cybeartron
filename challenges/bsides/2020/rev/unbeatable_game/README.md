# Unbeatable Game

Play scissors paper rock against a computer that is learning the transition
between the players moves as the game is played.

## Build

Program is built in a standard Ubuntu Bionic, with an Objective-C compiler and
GNUStep libraries (for Foundation).
