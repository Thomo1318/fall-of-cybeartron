import json
import os
import os.path
import sys
import yaml
import logging
import socket
import requests
import networkx as nx
from string import Template
from ctfd_api import extract_csrf_nonce

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR, 'lib')
sys.path.append(LIB_DIR)

import challenge
from utils import discover_manifests

DEPLOY_DIR = os.path.join(BASE_DIR, ".deploy")


def upload_page(api_session, page, ctfd_dir):
    logging.info(f"Attempting to upload the page content from {page['content']}")
    # Now we can create the page
    with open(
        os.path.join(ctfd_dir, page["content"]), "r"
    ) as pagefile:
        page_content = pagefile.read()

    # Make a new page from scratch
    if page["mode"] == "new":

        # Grab a list of existing pages
        page_get_r = api_session.get("pages")
        assert page_get_r.status_code == 200, page_get_r.content
        response_data = json.loads(page_get_r.content)
        assert response_data["success"] == True, response_data
        # Go hunting for an existing page with the same route
        for page_data in response_data["data"]:
            if page_data["route"] == page["route"]:
                # Delete the existing page
                page_delete_r = api_session.delete(
                    "pages/{}".format(page_data["id"]),
                    json={
                        "page_id": page_data["id"],
                    },
                )
                assert page_delete_r.status_code == 200, page_delete_r.content
                break

        page_post_r = api_session.post(
            "pages",
            json={
                "title": page["title"],
                "route": page["route"],
                "content": page_content,
            },
        )

    # Edit an existing page (assuming you know the page id)
    elif page["mode"] == "edit":
        page_post_r = api_session.patch(
            "pages/{}".format(page["id"]),
            json={
                "page_id": page["id"],
                "content": page_content,
            },
        )

    assert page_post_r.status_code == 200, page_post_r.content
    response_data = json.loads(page_post_r.content)
    page_id = response_data["data"]["id"]

    if page["mode"] == "new":
        logging.info("Uploaded {} as page with id {}".format(page["route"], page_id))
    elif page["mode"] == "edit":
        logging.info("Updated page with id {}, resulting id: {}".format(page["id"], page_id))

def get_server_details(deployment_yaml):
    dns = None
    port = None
    with open(deployment_yaml) as deployment:
        for data in yaml.load_all(deployment, Loader=yaml.SafeLoader):
            metadata = data.get("metadata")
            if metadata:
                annotations = metadata.get("annotations")
                if annotations:
                    try:
                        dns = annotations.get("external-dns.alpha.kubernetes.io/hostname")
                    except KeyError:
                        pass

            spec = data.get("spec")
            if spec:
                ports = spec.get("ports")
                if ports:
                    port = ports[0].get("port")
    if not dns:
        dns = "127.0.0.1"
    return (dns,port)


def find_k8s_deploy(challenge_obj):
    k8s_deploy = os.path.join(
        DEPLOY_DIR, challenge_obj.deploy_name + ".deployment.yaml"
    )
    logging.info(f"Searching for challenge k8s deploy: {k8s_deploy}")
    return k8s_deploy if os.path.exists(k8s_deploy) else None


def get_server_details_for_challenge(challenge_obj):
    k8s_deploy = find_k8s_deploy(challenge_obj)
    return get_server_details(k8s_deploy) if k8s_deploy else None

def get_challenge_id(api_session, challenge_name):
    chall_get_r = api_session.get("challenges?view=admin")
    if chall_get_r.status_code == 200:
        response_data = json.loads(chall_get_r.content)
        assert response_data["success"] == True, response_data
        # Go hunting for an existing challenge with the same name
        for challenge_data in response_data["data"]:
            if challenge_data["name"] == challenge_name:
                return challenge_data["id"]
    raise NameError


def find_or_create_challenge(api_session, challenge_obj, server_details, hostname, **kwargs):
    """
    Create a new challenge for the specified object or update an existing one.
    """
    # Attempt to find an existing challenge with the same name. NOTE: this will
    # duplicate challenges if the name is changed for some reason.
    try:
        challenge_id = get_challenge_id(api_session, challenge_obj.name)
        logging.info("Existing challenge found with id {!r}".format(
            challenge_id
        ))
        api_method = api_session.patch
        api_endpoint = "challenges/{}".format(challenge_id)
    except NameError:
        api_method = api_session.post
        api_endpoint = "challenges"
        pass

    # We may need to substitute some deployment bits in the description field
    description = challenge_obj.description
    if server_details:
        description = Template(challenge_obj.description).substitute(
            target_dns = hostname if hostname else server_details[0],
            target_port = str(server_details[1])
        )
    # Now we can construct the JSON we want to send to the API to either add or
    # update the challenge depending on trivia or regular
    chal_data = {
        "type": challenge_obj.type,
        "state": "visible",
        "name": challenge_obj.name,
        "description": description,
        "category": challenge_obj.category,
        "value": challenge_obj.value,
        # The requirements field in CTFd actually holds both the requirements
        # (called "prerequisites") and if it should be anonymized in the list
        "requirements": {
            "prerequisites": challenge_obj.requirements,
            "anonymize": challenge_obj.anonymize,
        },
    }

    if challenge_obj.type in challenge.DYNAMIC_TYPES:
        dc_value, dc_min, dc_decay = challenge_obj.dynamic_value
        chal_data.update({
            "value": dc_value,
            "decay": dc_decay,
            "minimum": dc_min,
        })
    # And finally submit it to CTFd
    chal_r = api_method(api_endpoint, json=chal_data)
    assert chal_r.status_code == 200, chal_r.content
    response_data = json.loads(chal_r.content)
    challenge_data = response_data["data"]
    return challenge_data


def load_manifest(manifest_path):
    logging.info("Loading challenges from {}".format(manifest_path))
    yield from challenge.Challenge.from_manifest(manifest_path)


def remove_challenge_subobjects(api_session, chal_id, api_name):
    # Delete all of the `<api_name>` things!
    get_r = api_session.get("challenges/{}/{}".format(chal_id, api_name))
    assert get_r.status_code == 200, get_r.content
    response_data = json.loads(get_r.content)
    assert response_data["success"] == True, response_data
    for obj in response_data["data"]:
        # We need to send an `application/json` request so just jam an empty
        # string in as the `json` kwarg to make that happen
        del_r = api_session.delete(
            "{}/{}".format(api_name, obj["id"]), json=""
        )
        assert del_r.status_code == 200, del_r.content

def upload_challenge(api_session, challenge, force=False, hostname=None, **kwargs):
    logging.info("Want to upload %r" % challenge.name)
    if not challenge.use and not force:
        logging.warn("Refusing to upload {!r} without --force".format(challenge.name))
        return
    server_details = get_server_details_for_challenge(challenge)
    challenge_data = find_or_create_challenge(api_session, challenge, server_details, hostname, **kwargs)
    challenge_id = challenge_data["id"]
    # Delete any existing files before uploading the ones we have now - this is
    # the nuclear option which is probably fine...
    remove_challenge_subobjects(api_session, challenge_id, "files")
    for handout in challenge.handouts:
        logging.info("Uploading handout: {!r}".format(handout.file.name))
        file_post_r = api_session.post(
            "files",
            # We use form data here to pack in the files to upload
            data={
                "nonce": api_session.nonce,
                "type": "challenge",
                "challenge_id": challenge_id,
            },
            files={"file": handout.file},
        )
        assert file_post_r.status_code == 200, file_post_r.content
    # Delete any old flags and add new ones
    remove_challenge_subobjects(api_session, challenge_id, "flags")
    for flag in challenge.flags:
        try:
            flag_post_r = api_session.post(
                "flags",
                json={
                    "challenge": challenge_id,
                    **flag,
                },
            )
            assert flag_post_r.status_code == 200, flag_post_r.content
        except TypeError:
            logging.warn("Misconfigured flag in {!r} MANIFEST.md".format(challenge.name))
    # add tags
    remove_challenge_subobjects(api_session, challenge_id, "tags")
    try:
        for tag in challenge.tags:
            tag_post_r = api_session.post(
                "tags",
                json={
                    "challenge_id":challenge_id,
                    "value":tag,
                },
            )
    except TypeError:
        logging.warn("No tags specified for {!r} in MANIFEST.md".format(challenge.name))
    logging.info("Finished uploading %r" % challenge.name)
    return challenge_id

def add_challenges(search_path, api_session, force=False, hostname=None, **kwargs):
    """ Discover manifests in the supplied search path and add them to a CTFD instance.
    Optional keyword arguments for
    """
    g = nx.DiGraph()
    for manifest_path in discover_manifests(search_path):
        for challenge_obj in load_manifest(manifest_path):
            g.add_node(challenge_obj.name, obj=challenge_obj)
            for req in challenge_obj.requirements:
                g.add_edge(req, challenge_obj.name)
    # Sanity check that we're a DAG or die
    if not nx.is_directed_acyclic_graph(g):
        raise ValueError(
            "Challenge requirements have cycles:\n\t{}"
            .format("\n\t".join(nx.simple_cycles(g)))
        )
    # Now upload them in topological order so we can resolve requirements into
    # IDs of challenges uploaded earlier in the loop
    for name in nx.topological_sort(g):
        node = g.nodes[name]
        chal_obj = node["obj"]
        chal_obj.requirements = [g.nodes[o]["id"] for o, _ in g.in_edges(name)]
        node["id"] = upload_challenge(
            api_session, chal_obj, force, hostname, **kwargs
        )

def create_team(api_session, team_name, password, hidden=False):
    team_r = api_session.post(
        "teams",
        json={
            "name": team_name,
            "password": password,
            "hidden": hidden
        }
    )
    assert team_r.status_code == 200
    response_data = json.loads(team_r.content)
    team_id = response_data["data"]["id"]
    return team_id

def join_team(api_session, ctfd_uri, team_id, user_id, team_name, password):

    team_r = api_session.post(
        f"teams/{team_id}/members",
        json={
            "user_id": user_id,
        }
    )
    if team_r.status_code != 200:
        client = requests.session()
        login_get_r = client.get(f'{ctfd_uri}/login')
        nonce = extract_csrf_nonce(login_get_r.content)
        login_post_r = client.post(
            f'{ctfd_uri}/login', data={
                "name": "cybears",
                "password": password,
                "nonce": nonce,
            }
        )
        nonce = extract_csrf_nonce(login_post_r.content)
        #root = client.get(root_uri)
        team_post_r = client.post(
            f"{ctfd_uri}/teams/join",
            # We use form data here to pack in the files to upload
            data={
                "nonce": nonce,
                "name": team_name,
                "password": password,
            },
        )
        assert team_post_r.status_code == 200, team_post_r.content
