import os
import passgen
import logging

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PASS_FILE_LOC =  os.path.join(SCRIPT_DIR, "..", "bin", ".ctfd_admin_pass")

def get_pass():
    with open(PASS_FILE_LOC, "a+") as passfile:
        passfile.seek(0)
        admin_pass = passfile.read()
        if not admin_pass:
            admin_pass = passgen.passgen(length=64)
            passfile.write(admin_pass)
            logging.info(
                "Generated a password for the cybears user: %s"
                % admin_pass
            )
            logging.info("You'll lose it if you run git clean -x!")
    return admin_pass

def set_pass(admin_pass):
    logging.info(f"Writing CTFd admin password to {PASS_FILE_LOC}")
    with open(PASS_FILE_LOC, "w") as passfile:
        passfile.write(admin_pass)
        