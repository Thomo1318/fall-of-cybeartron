#!/usr/bin/env python3
import os
import os.path
import logging
import argparse
import glob
import json
import shutil
import sys
import yaml
from string import Template


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
REPO_BASE = os.path.normpath(os.path.join(SCRIPT_DIR, ".."))

LIB_DIR=os.path.join(REPO_BASE, 'lib')
sys.path.append(LIB_DIR)

from challenge import Challenge
from utils import discover_manifests

K8S_EXTRAS_DIR=os.path.join(REPO_BASE, "infra", "kubernetes-deploy", "extras")
DEPLOYMENT_NAME = "deployment.yaml"
NEXT_PORT = 3000

def create_k8s_deploy(challenge, registry, dns, use_externaldns, output_dir, force=False, reuse_ports=True):
    deploy_script_path = os.path.normpath(os.path.join(output_dir, challenge.deploy_name + '.' + DEPLOYMENT_NAME))
    if challenge.deployment:
        logging.info("Creating deploy script for {}..".format(challenge.name))
        if not challenge.use and not force:
            logging.info(
                "Skipping %r since it opts out of use and we weren't forced",
                challenge.name
            )
            return
        if os.path.isfile(deploy_script_path) and not force:
            logging.info("Skipping existing file {}".format(deploy_script_path))
            return
           
        with open(challenge.deployment.template) as template_file:
            full_domain = challenge.deployment.target_dns_subdomain
            if dns:
                full_domain = full_domain + '.' + dns

            container_port = '''
                    - containerPort: $target_port
                      protocol: "TCP"
'''
            service_port = '''
        - port: $loadbalancer_port
          protocol: "TCP"
          targetPort: $target_port
          name: tcp-$loadbalancer_port-$target_port
'''
            container_ports = ''
            service_ports = ''
            target_loadbalancer_port = None
            for port in challenge.deployment.target_port:
                loadbalancer_port = port
                if not reuse_ports:
                    global NEXT_PORT
                    NEXT_PORT += 1
                    loadbalancer_port = NEXT_PORT
                # set the first loadbalancer port to target_loadbalancer_port
                if not target_loadbalancer_port:
                    target_loadbalancer_port = loadbalancer_port
                container_ports += Template(container_port).substitute(target_port=port)
                service_ports += Template(service_port).substitute(loadbalancer_port=loadbalancer_port, target_port=port)

            patched_solve_script = Template(challenge.deployment.solve_script).substitute(
                target_port=target_loadbalancer_port,
                target_dns=full_domain,
            )
            sleep_loop = '''/bin/sh
                    - -c
                    - while true; do sleep 5; done'''
            if challenge.deployment.container_image_os == 'windows':
                sleep_loop = '''powershell
                    - -Command
                    - "while ($true) {start-sleep -seconds 1}"'''
                patched_solve_script = '''powershell
                            - -Command
                            - ''' + patched_solve_script
            else:
                patched_solve_script = '''/bin/sh
                            - -c
                            - ''' + patched_solve_script

            template = Template(template_file.read())
            server_container = challenge.deployment.container_image if '/' in challenge.deployment.container_image else registry + challenge.deployment.container_image
            healthcheck_container = challenge.deployment.healthcheck_image if '/' in challenge.deployment.healthcheck_image else registry + challenge.deployment.healthcheck_image

            deployment = template.substitute(
                name=challenge.deploy_name,
                category=challenge.category,
                server_container=server_container,
                container_ports=container_ports,
                service_ports=service_ports,
                loadbalancer_port=loadbalancer_port,
                target_dns=full_domain,
                healthcheck_container=healthcheck_container,
                solve_script=patched_solve_script,
                min_replicas=challenge.deployment.min_replicas,
                max_replicas=challenge.deployment.max_replicas,
                replica_target_cpu_percentage=challenge.deployment.replica_target_cpu_percentage,
                healthcheck_interval=challenge.deployment.healthcheck_interval,
                registry=registry,
                node_os=challenge.deployment.container_image_os,
                sleep_loop=sleep_loop,
                cpu_request=challenge.deployment.cpu_request,
                mem_request=challenge.deployment.mem_request,
                cpu_limit=challenge.deployment.cpu_limit,
                mem_limit=challenge.deployment.mem_limit,
                healthcheck_cpu_request=challenge.deployment.healthcheck_cpu_request,
                healthcheck_mem_request=challenge.deployment.healthcheck_mem_request,
                healthcheck_cpu_limit=challenge.deployment.healthcheck_cpu_limit,
                healthcheck_mem_limit=challenge.deployment.healthcheck_mem_limit,
                privileged=challenge.deployment.privileged,
                isolation=challenge.deployment.isolation,
            )
            deployment_docs = tuple(yaml.load_all(deployment, Loader=yaml.SafeLoader))
            # Remove the external-dns reference if we are not using dns. Also a hint for CTFd spin up
            # that we are a local deployment
            if not use_externaldns:
                for doc in deployment_docs:
                    if (doc["kind"] == "Service"):
                        try:
                            doc["metadata"]["annotations"].pop('external-dns.alpha.kubernetes.io/hostname')
                        except KeyError:
                            pass

            logging.info(f"Writing deploy script to {deploy_script_path}")
            with open(deploy_script_path, "w", encoding='utf-8') as deployment_file:
                logging.info(f"Writing deploy script to {deploy_script_path}")
                yaml.dump_all(deployment_docs, deployment_file, default_flow_style=False, explicit_start=True)

def manifest_to_k8s_deploy(challenge_dirs, registry, dns, use_externaldns, force, output_dir, reuse_ports, start_port):
    NEXT_PORT = start_port-1
    os.makedirs(output_dir, exist_ok=True)
    for chal_obj in discover_manifests(challenge_dirs, challenge_obj=True, need_handouts=False):
        try:
            create_k8s_deploy(
                    chal_obj, registry, dns, use_externaldns,
                    output_dir, force, reuse_ports
                )
        except (ValueError, ) as exc:
            logging.error("Skipping %s: %s", chal_obj, exc)
    # Copy extra YAMLs which don't need to be templatised
    k8s_extra_output_dir = os.path.join(
        output_dir, os.path.basename(K8S_EXTRAS_DIR)
    )
    shutil.rmtree(k8s_extra_output_dir, ignore_errors=True)
    shutil.copytree(K8S_EXTRAS_DIR, k8s_extra_output_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')
    parser.add_argument('-f', '--force', action='store_true', default=False, help='Force creation of deployment scripts, will overwrite deployment.yaml files if they already exist')
    parser.add_argument('-r', '--registry', default='registry.gitlab.com/cybears/fall-of-cybeartron/', help='Base container registry location')
    parser.add_argument('-o', '--output-dir', default=os.path.join(REPO_BASE, '.deploy'), help='Directory to place the deployment yamls in. Defaults to .deploy in the repo root.')
    parser.add_argument(
        "--config", action="store",
        required=True,
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r')
    )

    # Local testing stuff
    parser.add_argument('--no-port-reuse', action='store_true', help="Don't reuse ports for loadbalancers. Useful when deploying locally on minikube or k3s, etc")
    parser.add_argument('--no-domain', action='store_true', help="Don't set a domain for loadbalancers, just uses subdomain. Useful when deploying locally on minikube or k3s, etc")
    parser.add_argument('--start-port', type=int, default=3000, help='The port to start with when assigning ports to the load balancers. Only useful when  the --no-port-reuse flag is specified.')

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig()
    
    config = json.loads(args.config.read())

    if args.no_domain:
        args.dns = None
    else:
        args.dns = config['chal_domain']

    manifest_to_k8s_deploy(config["challenge_directories"], registry=args.registry, dns=args.dns, use_externaldns=True, force=args.force, output_dir=args.output_dir, reuse_ports=not args.no_port_reuse, start_port=args.start_port)
