#!/usr/bin/env python3
import os
import argparse
import logging
import sys

BASE_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..',)
LIB_DIR=os.path.join(BASE_DIR, 'lib')
sys.path.append(LIB_DIR)
TERRAFORM_DNS_DIR=os.path.join(BASE_DIR, 'infra', 'dns')


import terraform

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--config", action="store",
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r')
    )
    parser.add_argument(
        "--apply", action="store_true",
        help="Apply the dns config, rather than just retrieving current information" )

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    if args.apply:
        terraform.apply(TERRAFORM_DNS_DIR, os.path.join(os.getcwd(), args.config.name))
        dns = terraform.output(TERRAFORM_DNS_DIR)
    else:
        dns = terraform.state_pull(TERRAFORM_DNS_DIR)
    logging.info(dns)