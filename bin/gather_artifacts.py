#!/usr/bin/env python3
import argparse
import io
import logging
import os
import pathlib
import zipfile

import gitlab

logging.basicConfig(level=logging.INFO)

SERVER = os.environ["CI_SERVER_URL"]
PROJ_ID = os.environ["CI_PROJECT_ID"]
PIPE_ID = os.environ["CI_PIPELINE_ID"]
# We'd like to use `CI_JOB_TOKEN` but it's too restrictive at the moment and we
# can't traverse pipelines or bridges with it :(
TOKEN = os.environ["SECRET_PA_TOKEN"]

# We do lazy instantiation most of the time to skip unnecessary API requests
gl = gitlab.Gitlab(SERVER, TOKEN)
this_proj = gl.projects.get(PROJ_ID, lazy=True)
this_pipeline = this_proj.pipelines.get(PIPE_ID, lazy=True)

UNBOUNDED = object()
def get_nested_jobs(pl, depth=UNBOUNDED):
    # Sort jobs by their ID so we return them in execution order. This seems to
    # be enough to ensure that if any artifacts get pulled and re-pushed in a
    # later job, we'll get the latest version
    for job in sorted(pl.jobs.list(all=True), key=lambda j: j.id):
        yield pathlib.PurePath(job.name), this_proj.jobs.get(job.id)
    if depth is UNBOUNDED or depth > 0:
        depth = depth - 1 if depth is not UNBOUNDED else UNBOUNDED
        for bridge in pl.bridges.list(all=True):
            if bridge.downstream_pipeline is not None:
                cpl_id = bridge.downstream_pipeline["id"]
                logging.info(
                    "Descending through bridge %r (%i) to child pipeline %i",
                    bridge.name, bridge.id, cpl_id,
                )
                cpl = this_proj.pipelines.get(cpl_id, lazy=True)
                yield from (
                    (pathlib.Path(bridge.name, n), j)
                    for n, j in get_nested_jobs(cpl, depth=depth)
                )
            else:
                logging.info(
                    "No downstream pipeline (yet) for bridge %r (%i)",
                    bridge.name, bridge.id,
                )

def run(args):
    # Attempt to make the extraction directory but do not create any parents
    if not args.extract_to.is_dir():
        args.extract_to.mkdir(parents=False)
    for cn, j in get_nested_jobs(this_pipeline, depth=args.depth):
        if args.job_matches and cn not in args.job_matches:
            logging.info("Skipping %s which does not match any arg", cn)
            continue
        with io.BytesIO() as fobj:
            try:
                j.artifacts(streamed=True, action=fobj.write)
            except gitlab.exceptions.GitlabGetError as exc:
                logging.info("No artifacts for %s", cn)
                continue
            else:
                logging.info("Extracting artifacts from %s (%i)", cn, j.id)
                fobj.seek(0)
                os.chdir(args.extract_to)
                zipfile.ZipFile(fobj).extractall()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--depth", "-d", type=int, default=UNBOUNDED)
    parser.add_argument("--extract-to", "-x", type=pathlib.Path, default=".")
    parser.add_argument(
        "job_matches", metavar="canon/job/name", nargs="*", type=pathlib.Path,
    )
    args = parser.parse_args()
    run(args)
