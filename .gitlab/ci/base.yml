---
include:
  - template: "Workflows/Branch-Pipelines.gitlab-ci.yml"
  - local: "/.gitlab/ci/cd-here.yml"

stages:
    - pre
    - builder
    - compile
    - contain
    - test
    - deploy
    - report

# These variables can't be overridden due to some weirdness with how include
# seems to work...
variables:
    # This can be overloaded to use a different docker in docker container
    # The project's local variables take precedence
    DIND: "docker:dind"
    # This is needed to support services on Windows
    FF_NETWORK_PER_BUILD: 1

# We define default variables which a job might wish to override here
.base_job:
    variables:
        # This is the script that manages challenges
        CHALLENGE_MANAGER: ${CI_PROJECT_DIR}/bin/chal.py
        # We ignore all submodules by default and expect individual jobs or
        # pipelines to change this if necessary
        GIT_SUBMODULE_STRATEGY: none

.compile_template: &compile_challenge
    # This template sets up the builder to run pwntools and compile C/C++ challenges
    extends:
        - .base_job
        - .cd_here
    image: gcc
    stage: compile
    variables:
        TERM: 'xterm'
        TERMINFO: '/etc/terminfo'
    artifacts:
        paths:
            - ${CBCI_JOB_PATH}/dist
            - ${CBCI_JOB_PATH}/handout*
    script:
        - bin/chal.py info
        - bin/chal.py --local build

.build_windows_container_template: &build_windows_container
    extends:
        - .base_job
        - .cd_here_windows
    # This template handles building and pushing containers
    # Fill in the challenge path and the tag
    # We try to use environment variables to allow building from other gitlab instances
    stage: contain
    tags:
        # Windows does not support docker-in-docker directly, so we'll run
        # these jobs directly on the runner
        - windows-shell
    variables:
        CHALLENGE_PATH: "."
        TAG: "anonymous-coward"
        WINDOW_IMAGES_REPOSITORY: "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/windows-images"
    script:
        # Defaults to registry.gitlab.com on gitlab.com
        - docker login -u gitlab-ci-token -p $env:CI_JOB_TOKEN $env:CI_REGISTRY
        # Here we pull in the last version and try to use it as a cache to speed up builds
        - docker pull $env:CI_REGISTRY_IMAGE/$env:TAG ; $True
        # We default to using a Dockerfile in the root of ${CHALLENGE_PATH}
        # but allow jobs which extend this template to use a different location
        # under that tree by defining ${DOCKERFILE_RELPATH}
        - >
            docker build
            --build-arg WINDOW_IMAGES_REPOSITORY=$env:WINDOW_IMAGES_REPOSITORY
            --build-arg CI_REGISTRY_IMAGE=$env:CI_REGISTRY_IMAGE
            --cache-from $env:CI_REGISTRY_IMAGE/$env:TAG -t $env:TAG
            -f $env:CHALLENGE_PATH/$(If ($env:DOCKERFILE_RELPATH) {$env:DOCKERFILE_RELPATH} Else {"Dockerfile"})
            $env:CHALLENGE_PATH
        - docker tag $env:TAG $env:CI_REGISTRY_IMAGE/$env:TAG
        - docker push $env:CI_REGISTRY_IMAGE/$env:TAG
    after_script:
        # Docker will save the credentials from the docker login above, which
        # will not be valid for subsequent jobs since we're on a persistent
        # runner. As such, we want to logout to deleted the saved creds when
        # we're done.
        - docker logout $env:CI_REGISTRY

.build_container_template: &build_container
    extends:
        - .base_job
        - .cd_here
    # This template handles building and pushing containers
    # Fill in the challenge path and the tag
    # We try to use environment variables to allow building from other gitlab instances
    image: docker
    stage: contain
    tags:
        - docker
    services:
        - name: ${DIND}
          alias: docker
    variables:
        CHALLENGE_PATH: "."
        TAG: "anonymous-coward"
    script:
        # Defaults to registry.gitlab.com on gitlab.com
        - docker login -u gitlab-ci-token -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
        # Here we pull in the last version and try to use it as a cache to speed up builds
        - docker pull ${CI_REGISTRY_IMAGE}/${TAG} || true
        # We default to using a Dockerfile in the root of ${CHALLENGE_PATH}
        # but allow jobs which extend this template to use a different location
        # under that tree by defining ${DOCKERFILE_RELPATH}
        - echo "CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}"
        - echo "CHALLENGE_PATH=${CHALLENGE_PATH}"
        - echo "DOCKERFILE_RELPATH=${DOCKERFILE_RELPATH:-Dockerfile}"
        - echo "TAG=${TAG}"
        # Set the variable DOCKER_BUILD_EXTRA_ARGS in your CI job to add
        # extra flags to the docker build command.
        # This is useful for passing build-args to your container.
        - >
            docker build
            --build-arg CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE}"
            --cache-from ${CI_REGISTRY_IMAGE}/${TAG} -t ${TAG}
            ${DOCKER_BUILD_EXTRA_ARGS}
            -f ${CHALLENGE_PATH}/${DOCKERFILE_RELPATH:-Dockerfile}
            ${CHALLENGE_PATH}
        - docker tag ${TAG} ${CI_REGISTRY_IMAGE}/${TAG}
        - docker push ${CI_REGISTRY_IMAGE}/${TAG}
