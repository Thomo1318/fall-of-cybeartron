variable "aws_region" {
  default = "ap-southeast-2"
}

variable "chal_domain" {
  default = "chal.cybears.io"
}
variable "ctf_domain" {
  default = "ctf.cybears.io"
}

variable "chal_domain_zone_id" {
  description = "zone id for the route53 zone for the chal_domain"
}

variable "ctf_domain_zone_id" {
  description = "zone id for the route53 zone for the ctf_domain"
}

variable "challenge_eks_cluster_name" {
  description = "challenges eks cluster name"
}

variable "kubernetes_deployment_node_selector" {
  type = map(string)
  default = {
    "beta.kubernetes.io/os" = "linux"
  }
  description = "Node selectors for kubernetes deployment"
}