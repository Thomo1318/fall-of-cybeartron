
output "ctf_domain" {
  description = "Domain name for ctf frontend."
  value       = aws_route53_zone.ctf.name
}

output "ctf_dns_zone" {
  description = "Route53 zone for ctf frontend domain."
  value       = aws_route53_zone.ctf.zone_id
}

output "ctf_dns_ns" {
  description = "Route53 nameservers for ctf frontend domain."
  value       = aws_route53_zone.ctf.name_servers
}

output "chal_domain" {
  description = "Domain name for challenges."
  value       = aws_route53_zone.chal.name
}

output "chal_dns_zone" {
  description = "Route53 zone for challenge domain."
  value       = aws_route53_zone.chal.zone_id
}

output "chal_dns_ns" {
  description = "Route53 nameservers for challenge domain."
  value       = aws_route53_zone.chal.name_servers
}
