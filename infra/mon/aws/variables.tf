variable "aws_region" {
  default = "ap-southeast-2"
}

variable "grafana_subdomain" {
  type        = string
  description = "subdomain for the grafana instance."
  default     = "status"
}

variable "ctf_domain" {
  type        = string
  description = "Domain for the CTF."
  default     = ""
}

variable "grafana_certificate_arn" {
  type        = string
  description = "SSL Certificate ARN to be used for the HTTPS server."
  default     = ""
}