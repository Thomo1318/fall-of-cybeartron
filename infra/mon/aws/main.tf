terraform {
  required_version = ">= 0.14.9"
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.34"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

data "terraform_remote_state" "eks" {
  backend = "s3"
  config = {
    bucket = "terraform-20191116042935470700000001"
    key    = "eks/terraform.tfstate"
    region = "ap-southeast-2"
  }
}

data "aws_eks_cluster" "cluster" {
  name = data.terraform_remote_state.eks.outputs.aws_eks_cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = data.terraform_remote_state.eks.outputs.aws_eks_cluster_id
}
provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.id]
    command     = "aws"
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.id]
      command     = "aws"
    }
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

locals {
  annotations         = <<EOT
      kubernetes.io/ingress.class: alb
      alb.ingress.kubernetes.io/scheme: internet-facing
      alb.ingress.kubernetes.io/certificate-arn: ${var.grafana_certificate_arn}
      alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80}, {"HTTPS":443}]'
      external-dns.alpha.kubernetes.io/hostname: ${var.grafana_subdomain}.${var.ctf_domain}
      alb.ingress.kubernetes.io/actions.ssl-redirect: '{"Type": "redirect", "RedirectConfig": { "Protocol": "HTTPS", "Port": "443", "StatusCode": "HTTP_301"}}'
    EOT
  host                = "- ${var.grafana_subdomain}.${var.ctf_domain}"
  service             = <<EOT
    type: ClusterIP
    port: 80
    targetPort: 3000
  EOT
  service_annotations = <<EOT
      alb.ingress.kubernetes.io/target-type: ip
  EOT
  extra_paths         = <<EOT
    - path: /*
      backend:
        serviceName: ssl-redirect
        servicePort: use-annotation
  EOT
}
module "prometheus" {
  source              = "../prometheus_stack"
  namespace           = kubernetes_namespace.monitoring.metadata[0].name
  annotations         = local.annotations
  host                = local.host
  service             = local.service
  service_annotations = local.service_annotations
  extra_paths         = local.extra_paths
}

module "cloudwatch_exporter" {
  source = "../cloudwatch_exporter"
  # we have the right IAM's so dont need to use creds
  aws_access_key_id     = ""
  aws_secret_access_key = ""
  namespace             = kubernetes_namespace.monitoring.metadata[0].name
}

terraform {
  backend "s3" {
    bucket         = "terraform-20191116042935470700000001"
    key            = "mon/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-lock-dynamo"
    region         = "ap-southeast-2"
  }
}