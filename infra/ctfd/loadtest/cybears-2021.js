
import { group, sleep, check, fail } from 'k6';
import http from 'k6/http';
import { Rate } from "k6/metrics";

// Version: 1.2
// Creator: WebInspector


export let errorRate = new Rate("errors");

export let options = {
	maxRedirects: 0,
	thresholds: {
		errors: ["rate<0.01"] // <1% errors
	  }
};

let host =  `${__ENV.HOSTNAME}`;
let origin = "http://" + host;
let base_url = origin + "/";
let result;
export default function main() {

	let username = Math.random().toString(36).substring(2);
	let newteam = Math.random().toString(36).substring(2);
	let password = Math.random().toString(36).substring(7);
	let session = "";
	let nonce = "";
	let team_nonce = "";
	let io = "";
	let sid = "";
	let counter = 0;
	group("Get vars", function() {

		let res = http.get(base_url,{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get status was 200': res => res.status === 200,
		})) {
			session = res.cookies['session'];
            if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
			
		} else {
			fail("status code was *not* 200");
		}

	});

	group("Register User", function() {

		let res = http.get(base_url + "register",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get register page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
		} else {
			fail("get register page status was *not* 200");
		}

		res = http.post(base_url + 'register',
            {
                "name": username,
                "email": username + "@mail.com",
                "password": password,
                "nonce": nonce,
                "_submit": "Submit"
            },
            {
                "headers": {
                    "Host": host,
                    "Connection": "keep-alive",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                    "Sec-Fetch-Dest": "document",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                    "Sec-Fetch-Site": "none",
                    "Sec-Fetch-Mode": "navigate",
                    "Sec-Fetch-User": "?1",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "en-US,en;q=0.9"
                }
            }
		);
		if (!check(res, {
			'User registration status was 302': res => res.status === 302,
		})) {
			fail("status code was *not* 302");
		}

	});

	group("Register Team", function() {

		let res = http.get(base_url + "teams/new",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get new team page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                team_nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                team_nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
		} else {
			fail("get new team page status was *not* 200");
		}

		res = http.post(base_url + 'teams/new',
            {
                "name": newteam,
                "password": password,
                "nonce": team_nonce,
                "_submit": "Create"
            },
            {
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (!check(res, {
			'team registration status was 302': res => res.status === 302,
		})) {
			fail("team registration status *not* 302");
		}

	});
  let response;

  group("page_11 - http://10.0.2.15:51245/challenges", function () {
    response = http.get(base_url + "challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/challenge-board.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/2.7c5410d1.chunk.js",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/main.2b14090f.chunk.js",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "script",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/challenge-board.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_blk.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_bot.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "api/v1/challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
  });

  group("page_10 - http://10.0.2.15:51245/users", function () {
    response = http.get(base_url + "users", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pigbot_blk.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pig_bot.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_9 - http://10.0.2.15:51245/teams", function () {
    response = http.get(base_url + "teams", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_blk.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_bot.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_8 - http://10.0.2.15:51245/scoreboard", function () {
    response = http.get(base_url + "scoreboard", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/scoreboard.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/echarts.bundle.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/trypitakon_blk.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/trypitakon_bot.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "api/v1/scoreboard/top/10", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
  });

  group("page_7 - http://10.0.2.15:51245/challenges", function () {
    response = http.get(base_url + "challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/css/challenge-board.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/js/2.7c5410d1.chunk.js",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/js/main.2b14090f.chunk.js",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "script",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/challenge-board.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_blk.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_bot.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(base_url + "api/v1/challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(base_url + "api/v1/challenges/61", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.post(
      base_url + "api/v1/challenges/attempt",
      '{"challenge_id":61,"submission":"4 May 1981"}',
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          accept: "application/json",
          "csrf-token":
            "f6bee3fffad7dae5831f22c0c4cbaf993535103e8c3fcc9b9d8fafa1d8ea0c1d",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          "content-type": "application/json",
          "Sec-GPC": "1",
          Origin: "http://10.0.2.15:51245",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)

    response = http.get(base_url + "api/v1/challenges/61/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/61", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept: "*/*",
        "Sec-GPC": "1",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_6 - http://10.0.2.15:51245/scoreboard", function () {
    response = http.get(base_url + "scoreboard", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/scoreboard.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/echarts.bundle.min.js?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=ac6e5545",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_blk.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/sanditron_bot.png?d=ac6e5545",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "api/v1/scoreboard/top/10", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_5 - http://10.0.2.15:51245/teams", function () {
    response = http.get(base_url + "teams", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=5df20373",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_blk.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_bot.png?d=5df20373",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "teams",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
  });

  group("page_4 - http://10.0.2.15:51245/users", function () {
    response = http.get(base_url + "users", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "ctf_intro",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_blk.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_bot.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_3 - http://10.0.2.15:51245/ctf_intro", function () {
    response = http.get(base_url + "ctf_intro", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/pixel.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/base.min.css?d=95832cd0",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=95832cd0",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-regular.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=95832cd0",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=95832cd0",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/wukongprime_bot.png?d=95832cd0",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "ctf_intro",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "ctf_intro",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
  });

  group("page_2 - http://10.0.2.15:51245/", function () {
    response = http.get(base_url + "", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        Referer: base_url + "ctf_intro",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/main.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/core.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/base.min.css?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/asd-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/halftone-logo.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/vendor.bundle.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/core.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/helpers.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/js/pages/main.min.js?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "text/css,*/*;q=0.1",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "no-cors",
          "sec-fetch-dest": "style",
          referer: base_url + "",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/AAsianHiro.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          pragma: "no-cache",
          "cache-control": "no-cache",
          origin: "http://10.0.2.15:51245",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/Shojumaru.ttf",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/cybear_bot.png?d=0014a685",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/fonts/roboto-v20-latin-100.woff2",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          Origin: "http://10.0.2.15:51245",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/fonts.min.css?d=0014a685",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Pragma: "no-cache",
        "Cache-Control": "no-cache",
        Accept: "text/event-stream",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        Referer: base_url + "",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "files/13ce8e44c568951922e29e0d01b773ad/favicon.ico",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
    response = http.get(
      base_url + "themes/cybears/static/img/4bots_300dpi.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Pragma: "no-cache",
          "Cache-Control": "no-cache",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          Referer:
            base_url + "themes/cybears/static/css/enlightenment-landing.min.css",
          "Accept-Encoding": "gzip, deflate",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
  });

  // Automatically added sleep
  sleep(1);
}
