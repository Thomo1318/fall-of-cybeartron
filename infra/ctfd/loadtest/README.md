# Load Testing

Current testing is done via k6 with influxdb and grafana for monitoring.

## Visualisation 
```
    docker-compose up -d influxdb grafana
```
This will spin up a influxdb (8086) and grafana (3000) to watch performance.

## Running

k6 requires a javascript file that programatically access the target website. We need a separate javascript file for each theme/version of CTFd as the requests are tied heavily to the theme(scripts, fonts, css etc.)

Set HOSTNAME to the CTFd location to test and the js file to the config you want to test. See the next section if you need to create a new config. Note: the current directory is mapped to /scripts/ in the k6 container.

```bash
docker-compose run -e HOSTNAME=10.0.2.15:52664 k6 run -i 600 -u 100 /scripts/core-2021.js
```
where 600 is number of iterations of the script, 100 is number of virtual users (connections)

## Creating a config

This is required when a new theme (or major update to CTFd) is being used.

Follow the instructions at https://k6.io/docs/test-authoring/recording-a-session For my testing I firstly logged in with the cybears account before clearing logs in the browser then did the following after selecting preserve logs checkbox in your browser.

  1. Load home page
  1. Look at all other pages
  1. Look at scoreboard
  1. Look at challenges
  1. Fail to solve challenge
  1. Successfully solve challenge
  1. Look at scoreboard

Then save the .har file.

Assuming you saved a .har file as `ctfd.har` in this directory run the following to convert to a k6 script file (you need docker installed):

```bash
./convert.sh ctfd.har ctfd.js
```
