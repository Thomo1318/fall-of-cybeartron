#!/usr/bin/env python3
import argparse

start = """
import { group, sleep, check, fail } from 'k6';
import http from 'k6/http';
import { Rate } from "k6/metrics";

// Version: 1.2
// Creator: WebInspector


export let errorRate = new Rate("errors");

export let options = {
	maxRedirects: 0,
	thresholds: {
		errors: ["rate<0.01"] // <1% errors
	  }
};

let host =  `${__ENV.HOSTNAME}`;
let origin = "http://" + host;
let base_url = origin + "/";
let result;
"""
get_vars = """
	let username = Math.random().toString(36).substring(2);
	let newteam = Math.random().toString(36).substring(2);
	let password = Math.random().toString(36).substring(7);
	let session = "";
	let nonce = "";
	let team_nonce = "";
	let io = "";
	let sid = "";
	let counter = 0;
	group("Get vars", function() {

		let res = http.get(base_url,{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get status was 200': res => res.status === 200,
		})) {
			session = res.cookies['session'];
            if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\\': "')[1].split('"')[0];
            }
			
		} else {
			fail("status code was *not* 200");
		}

	});

	group("Register User", function() {

		let res = http.get(base_url + "register",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get register page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\\': "')[1].split('"')[0];
            }
		} else {
			fail("get register page status was *not* 200");
		}

		res = http.post(base_url + 'register',
            {
                "name": username,
                "email": username + "@mail.com",
                "password": password,
                "nonce": nonce,
                "_submit": "Submit"
            },
            {
                "headers": {
                    "Host": host,
                    "Connection": "keep-alive",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                    "Sec-Fetch-Dest": "document",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                    "Sec-Fetch-Site": "none",
                    "Sec-Fetch-Mode": "navigate",
                    "Sec-Fetch-User": "?1",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "en-US,en;q=0.9"
                }
            }
		);
		if (!check(res, {
			'User registration status was 302': res => res.status === 302,
		})) {
			fail("status code was *not* 302");
		}

	});

	group("Register Team", function() {

		let res = http.get(base_url + "teams/new",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get new team page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                team_nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                team_nonce = res.body.split('csrfNonce\\': "')[1].split('"')[0];
            }
		} else {
			fail("get new team page status was *not* 200");
		}

		res = http.post(base_url + 'teams/new',
            {
                "name": newteam,
                "password": password,
                "nonce": team_nonce,
                "_submit": "Create"
            },
            {
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (!check(res, {
			'team registration status was 302': res => res.status === 302,
		})) {
			fail("team registration status *not* 302");
		}

	});
"""

response_check = """    result = check(response, {' status was 200': (res) => res.status === 200,});
    errorRate.add(!result)
"""
def convert(input, output):
    host = ''
    base_url = ''
    previous_line = ''
    nonce = ''
    reached_main = False
    tracking_responce = False
    output.write(start)
    for line in input.readlines():
        new_line = line
        if 'CSRF-Token' in previous_line:
            # if not nonce:
            #     nonce = line.split('"')[1]
            #     print(f'nonce {nonce}')
            new_line = line.split('"')[0] + 'team_nonce,\n'
        if '://' in line:
            if not base_url:
                host = line.split('://')[1].split('/')[0]
                base_url = '\"http://' + host + '/'
                print(f'host {host}')
                print(f'base_url {base_url}')
        if base_url and base_url in line:
            new_line = line.replace(base_url, 'base_url + \"')
        elif host and '"' + host + '"' in line:
            new_line = line.replace('"' + host + '"', 'host')
        elif host and '"' + host in line:
            new_line = line.replace('"' + host, 'host + "')
        elif '"session=' in line:
            new_line = ''
        elif 'Cookie:' in line:
            new_line = ''
        elif 'response' in line:
            tracking_responce = True
        if ');' in line and tracking_responce:
            new_line = line + response_check
            tracking_responce = False

        previous_line = line
        if reached_main:
            output.write(new_line)
        if 'export default function' in line:
            output.write(new_line)
            output.write(get_vars)
            reached_main = True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input", "-i", action="store",
        help="Input file",
        type=argparse.FileType('r'),
        required=True
    )
    parser.add_argument(
        "--output", "-o", action="store",
        help="output file",
        type=argparse.FileType('w'),
        required=True
    )
    args = parser.parse_args()
    convert(args.input, args.output)
