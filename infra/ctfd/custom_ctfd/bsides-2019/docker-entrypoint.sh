#!/bin/sh
set -eo pipefail

WORKERS=${WORKERS:-1}
WORKER_CLASS=${WORKER_CLASS:-geventwebsocket.gunicorn.workers.GeventWebSocketWorker}
CTFD_PORT=${CTFD_PORT:-8000}

# Check that the database is available
if [ -n "$DATABASE_URL" ]
    then
    database=`echo $DATABASE_URL | awk -F[@//] '{print $4}'`
    echo "Waiting for $database to be ready"
    while ! mysqladmin ping -h $database --silent; do
        # Show some progress
        echo -n '.';
        sleep 1;
    done
    echo "$database is ready"
    # Give it another second.
    sleep 1;
fi

# Log to stdout/stderr by default
if [ -n "$LOG_FOLDER" ]; then
    ACCESS_LOG=${LOG_FOLDER}/access.log
    ERROR_LOG=${LOG_FOLDER}/error.log
else
    ACCESS_LOG=-
    ERROR_LOG=-
fi

# Initialize database
python manage.py db upgrade

# Start CTFd
echo "Starting CTFd"
exec gunicorn 'CTFd:create_app("cybears_config.Config")' \
    --bind "0.0.0.0:${CTFD_PORT}" \
    --workers $WORKERS \
    --worker-class "$WORKER_CLASS" \
    --access-logfile "$ACCESS_LOG" \
    --error-logfile "$ERROR_LOG"
