Theme instructions
---

1. Copy the cybears directory into the container themes dir. Unfortunately, some parts of the theme are hardcoded, so you cannot rename the theme folder to be something else. It should end up as something like this: /opt/CTFd/CTFd/themes/cybears
2. Choose the 'cybears' theme in the CTFd admin config, or using the API. Alternatively, symlink the 'core' theme to 'cybears'.
3. At some stage, run the 'bin/upload_pages.py' script to upload the rules, story, and patch the welcome page.

