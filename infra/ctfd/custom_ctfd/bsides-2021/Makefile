CI_REGISTRY_IMAGE ?= registry.gitlab.com/cybears/fall-of-cybeartron
DOCKER ?= docker

.PHONY: default theme enlightenment container merge clean cleanall

all: theme enlightenment container
local: theme enlightenment merge
theme: .theme.stamp
enlightenment: .enlightenment.stamp
container: .container.stamp

# Building the theme itself
CORE_THEME_PATH := CTFd/CTFd/themes/core
CORE_THEME_DEPS := $(shell find "$(CORE_THEME_PATH)/assets" -type f)
CYBEARS_THEME_PATH := extras/themes/cybears
CYBEARS_THEME_DEPS := $(shell find "$(CYBEARS_THEME_PATH)/assets" -type f)

CYBEARS_THEME_DEPS += $(CYBEARS_THEME_PATH)/node_modules
$(CYBEARS_THEME_PATH)/node_modules: $(CYBEARS_THEME_PATH)/package-lock.json
	ln -srft $(dir $(CYBEARS_THEME_PATH)) $(CORE_THEME_PATH)
	cd $(CYBEARS_THEME_PATH) ; npm ci
.INTERMEDIATE: $(CYBEARS_THEME_PATH)/node_modules

.theme.stamp: $(CORE_THEME_DEPS) $(CYBEARS_THEME_DEPS)
	ln -srft $(CORE_THEME_PATH) $(CYBEARS_THEME_PATH)/node_modules
	cd $(CYBEARS_THEME_PATH) ; npm run build
	touch "$@"

# Building the enlightenment drop-in components
ENLIGHTENMENT_PATH := enlightenment
ENLIGHTENMENT_DEPS := $(shell find "$(ENLIGHTENMENT_PATH)/src" -type f)

ENLIGHTENMENT_DEPS += $(ENLIGHTENMENT_PATH)/node_modules
$(ENLIGHTENMENT_PATH)/node_modules: $(ENLIGHTENMENT_PATH)/package-lock.json
	cd $(ENLIGHTENMENT_PATH) ; npm ci
.INTERMEDIATE: $(ENLIGHTENMENT_PATH)/node_modules

.enlightenment.stamp: $(ENLIGHTENMENT_DEPS)
	cd $(ENLIGHTENMENT_PATH) ; npm run build -- ignore-scripts
	touch "$@"

merge: .theme.stamp .enlightenment.stamp
	enlightenment/postbuild.sh

# Container build
.container.stamp: theme enlightenment
	$(DOCKER) build --pull                                                  \
	    -t "$(CI_REGISTRY_IMAGE)/ctfd-bsides-2021:local"                    \
	    --build-arg="CI_REGISTRY_IMAGE=$(CI_REGISTRY_IMAGE)"                \
	    .

clean:
	rm -rf --                                                               \
	    "$(CYBEARS_THEME_PATH)/static"                                      \
	    "$(ENLIGHTENMENT_PATH)/build"                                       \
	    "$(CYBEARS_THEME_PATH)/templates/enlightenment"
	rm -f --                                                                \
	    "$(CORE_THEME_PATH)/node_modules"                                   \
	    "$(dir $(CYBEARS_THEME_PATH))/core"
	git checkout HEAD -- "$(CYBEARS_THEME_PATH)/templates" ||:
	rm -f -- .*.stamp

cleanall: clean
	rm -rf --                                                               \
	    "$(CYBEARS_THEME_PATH)/node_modules"                                \
	    "$(ENLIGHTENMENT_PATH)/node_modules"
