import Badge from 'react-bootstrap/Badge'

const ChallengeTags = props => {
    const { tagsArray } = props
    const tags = tagsArray.map(tag => <Badge key={tag} variant="secondary">{tag}</Badge>)
    return <div className="challenge-tags-container">{tags}</div>
}

export default ChallengeTags
