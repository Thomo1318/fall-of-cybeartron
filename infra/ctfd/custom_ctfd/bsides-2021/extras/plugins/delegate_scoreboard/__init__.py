import unittest.mock

from ...cache import clear_standings as real_clear_standings
from . import api, scoreboard, scores

SCOREBOARD_LISTING_EP = "scoreboard.listing"


def load(app):
    # Ensure that clearing the cache using normal mechanisms resets ours too
    def override_clear_standings(*a, **k):
        ret = real_clear_standings(*a, **k)
        cache.delete_memoized(scores.plank_standings)
        cache.delete_memoized(api.PlankScoreboardDetail.get)
        cache.delete(SCOREBOARD_LISTING_EP)
        return ret
    mock_clear_standings = unittest.mock.patch("CTFd.cache.clear_standings")
    mock_clear_standings.side_effect = override_clear_standings
    mock_clear_standings.start()
    # Hijack the scoreboard view with our own which shows delegate status
    app.view_functions[SCOREBOARD_LISTING_EP] = scoreboard.listing
